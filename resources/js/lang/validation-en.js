const validation_en = {
    'field/validation/required': '{field} is required.',
    'field/validation/text/invalid': 'Please provide valid {field}',
    'field/validation/dropdown/invalid': 'Please provide valid {field}',

    'auth/failed': 'These credentials do not match our records',
}

export default validation_en;