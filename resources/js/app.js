/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

// require('./bootstrap');
import './bootstrap';
import Vue from 'vue';
import Vuetify from 'vuetify';

// Route information for Vue Router
import Routes from '@/js/routes.js';

// Component File
import App from '@/js/views/App';
import adminApp from '@/js/views/AdminApp';
import memberApp from '@/js/views/MemberApp';

// Config File
import Config from '@/js/config/app.js';

// Check Language
import langEnglish from '@/js/lang/en.js';
import langChinese from '@/js/lang/zh-CN.js';
import validateEnglish from '@/js/lang/validation-en.js';
import validateChinese from '@/js/lang/validation-zh-CN.js';
import Vuelidate from 'vuelidate';

import adminStore from '@/js/stores/admin/store';
import memberStore from '@/js/stores/member/store';
import axios from 'axios';

let language = localStorage.getItem('language');
let lang = "";
let validateLang = "";
let langValue = '';

if(!language){
	localStorage.setItem('language', Config.locale.english);
	lang = langEnglish;
	validateLang = validateEnglish;
	langValue = Config.locale.english;
}else{
	if(localStorage.getItem('language') == Config.locale.english){
		lang = langEnglish;
		validateLang = validateEnglish;
		langValue = Config.locale.english;
	}else{
		lang = langChinese;
		validateLang = validateChinese;
		langValue = Config.locale.chinese;
	}
}

Vue.use(Vuetify);
Vue.use(Vuelidate);

var urlPath = window.location.pathname.split('/');

if(urlPath.indexOf(Config.prefix.admin_prefix) > 0){
	let fullPath = '';

	urlPath.forEach(function(element) {
		if(element != ''){
			fullPath = fullPath + '/' + element;
		}				
	});

	let isNotFound = true;
	
	/* Detect Url Path */
	let routeInfo = Routes.resolve({path: fullPath});
	if(routeInfo.resolved.matched.length > 0){
		isNotFound = false;
	}

	Routes.afterEach((to, from) => {
		document.title = to.meta.title || 'Some Defaul Title';
	});

	let loginToken = '';
	loginToken = (localStorage.getItem('loginToken') != '')? localStorage.getItem('loginToken') : '';
	axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('loginToken');
	axios.defaults.headers.common['Content-Type'] = "application/json";
	
	new Vue({
		el: '#adminApp',		
		data: {
			language: lang,
			validateLanguage: validateLang,
			base_url: Config.base_url(),
			pageNotFound: isNotFound,
			langValue: langValue,
		},
		router: Routes,
		store: adminStore,
		render: al => al(adminApp),
	});
}else if(urlPath.indexOf(Config.prefix.member_prefix) > 0){
	let fullPath = '';

	urlPath.forEach(function(element) {
		if(element != ''){
			fullPath = fullPath + '/' + element;
		}				
	});

	let isNotFound = true;
	
	/* Detect Url Path */
	let routeInfo = Routes.resolve({path: fullPath});
	if(routeInfo.resolved.matched.length > 0){
		isNotFound = false;
	}

	Routes.afterEach((to, from) => {
		document.title = to.meta.title || 'Some Defaul Title';
	});

	let loginToken = '';
	loginToken = (localStorage.getItem('loginToken') != '')? localStorage.getItem('loginToken') : '';
	axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('loginToken');
	axios.defaults.headers.common['Content-Type'] = "application/json";
	
	new Vue({
		el: '#memberApp',		
		data: {
			language: lang,
			validateLanguage: validateLang,
			base_url: Config.base_url(),
			pageNotFound: isNotFound,
			langValue: langValue,
		},
		router: Routes,
		store: memberStore,
		render: al => al(memberApp),
	});
} else {
	const app = new Vue({
		el: '#app',
		router: Routes,
		render: h => h(App),
	});
}