<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\WebService;
use App\WebServiceUser;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use Lang;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    public function register(Request $request) { 
		$ws = new WebService();
		$ws_user = new WebServiceUser();
        $user = new User();
		
		$form_fields = array("name", "password", "email", "mobile", "ic", "bank_name", "bank_account_num");

        foreach($form_fields as $field){
            $data[$field] = $request->$field;
        }

        try{
            $user->validate_add_member($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '4000001200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data, $field_error = $user->err_field_params);
                    return response()->json($result);  
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array("dev_msg" => "Error at validate add user."), $field_error = array());
                	return response()->json($result);  
                    break;
            }   
        }
        
        try{
            $mid = $user->add_member($data);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_query_info['message']))), $field_error = array());
                    break;
            }    
            return response()->json($result);  
        }
        
        try{
            $memberInfo = $user->get_member_detail($uid='',$id = $mid);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_query_info['message']))), $field_error = array());
                    break;
            }    
            return response()->json($result);  
        }

        try{
        	$wsdata = array("uid" => $memberInfo['uid']);
            $ws_user->add_ws_user($wsdata);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    break;
            }    
            return response()->json($result);  
        }

		$adata = array();
		
		$adata['success'] = array(
			"profile" => $memberInfo,
		);
		
		$result = $ws->api_result($ws::$api_code_to_status['success'], $adata);
		
		return response()->json($result);        
    }

    public function login(Request $request){
        $user = new User();
        $ws = new WebService();

        $result = [];

        $form_fields = array("email", "password");

        foreach($form_fields as $field){
            $data[$field] = $request->$field;
        }
        
        try{
            $user->validate_login($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '3000004200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $user->err_field_params);
                    return response()->json($result);  
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array("dev_msg" => "Error at validate user login."), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
        }

        $result = $user->login($data);

        if(empty($result)){
            $status = $ws::$api_code_to_status['internal-server-error'];
            $data = array(
                "dev_status" => $ws::$api_error_detail_to_code['internal-server-error']['user-not-found'],
                "general_msg" => Lang::get('trans.api/login/error'),
                "success" => "",
                "dev_msg" => "User Not Found.",
            ); 
            $result = $ws->api_result($status, $data);
            return response()->json($result); 
        }

        return response()->json($result);
    }

    public function logout(Request $request){
        $ws = new WebService();
        Auth::guard('web')->logout();  
        $data = array();        
        $data['success'] = Lang::get('trans.api/logout/success/message');
        $result = $ws->api_result($status = $ws::$api_code_to_status['success'], $data);
        
        return response()->json($result);
    }

    public function update_profile(Request $request){
        $user = new User();
        $ws = new WebService();
        
        $form_fields = array("name", "email", "id");
        $data = array();
        
        foreach($form_fields as $field){
            if(isset($request->$field)){
                $data[$field] = $request->$field;
            }
        }
        
        try{
            $user->validate_update_profile($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '4000018200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $user->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    break;
            }   
            
            return response()->json($result);
        }
        
        try{
            $user->update_profile($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }
    
        try{
            $userDetail = $user->get_user_detail($data['id']);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        return $ws->api_result($ws::$api_code_to_status['success'], array('profile' => $userDetail));
    }

    public function update_password(Request $request){
        $user = new User();
        $ws = new WebService();
        
        $form_fields = array("password", "confirm_password", "id");
        $data = array();

        foreach($form_fields as $field){
            if(isset($request->$field)){
                $data[$field] = $request->$field;
            }
        }
        
        try{
            $user->validate_update_password($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '4000020200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $user->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    break;
            }   
            return response()->json($result);
        }

        try{
            $user->update_password($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        try{
            $userDetail = $user->get_user_detail($data['id']);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        return $ws->api_result($ws::$api_code_to_status['success'], array('profile' => $userDetail));
    }
}
