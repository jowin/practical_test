<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Password;
use Auth;
use Illuminate\Http\Request;
use Config;
use App;

class MerchantResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/merchant';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:merchant');
    }

    protected function broker(){
        return Password::broker('merchants');
    }

    protected function guard(){
        return Auth::guard('merchant');
    }

    /* 
    # Overwrite function
    */
    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.reset-merchant')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    /**
     * Overwrite Get the response for a successful password reset.
     *
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse($response, $request)
    {
        $locale = \Request::segment(1);

        if( ! in_array($locale, Config::get('app.locales')) ){
           $locale = Config::get('app.fallback_locale');
        }       
        
        App::setLocale($locale);

        if($locale == Config::get('app.chinese_locale')){
            return redirect()->route($locale.'.admin.password.reset.success');
        }else{
            return redirect()->route('merchant.password.reset.success');
        }

        // return redirect($this->redirectPath())->with('status', trans($response));
    }

    public function showResetPasswordSuccess()
    {
        return view('auth/passwords/reset-success-merchant');
    }
}
