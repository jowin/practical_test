<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErrorLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('err_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('uid');
            $table->text('ip');
            $table->text('url');
            $table->text('post_data');
            $table->text('get_data');
            $table->text('trace');
            $table->text('err_msg');
            $table->datetime('created_at');
            $table->datetime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('err_logs');
    }
}