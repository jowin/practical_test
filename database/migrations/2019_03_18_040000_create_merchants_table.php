<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('uid');
            $table->string('name', 191);
            $table->string('email')->unique();
            $table->string('password', 191);
            $table->string('mobile', 100);
            $table->string('ic', 12);
            $table->string('bank_name', 255);
            $table->string('bank_account_num', 100);
            $table->rememberToken();
            $table->text('api_token')->nullable();
            $table->string('api_token_expire_date')->nullable();
            $table->string('forgot_password_vcode', 100);
            $table->string('register_vcode', 100);
            $table->unsignedSmallInteger('status')->comment('10-active, 90-suspend, 100-terminated');
            $table->datetime('register_vcode_cdate');
            $table->datetime('created_at');
            $table->datetime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
}