<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use App\ApiLogActivity;
use App\WebService;

class ApiLog
{
	public $attributes;

	public function __construct(Application $app, Redirector $redirector, Request $request) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
    }
	
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {	
    	$this->request->headers->set('Accept', "application/json");

    	$request->attributes->add(['global_wsid' => $this->request->wsid, 'global_url' => $this->request->fullUrl()]);
    	
		$ws = new WebService();

		// check whether has same wsid before
		try{			
			$data = array("wsid" => $this->request->wsid);
			$api = $ws->retreive_ws_record($data);
		}catch(Exception $e){
			dd($e);
		}
		
		if(is_null($api)){
			# insert new api log
			try{
				// check wsid is not empty
				if(empty($this->request->wsid)){
					$result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['empty-wsid']);
					return response()->json($result);
				}
				
				$apilog = new ApiLogActivity();
				$apilog->post_data = json_encode($_POST);
				$apilog->get_data = json_encode($_GET);            
				$apilog->url = $this->request->fullUrl();
				$apilog->wsid = $this->request->wsid;
				$apilog->last_response = "";
				$apilog->save();
				return $next($request);
			}catch(Exception $e){
				dd($e);
			}
		}else{	
			if($api->count() >= 1){
				if($api->last_status_code == $ws::$api_code_to_status['success']){
					# retreive back last response
					$result = json_decode($api->last_response);
					return response()->json($result);
				}
				return $next($request);
			}
			return $next($request);
		}		
    }
}
