<?php

namespace App\Exceptions;

use Exception;

class CustomQueryException extends Exception
{
    public function report(){
    }

    public function render($request){
    	return $this->code;
    }
}
