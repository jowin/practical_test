<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mail;
use Swift_SmtpTransport;
use Swift_Mailer;
use View;
use Swift_Message;
use App\ErrorLog;
use Auth;
use Illuminate\Http\Request;
use App\Exceptions\CustomGeneralError;
use DB;
use App\EmailTemplate;

class ThirdPartyMail extends Model
{
    protected $class_code = '5500';

    public static $template_to_code = array(
        "forgot_password" => 10000
    );

    protected $FromAddress;
    protected $FromName;
    protected $body = "text/html";
    
    public function __construct($email_provider = "pepipost")
    {
		require_once "../vendor/swiftmailer/swiftmailer/lib/swift_required.php";

    	// Configuration
        $smtpAddress = config('services.'.$email_provider.'.host');
        $port = config('services.'.$email_provider.'.port');
        $encryption = config('services.'.$email_provider.'.encryption');;
        $username = config('services.'.$email_provider.'.username');
        $password = config('services.'.$email_provider.'.password');

        $this->FromAddress = config('services.'.$email_provider.'.from.address');
        $this->FromName = config('services.'.$email_provider.'.from.name');

        // Prepare transport
        $transport = (new Swift_SmtpTransport($smtpAddress, $port, $encryption))->setUsername($username)->setPassword($password);       	
       	
		// Create the Mailer using your created Transport
        $this->_mailer = new Swift_Mailer($transport);
    }

    public function send($data = array())
    {
    	$data["type"] = "register";

    	/* Can get from database */
    	$data["subject"] = "New User Registration";
    	$data["content"] = "register";

    	// Prepare content 
        $view = View::make('emails/test-mail', [
            'message' => '<h1>Hello World !</h1>'
        ]);
        
        $html = $view->render();

        // Create a messagez
        $message = (new Swift_Message('Wonderful Subject'))
				  ->setFrom([config('services.pepipost.from.address') => config('services.pepipost.from.name')])
				  ->setTo(["jowinkoh94@gmail.com"])
				  ->setBody($html, 'text/html');

        // Send the message   
        $result = $this->_mailer->send($message);   
          
		return $result;
		
    }

    public function sendForgotPassword($data)
    {
        $method_name = 'send-forgot-password';
        $method_code = '001';
       
        try{
            $email_info = DB::table("email_template AS e")->rightJoin('email_template_content AS c', 'e.code', '=', 'c.eid')->where('e.code', static::$template_to_code)->where('c.language', config('app.fallback_locale'))->select('e.title', 'c.subject', 'c.language', 'c.message')->first();
            $view = View::make('emails/content', ['content' => array("message" => $email_info->message)]);            
            $html = $view->render();    
            $message = (new Swift_Message(config('app.name').' '.$email_info->subject))
                    ->setFrom([$this->FromAddress => $this->FromName])
                    ->setTo($data['email'])
                    ->setBody($html, $this->body);
            $result = $this->_mailer->send($message);  
        }catch(QueryException $e){
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
    }
}
