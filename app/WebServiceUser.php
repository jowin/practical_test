<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use DB;
use Carbon\Carbon;

class WebServiceUser extends Model
{
	protected $table = 'ws_users';
	
    protected $fillable = [
        'id', 'uid', 'token_type', 'login_token', 'login_token_id', 'refresh_token', 'refresh_token_id', 'notification_id', 'device_type', 'device_agent', 'created_at', 'updated_at'
    ];

    protected $class_code = "5000";

    public $err_query_info = ["code" => "", "message" => "", "params" => array()];
    
    public $custom_err_info = ["code" => "", "message" => "", "params" => array()]; 

    public static $ws_user_table = 'ws_users';

    public function add_ws_user($data){
        $method_code = "002";
        $method_name = "add-ws-user";
    
        $data['uid'] = $data['uid'];
        $data['notification_id'] = '';
        $data['device_type'] = 0;
        $data['device_agent'] = '';
        $data['created_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();   
        $data['updated_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();
       
        try{
            DB::table($this::$ws_user_table)->insert($data);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }     
    }

    public function update_ws_user($data){
        $method_code = "003";
        $method_name = "update-ws-user";
    
        $data['notification_id'] = (isset($data['notification_id']) || !empty($data['notification_id']))? $data['notification_id'] : '';
        $data['device_type'] = (isset($data['device_type']) || !empty($data['device_type']))? $data['device_type'] : 0;
        $data['device_agent'] = (isset($data['device_agent']) || !empty($data['device_agent']))? $data['device_agent'] : '';
        $data['updated_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();
       
        try{
            DB::table($this::$ws_user_table)->where('uid', $data['uid'])->update($data);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }     
    }
}
