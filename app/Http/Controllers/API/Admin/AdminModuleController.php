<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\WebService;
use App\Admin;
use App\User;
use Illuminate\Support\Facades\Auth; 
use App\WebServiceUser;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Lang;
use Lcobucci\JWT\Parser;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use App\System;
use App\Role;

class AdminModuleController extends Controller
{
    public function index(Request $request)
    {
        $admin = new Admin();
        $ws = new WebService();

        $quick_search_fields = array(
            'name' => array('search_type' => 'like'),
            'mobile' => array('search_type' => '='),
            'email' => array('search_type' => '='),
        );
        $adv_search_fields = array('name', 'mobile', 'email', 'status', 'created_at');
        $sort_type = '';
        $sort_fields = array();
        $sort_field = '';

        foreach(array('admin_uid', 'qsearch', 'searchType', 'sortField', 'sortType') as $field){
            $data[$field] = $request->$field;
        }

        if($data['searchType'] == System::$searchType['quick']){
            $searchType = System::$searchType['quick'];
        }elseif($data['searchType'] == System::$searchType['advance']){
            $searchType = System::$searchType['advance'];
        }else{
            $searchType = '';
        }

        if(!empty($data['sortField'])){
            $sort_field = $data['sortField'];
            $sort_type = $data['sortType'];
        }

        $filterData = System::process_search_input($request, $searchType, $quick_search_fields, $adv_search_fields, $sort_fields, $sort_field, $sort_type); 
        $data = array_merge($data, $filterData);
       
        try{
            $admin->validate_get_admin_modules_info($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '2000013001':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $admin->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }
       
        try{
            $result = $admin->get_admin_modules_info($data); 
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }
        return response()->json($result);
    }

    public function getAdminStatusList(Request $request){  
        $admin = new Admin();
        $ws = new WebService();

        foreach(array('admin_uid') as $field){
            $data[$field] = $request->$field;
        }

        try{
            $admin->validate_get_admin_status_list($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '2000015001':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $admin->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        try{
            $statusList = $admin->get_admin_status_list($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("status_list" => $statusList)); 

        return $result;
    }

    public function create(Request $request){
        $admin = new Admin();
        $ws = new WebService();

        foreach(array("name", "email", "mobile", "password", "role_id") as $field){
            $data[$field] = $request->$field;
        }

        try{
            $admin->validate_add_admin($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '2000001200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $admin->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        try{
            $aid = $admin->add_admin($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        try{
            $adminInfo = $admin->get_admin_module_detail('', $aid);
        }catch(CustomValidationException $e){   
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("profile" => $adminInfo)); 

        return $result;
    }

    public function detail(Request $request){
        $admin = new Admin();
        $ws = new WebService();

        $result = '';

        foreach(array("admin_uid", "aid") as $field){
            $data[$field] = $request->$field;
        }

        try{
            $admin->validate_get_admin($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '2000018200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $admin->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        try{
            $adminInfo = $admin->get_admin($data);
            foreach($adminInfo as $li => $value){
                $adminInfo[$li]->privileges = json_decode($value->privileges, true);
            }
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                   
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("admin_info" => $adminInfo)); 

        return $result;
    }

    public function update(Request $request){
        $admin = new Admin();
        $ws = new WebService();

        $result = '';

        foreach(array("admin_uid", "aid", "email", "name", "mobile", "privileges", "id", "role_id") as $field){
            $data[$field] = $request->$field;
        }

        try{
            $adminInfo = $admin->get_admin_module_detail($data['aid']);
        }catch(CustomValidationException $e){   
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        $data['id'] = $adminInfo['id'];

        try{
            $admin->validate_update_admin($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '2000005200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $admin->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }
        
        try{
            $admin->update_admin($data);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                   
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("profile" => $adminInfo));

        return $result;
    }

    public function get_role_list(Request $request){
        $role = new Role();
        $ws = new WebService();

        $list = $role::get()->toArray();

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $list));

        return $result;
    }
}
