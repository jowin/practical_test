<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\WebService;

class ApiSetHeader
{
    public function __construct(Request $request) {
        $this->request = $request;
    }
	
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if($this->request->has('login_token')){
			$this->setHeader($this->request->login_token);
        }
        $this->request->headers->set('Accept', 'application/json');
        return $next($request);
    }
	
	public function setHeader($login_token = ""){        
		$this->request->headers->set('Authorization', WebService::$token_type . " " . $login_token);
    }
    
}
