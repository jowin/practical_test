<?php 

return [
	# Web Service
	'api/general/internal-server-error' => "Internal Server Error",
	'api/general/field-error' => "Field Error",
	'api/general/empty-wsid' => "Internal Server Error",
	'api/general/system-maintenance' => "System is Maintenance",
	'api/general/dev/message' => ':error_code: :method_name - :error_message',
	
	# Web Service Login
	'api/login/error' => "Email or Password Incorrect",
	'api/authenticated/error' => "Unauthenticated",
	'api/logout/success/message' => "You have been successfully logged out!",
	'api/forgot-password/send/email/success/message' => "Successfully send reset password link to your email.",

	# Person Status
    'person/status/active' => "Active",
    'person/status/suspend' => "Suspend",
    'person/status/terminated' => "Terminated",
]

?>