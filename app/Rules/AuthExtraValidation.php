<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Merchant;
use App\User;
use App\Admin;

class AuthExtraValidation implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $confirm_valid = true;

        switch ($attribute) {
            case 'email':
                $this->field = "email";
                $confirm_valid = false;
                $merchant = Merchant::where('email', $value)->first();
                $admin = Admin::where('email', $value)->first();
                $member = User::where('email', $value)->first();
                if(empty($admin) && empty($merchant) && empty($user)){
                    $confirm_valid = true;
                }
                break;
            default:
                break;
        }

        return $confirm_valid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $msg = trans('validation.valid_data');

        if($this->field == "email"){
            $msg = trans('validation.info_used');
        }

        return $msg;
    }
}
