import Vue from 'vue'
import Vuex from 'vuex'
import Config from '@/js/config/app.js';
import Router from '@/js/routes';
import langEnglish from '@/js/lang/en.js';
import langChinese from '@/js/lang/zh-CN.js';
import validateLangEnglish from '@/js/lang/validation-en.js';
import validateLangChinese from '@/js/lang/validation-zh-CN.js';
import AdminModules from '@/js/stores/modules/admin/admin-modules.js';

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        AdminModules: AdminModules,
    }, 
    state: {        
        authInfo: {
            token: null,
            expireDate: null,
            isAuthenticated: null,
            status: null,
            message: null,
        },
        adminInfo: { 
            id: null,
            uid: null,
            name: null,
            mobile: null,
            master: null,
            dev: null,
            privileges: null,
            email: null,
            status: null,
        },
        field_error: {},
        profile_password_field_error: {},
        updateInfoSignal: {
            updateProfileSignal: '',
            updatePasswordSignal: '',
        },
		adminCurrentPrivileges: "",
        noPrivileges: true,
    },
    mutations: {
        saveAuthInfo(state, adminData){
            localStorage.setItem('loginToken', adminData.loginToken); 
            localStorage.setItem('loginTokenExpireDate', adminData.loginTokenExpireDate); 

            state.authInfo.token = adminData.loginToken;
            state.authInfo.expireDate = adminData.loginTokenExpireDate;
            state.authInfo.status = Config.generalSuccessStatus;
            state.authInfo.message = 'success';
            state.authInfo.isAuthenticated = true;
            
        },
        saveAdminInfo(state, adminData){
            localStorage.setItem('adminInfo', JSON.stringify(adminData.adminInfo));
            state.adminInfo.id = adminData.adminInfo.id;
            state.adminInfo.uid = adminData.adminInfo.uid;
            state.adminInfo.name = adminData.adminInfo.name;
            state.adminInfo.mobile = adminData.adminInfo.mobile;
            state.adminInfo.master = adminData.adminInfo.master;
            state.adminInfo.dev = adminData.adminInfo.dev;
            state.adminInfo.privileges = adminData.adminInfo.privileges;
            state.adminInfo.status = adminData.adminInfo.status;   
            state.adminInfo.email = adminData.adminInfo.email;
        },
    }, 
    actions: {
        tryAutoLogin ({commit}) {
            const token = localStorage.getItem('loginToken');
            if (!token) {
                return;
            }
            const expirationDate = localStorage.getItem('loginTokenExpireDate');
            const now = new Date()
            if (now >= expirationDate) {
                return;
            }
            let aInfo = localStorage.getItem('adminInfo');
            aInfo = JSON.parse(aInfo);
            
            commit('saveAuthInfo', {
                loginToken: token,
                loginTokenExpireDate: expirationDate,
            })
           
            commit('saveAdminInfo', {adminInfo: aInfo, saveLocale: false});
           
            if (Router.currentRoute.name == 'admin.login' || Router.currentRoute.name == 'admin' || !Router.currentRoute.name){
                Router.replace({name: 'admin.index'});
            } 
        },
    },
    getters: {
    },
});

