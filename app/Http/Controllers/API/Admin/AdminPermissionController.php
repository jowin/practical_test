<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\WebService;
use App\Admin;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use App\System;
use App\Permission;
use Lang;

class AdminPermissionController extends Controller
{
    public function index(Request $request)
    {
        $admin = new Admin();
        $ws = new WebService();
        $permission = new Permission();

        $quick_search_fields = array(
            'name' => array('search_type' => 'like'),
        );
        $adv_search_fields = array('name');
        $sort_type = '';
        $sort_fields = array();
        $sort_field = '';

        foreach(array('admin_uid', 'qsearch', 'searchType', 'sortField', 'sortType') as $field){
            $data[$field] = $request->$field;
        }

        if($data['searchType'] == System::$searchType['quick']){
            $searchType = System::$searchType['quick'];
        }elseif($data['searchType'] == System::$searchType['advance']){
            $searchType = System::$searchType['advance'];
        }else{
            $searchType = '';
        }

        if(!empty($data['sortField'])){
            $sort_field = $data['sortField'];
            $sort_type = $data['sortType'];
        }

        $filterData = System::process_search_input($request, $searchType, $quick_search_fields, $adv_search_fields, $sort_fields, $sort_field, $sort_type); 
        $data = array_merge($data, $filterData);
       
        try{
            $admin->validate_get_admin_modules_info($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            
            switch ($err_code) {   
                case '2000013001':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $admin->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }
       
        try{
            $result = $permission->get_permission_info($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }
        return response()->json($result);
    }

    public function create(Request $request){
        $admin = new Admin();
        $ws = new WebService();
        $permission = new Permission();

        foreach(array("name", "privileges") as $field){
            $data[$field] = $request->$field;
        }

        try{
            $permission->validate_add_permission($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '6000001200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $permission->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $permission->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        try{
            $pid = $permission->add_permission($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $permission->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        try{
            $info = $permission->get_permission_detail($pid);
        }catch(CustomValidationException $e){   
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $info)); 

        return $result;
    }

    public function detail(Request $request){
        $admin = new Admin();
        $ws = new WebService();
        $permission = new Permission();

        $result = '';

        foreach(array("admin_uid", "id") as $field){
            $data[$field] = $request->$field;
        }

        try{
            $permission->validate_get_permission($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '6000018200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $permission->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        try{
            $info = $permission->get_permission_detail($data['id']);
            $info['privileges'] = json_decode($info['privileges'], true);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                   
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $permission->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $info)); 

        return $result;
    }

    public function update(Request $request){
        $admin = new Admin();
        $ws = new WebService();
        $permission = new Permission();

        $result = '';

        foreach(array("name", "privileges", "id") as $field){
            $data[$field] = $request->$field;
        }

        try{
            $info = $permission->get_permission_detail($data['id']);
        }catch(CustomValidationException $e){   
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $permission->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        try{
            $permission->validate_update_permission($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '6000006200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $permission->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $permission->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }
       
        try{
            $permission->update_permission($data);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                   
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $permission->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $info));

        return $result;
    }

    public function delete(Request $request)
    {
        $ws = new WebService();
        $permission = new Permission();

        $result = '';

        foreach(array("id") as $field){
            $data[$field] = $request->$field;
        } 

        try{
            $info = $permission->validate_get_permission($data);
        }catch(CustomValidationException $e){   
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $permission->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        try{
            $permission->delete_permission($data);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                   
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $permission->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }
        
        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $data)); 

        return $result;
    }

    public function get_privileges_list(Request $request)
    {
        $permission = new Permission();
        $ws = new WebService();

        $privilegesList = $permission::$api_privileges_list;

        $privilegesResult = [];
		$privilegesDetail = [];
		
		if(!empty($privilegesList)){
			foreach($privilegesList as $pname => $pdetail){
				if(!empty($pdetail['sub'])){
					foreach($pdetail['sub'] as $action => $actionDetail){
						$privilegesDetail[] = (object) array('id' => $actionDetail['id'], "name" => $actionDetail['name'], 'label' => $actionDetail['label']);
					}
				}				
				$privilegesResult[] = (object) array("id" => $pdetail['id'], "name" => $pname, 'label' => $pdetail['label'], "sub" => $privilegesDetail);
				$privilegesDetail = [];
			}
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array('admin_privileges_list' => $privilegesResult));

        return $result;
    }
}

?>