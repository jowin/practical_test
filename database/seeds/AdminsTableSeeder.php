<?php

use Illuminate\Database\Seeder;
use App\Admin;
use Carbon\Carbon;
use Illuminate\Support\Str;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(Admin::$admin_table)->insert([
            'uid' => 2,
            'name' => "Developer",
            'mobile' => "0129232123",
            'master' => 1,
            'dev' => 1,
            'privileges' => '',
            'email' => 'dev@'.strtolower(config('app.domain')),
            'password' => bcrypt('qwe123'),
            'remember_token' => '',
            'api_token' => Str::random(60),
            'api_token_expire_date' => Carbon::now(config('app.server_timezone'))->addYear(1)->toDateTimeString(),
            'status' => Admin::$status_to_code['active'],
            'created_at' => Carbon::now(config('app.server_timezone'))->toDateTimeString(),
            'updated_at' => Carbon::now(config('app.server_timezone'))->toDateTimeString(),
        ]);

        DB::table(Admin::$admin_table)->insert([
            'uid' => 3,
            'name' => "client",
            'mobile' => "0119283920",
            'master' => 1,
            'dev' => 0,
            'privileges' => '',
            'email' => 'client@'.strtolower(config('app.domain')),
            'password' => bcrypt('qwe123'),
            'remember_token' => '',
            'api_token' => Str::random(60),
            'api_token_expire_date' => Carbon::now(config('app.server_timezone'))->addYear(1)->toDateTimeString(),
            'status' => Admin::$status_to_code['active'],
            'created_at' => Carbon::now(config('app.server_timezone'))->toDateTimeString(),
            'updated_at' => Carbon::now(config('app.server_timezone'))->toDateTimeString(),
        ]);

        DB::table(Admin::$admin_table)->insert([
            'uid' => 4,
            'name' => "admin",
            'mobile' => "0119283920",
            'master' => 0,
            'dev' => 0,
            'privileges' => '',
            'email' => 'admin@'.strtolower(config('app.domain')),
            'password' => bcrypt('qwe123'),
            'remember_token' => '',
            'api_token' => Str::random(60),
            'api_token_expire_date' => Carbon::now(config('app.server_timezone'))->addYear(1)->toDateTimeString(),
            'status' => Admin::$status_to_code['active'],
            'created_at' => Carbon::now(config('app.server_timezone'))->toDateTimeString(),
            'updated_at' => Carbon::now(config('app.server_timezone'))->toDateTimeString(),
        ]);
    }
}
