import Config from '@/js/config/app.js';
import axios from 'axios';
import Router from '@/js/routes';

export default {
    namespaced: true,
    state: {
        authInfo: {
            token: null,
            expireDate: null,
            isAuthenticated: null,
            status: null,
            message: null,
        },
        memberInfo: { 
            id: null,
            uid: null,
            name: null,
            email: null,
        },
    },
    mutations: {
        saveAuthInfo(state, memberData){
            localStorage.setItem('loginToken', memberData.loginToken); 
            localStorage.setItem('loginTokenExpireDate', memberData.loginTokenExpireDate); 
            state.authInfo.token = memberData.loginToken;
            state.authInfo.expireDate = memberData.loginTokenExpireDate;
            state.authInfo.status = Config.generalStatus["success"];
            state.authInfo.message = 'success';
            state.authInfo.isAuthenticated = true;           
        },
        saveMemberInfo(state, memberData){
            if(memberData.saveLocal){   
                localStorage.setItem('memberInfo', JSON.stringify(memberData.memberInfo));
            }
            state.memberInfo.id = (memberData.memberInfo.id != undefined)? memberData.memberInfo.id : state.memberInfo.id;
            state.memberInfo.uid = (memberData.memberInfo.uid != undefined)? memberData.memberInfo.uid : state.memberInfo.uid;
            state.memberInfo.name = (memberData.memberInfo.name != undefined)? memberData.memberInfo.name : state.memberInfo.name;
            state.memberInfo.email = (memberData.memberInfo.email != undefined)? memberData.memberInfo.email : state.memberInfo.email;
        },
        errorAuthInfo(state, validateLanguage){
            state.authInfo.token = null;
            state.authInfo.expireDate = null;
            state.authInfo.status = Config.generalErrorStatus;
            state.authInfo.message = validateLanguage['auth/failed']; 
            state.authInfo.isAuthenticated = false;
        },
    },
    getters: {        
    },
    actions: {   
        login({state, commit, rootState}, authData){      
            axios.post(Config.base_url() + 'member/login', authData)
                .then(res => {
            	if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    const loginToken = res['data']['data']['auth']['api_token'];
                    const loginTokenExpireDate = res['data']['data']['auth']['api_token_expire_date'];
                    const aInfo = res['data']['data']['profile'];         
                    commit('saveAuthInfo', {loginToken: loginToken, loginTokenExpireDate: loginTokenExpireDate});
                    commit('saveMemberInfo', {memberInfo: aInfo, saveLocal: true});
                    Router.replace({name: "member-dashboard"});
                    Router.go({name: "member-dashboard"});
            	} else {
                    commit('errorAuthInfo', authData.vlanguage);
                }
                
            })
            .catch(error => console.log(error));
        },
    },

}