const validation_zh_CN = {
    'field/validation/required': '{field}是必填的。',
    'field/validation/text/invalid': '请填写正当的{field}',
    'field/validation/dropdown/invalid': '请选择正当的{field}',

    'auth/failed': '这些凭据与我们的记录不匹配',
}

export default validation_zh_CN;