<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Merchant;

class MerchantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(Merchant::$merchant_table)->insert([
            'uid' => 1,
            'name' => "Merchant",
            'email' => 'merchant@'.strtolower(config('app.domain')),
            'password' => bcrypt('qwe123'),
            'remember_token' => '',
            'api_token' => Str::random(60),
            'api_token_expire_date' => Carbon::now(config('app.server_timezone'))->addYear(1)->toDateTimeString(),
            'forgot_password_vcode' => '',
            'register_vcode' => '',
            'register_vcode_cdate' => '',
            'status' => Merchant::$status_to_code['active'],
            'created_at' => Carbon::now(config('app.server_timezone'))->toDateTimeString(),
            'updated_at' => Carbon::now(config('app.server_timezone'))->toDateTimeString(),
        ]);
    }
}
