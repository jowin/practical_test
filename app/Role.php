<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\WebService;
use Validator;
use Carbon\Carbon;
use App\Exceptions\CustomValidationException;
use DB;
use App\Rules\AdminExtraValidation;

class Role extends Model
{
    protected $table = "roles";

    public $err_query_info = ["code" => "", "message" => "", "params" => array()];
    
    public $custom_err_info = ["code" => "", "message" => "", "params" => array()];

    public $cust_err_field_params = array();

    protected $fillable = [
        'id', 'name', 'permission_id', 'created_at', 'updated_at'
    ];

    public static $item_per_page = 5;

    protected $class_code = "6500";

    protected $insert_rules = [
        'name' => 'required',
        'permission_id' => 'required',
    ];

    protected $update_rules = [
        'name' => 'required',
        'permission_id' => 'required',
    ];

    public function get_role_info($filterData){
        $method_code = "001";
        $method_name = "get-role-info";

        $ws = new WebService();
       
        try{
            $result = DB::table("roles")->where($filterData['asearch'])->where(function($query) use ($filterData){     
                if(!empty($filterData['qsearch'])){
                    foreach($filterData['qsearch'] as $field => $info){
                        $query->orWhere($field, $info['search_type'], $info['value']);    
                    }
                }elseif(!empty($filterData['asearch_extra'])){
                    foreach($filterData['asearch_extra'] as $index => $value){                    
                        $query->orWhere($value[0], $value[1], $value[2]);
                    }
                }else{
                    $query->orWhere([]);
                }
            })->paginate(static::$item_per_page);  
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
        
        $info = array(
            'role_list_info' => $result,
        );
        
        $result = $ws->api_result($ws::$api_code_to_status['success'], $info); 

        return $result;
    }

    public function validate_add_role($data){
        $method_code = "001";
        $method_name = "validate-add-role";

        $messages = [];     
        $validator = Validator::make($data, $this->insert_rules, $messages); 

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }
    }

    public function add_role($data){
        $method_code = "002";
        $method_name = "add-role";
    
        $data['created_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();   
        $data['updated_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();
        
        $this->validate_add_role($data);

        // $data['permission_id'] = json_encode($data['permission_id']);
        try{
            $pid = Role::insertGetId($data);
        }catch(QueryException $e){
            dd($e->getMessage());
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }  
        
        return $pid;
    }

    public function get_role_detail($id = ''){
        $method_code = "003";
        $method_name = "get-role-detail";

        try{
            $result = Role::where(['id' => $id])->first();
            $result = json_decode($result, true);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomQueryException($method_name, $this->class_code.$method_code.'001');
        }

        return $result;
    }
    
    public function validate_get_role($data){
        $method_code = '004';
        $method_name = 'validate-get-role';

        $validationRules['id'] = ['required', 'numeric'];

        $messages = [];        
        $validator = Validator::make($data, $validationRules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'001');
        }
    }

    public function get_role($data){
        $method_code = '005';
        $method_name = 'get-role';

        $this->validate_get_role($data);
       
        try{ 
            $result = Role::paginate(static::$item_per_page);                
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }

        return $result;
    }

    public function validate_update_role($data){
        $method_code = "006";
        $method_name = "validate-update-role";

        $messages = [];

        $validator = Validator::make($data, $this->update_rules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }

        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;
            /*
            * @param message - The Exception message to throw.
            * @param code - The Exception code.
            * @param previous - The previous exception used for the exception chaining.
            */
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }
    }

    public function update_role($data){
        $method_code = "007";
        $method_name = "update-role";

        $this->validate_update_role($data);

        $data['updated_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();   

        $udata = $data;
        
        try{
            Role::where('id', $data['id'])->update(['name' => $data['name'], 'permission_id' => $data['permission_id'], 'updated_at' => $data['updated_at']]);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        } 
    }

    public function delete_role($data)
    {
        $method_code = "008";
        $method_name = "delete-role";

        $this->validate_get_role($data);

        try{
            Role::where('id', $data['id'])->delete();
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        } 
    }

    public function validate_get_role_info($data){
        $method_code = "009";
        $method_name = 'validate-get-role-info';   

        $validationRules = array();
        $validationRules['admin_uid'] = ['required', 'numeric', new AdminExtraValidation];

        $messages = [];        
        $validator = Validator::make($data, $validationRules, $messages);
        
        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'001');
        }
    }

    public function get_permission_list(){
        $method_code = "010";
        $method_name = 'get-permission-list';

        try{
            $result = Permission::get()->toArray();
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        } 

        return $result;
    }
}
