<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Illuminate\Support\Facades\Hash;
use Auth;

class SystemController extends Controller
{
    public function down(){
        return view('maintenance.down');
    }

    public function confirm_down(Request $request){
        $email = $request->email;
        $password = $request->password;

        $encry_password = Hash::make($password);
        
        $admin_logged_in = Auth::guard('admin')->check();

        if($admin_logged_in){
            # check is master admin
            if(Auth::guard('admin')->user()->master){
                touch(storage_path().'/framework/down');
                return "Website is down...";
            }else{
                return "You don't have privileges";
            }
        }else{
            if(Auth::guard('admin')->attempt(['email' => $email, 'password' => $password])){
                if(Auth::guard('admin')->user()->master){
                    touch(storage_path().'/framework/down');
                    return "Website is down...";
                }else{
                    return "You don't have privileges"; 
                }
            }else{
                return "Invalid email or password.";
            }
        }
        
        return "error";
    }

    public function up(){
        return view('maintenance.up');
    }

    public function confirm_up(Request $request){
        $email = $request->email;
        $password = $request->password;

        $encry_password = Hash::make($password);
        
        $admin_logged_in = Auth::guard('admin')->check();

        if($admin_logged_in){
            # check is master admin
            if(Auth::guard('admin')->user()->master){
                @unlink(storage_path().'/framework/down');
                return "Website is up";
            }else{
                return "You don't have privileges";
            }
        }else{
            if(Auth::guard('admin')->attempt(['email' => $email, 'password' => $password])){
                if(Auth::guard('admin')->user()->master){
                    @unlink(storage_path().'/framework/down');
                    return "Website is up";
                }else{
                    return "You don't have privileges"; 
                }
            }else{
                return "Invalid email or password";
            }
        }

        return "error";
    }

}
