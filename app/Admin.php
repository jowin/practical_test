<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPasswordNotification;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use Illuminate\Database\QueryException;
use App\Rules\AdminExtraValidation;
use App\Rules\AuthExtraValidation;
use Validator;
use Carbon\Carbon;
use DB;
use Auth;
use GuzzleHttp\Client;
use Laravel\Passport\HasApiTokens;
use App;

class Admin extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $guard = 'admin';

    protected $table = "admins";

    public static $prefix = "admin";    

    public static $status_to_code = array(
        'active' => 10,
        'suspend' => 90,
        'terminited' => 100,
    );
	
	public static $code_to_text = array(
        10 => "trans.person/status/active",
        90 => "trans.person/status/suspend",
        100 => "trans.person/status/terminated"
    );

    public static $code_to_status = array(
        10 => 'active',
        90 => 'suspend',
        100 => 'terminated',
    );

    public static $admin_table = "admins";

    protected $class_code = "2000";

    public $err_query_info = ["code" => "", "message" => "", "params" => array()];
    
    public $custom_err_info = ["code" => "", "message" => "", "params" => array()];

    public $cust_err_field_params = array();

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'mobile', 'master', 'privileges', 'role_id', 'email', 'password', 'remember_token', 'api_token', 'api_token_expire_date', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }

    public static $unprivileges_page = array("admin.logout", "admin.dashboard", "admin.profile");

    protected $insert_rules = [
        'name' => 'required',
        'password' => 'required|min:6',
        'mobile' => 'required|min:10|numeric',
    ];

    protected $update_rules = [
        'name' => 'nullable',
        'mobile' => 'nullable|min:10|numeric', 
    ];

    protected $update_profile_rules = [        
        'name' => 'required',
        'mobile' => 'required|min:10|numeric', 
        //'password' => 'nullable|min:6',
    ];

    protected $login_rules = [
        'email' => 'required|email', 
        'password' => 'required', 
    ];

    protected $update_password_rules = [
        'password' => "required|min:6",
        'confirm_password' => "required_with:password|same:password", 
    ];

	protected $dev_admin_uid = 2;

    public static $item_per_page = 5;

    /*
    * All Admin Privileges List
    * @return privilege info 
    */
    public static $privileges_list = [
        "admin-module" => array(
            "label" => "trans.admin/privileges/admin-module",
            "sub" => array(
                "view" => array(
                    "id" => 1000,
                    "name" => "view",
                    "label" => "trans.admin/privileges/admin-module/label/view",
                    "url_name" => "admin.admin-module.index",
                    "extra_url_name" => ["admin.admin-module.indexData", "admin.admin-module.detail", "admin-module.listIndex"],
                ),
                "insert" => array(
                    "id" => 1001,
                    "name" => "insert",
                    "label" => "trans.admin/privileges/admin-module/label/insert",
                    "url_name" => "admin.admin-module.create",
                    'extra_url_name' => '',
                ),                
                "edit" => array(
                    "id" => 1002,
                    "name" => "edit",
                    "label" => "trans.admin/privileges/admin-module/label/edit",
                    "url_name" => "admin.admin-module.edit",
                    'extra_url_name' => '',
                )
            ),
        ),

        
        "anouncement" => array(
            "label" => "trans.admin/privileges/anouncement",
            "sub" => array(
                "view" => array(
                    "id" => 1100,
                    "name" => "view",
                    "label" => "trans.admin/privileges/label/view",
                    "url_name" => "admin.anouncement.index",
                    "extra_url_name" => "admin.anouncement.detail",
                ),
                "insert" => array(
                    "id" => 1101,
                    "name" => "insert",
                    "label" => "trans.admin/privileges/label/insert",
                    "url_name" => "admin.anouncement.create",
                ),                
                "edit" => array(
                    "id" => 1102,
                    "name" => "edit",
                    "label" => "trans.admin/privileges/label/edit",
                    "url_name" => "admin.anouncement.edit",
                )
            ),
        ),
        
        
    ];

    public static $api_privileges_list = [
        "admin-modules" => array(
			"id" => 1000,
            "label" => "admin/privileges/admin-module", // translate key in js files.
            "sub" => array(
                "view" => array(
                    "id" => 1000,
                    "name" => "view",
                    "label" => "admin/privileges/label/view", // translate key in js files.
                    "url_name" => "admin.admin-module.index", // name in routes.js
                    "extra_url_name" => ["admin.admin-module.indexData", "admin.admin-module.detail", "admin-module.listIndex"], // name in routes.js
                ),
                "insert" => array(
                    "id" => 1001,
                    "name" => "insert",
                    "label" => "admin/privileges/label/insert", // translate key in js files.
                    "url_name" => "admin.admin-module.create", // name in routes.js
                    'extra_url_name' => '',
                ),                
                "edit" => array(
                    "id" => 1002,
                    "name" => "edit",
                    "label" => "admin/privileges/label/edit",  // translate key in js files.
                    "url_name" => "admin.admin-module.edit", // name in routes.js
                    'extra_url_name' => '',
                )
            ),
        ),
        
        "anouncement" => array(
            "id" => 1100,
            "label" => "admin/privileges/anouncement",
            "sub" => array(
                "view" => array(
                    "id" => 1100,
                    "name" => "view",
                    "label" => "admin/privileges/label/view",
                    "url_name" => "admin.anouncement.index",
                    "extra_url_name" => "admin.anouncement.detail",
                ),
                "insert" => array(
                    "id" => 1101,
                    "name" => "insert",
                    "label" => "admin/privileges/label/insert",
                    "url_name" => "admin.anouncement.create",
                ),                
                "edit" => array(
                    "id" => 1102,
                    "name" => "edit",
                    "label" => "admin/privileges/label/edit",
                    "url_name" => "admin.anouncement.edit",
                )
            ),
        ),
             
    ];

    public function generate_unique_id()
    {
        $random_id = 0;
        // $random_id = mt_rand(00000000, 99999999);
        do{
            $random_id = mt_rand(00000000, 99999999);
            $info = DB::table('admins')->select('uid')->where('uid', $random_id)->get();
        }while(!$info->isEmpty());

        return $random_id;
    }

    public function add_admin($data){
        $method_code = "002";
        $method_name = "add-admin";
    
        $data['uid'] = $this->generate_unique_id();
        $data['created_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();   
        $data['updated_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();
        $data['master'] = $data['dev'] = 0;
        $data['status'] = static::$status_to_code['active'];
        
        $this->validate_add_admin($data);

        $data['privileges'] = '';
        $data['password'] = bcrypt($data['password']);
        $data['remember_token'] = "";

        try{
            $aid = DB::table($this::$admin_table)->insertGetId($data);
        }catch(QueryException $e){
            dd($e->getMessage());
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }  

        return $aid;
    }

    public function validate_add_admin($data){
        $method_code = "001";
        $method_name = "validate-add-admin";

        $messages = [];
        $this->insert_rules['email'] = ['required', 'email', 'unique:admins', new AuthExtraValidation];
        $this->insert_rules['role_id'] = ['required'];
        // $this->insert_rules['privileges'] = ['nullable', 'array', new AdminExtraValidation];       
        $validator = Validator::make($data, $this->insert_rules, $messages); 

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }

            
        // if($validator->fails()){
            // $this->err_field_params = $validator->errors();

            /*
            * @param message - The Exception message to throw.
            * @param code - The Exception code.
            * @param previous - The previous exception used for the exception chaining.
            */
            /*throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');*/
        // }
        
    }

    public function get_all_admin_module(){
        $method_code = "003";
        $method_name = "get-all-admin-module";

        if(!has_previleges("admin.admin-module.indexData")){
            $this->custom_err_info['code'] = $this->class_code.$method_code.'001';
            $this->custom_err_info['message'] = trans('trans.admin/privileges/error/message');
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
        
        $filter = (admin()->uid == $this->dev_admin_uid)? [] : [$this->dev_admin_uid];
        
        try{
            $result = DB::table($this::$admin_table)->select(['uid', 'master', 'dev', 'name', 'email', 'mobile', 'status', 'created_at', 'updated_at'])->whereNotIn("uid", $filter)->get();
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomQueryException($method_name, $this->class_code.$method_code.'002');
        }

        return $result;
    }

    public function get_admin_module_detail($uid = '',$id = ''){
        $method_code = "004";
        $method_name = "get-admin-module-detail";

        try{
            $result = Admin::where(['uid'=> $uid])->orWhere(['id' => $id])->first();
            $result = json_decode($result, true);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomQueryException($method_name, $this->class_code.$method_code.'001');
        }

        return $result;
    }  

    public function validate_update_admin($data){
        $method_code = "005";
        $method_name = "validate-update-admin";

        $messages = [];
        
        $this->update_rules['admin_uid'] = ['required', 'numeric', new AdminExtraValidation];
        // $this->update_rules['privileges'] = ['nullable', 'array', new AdminExtraValidation];
        $this->update_rules['status'] = ['nullable', 'numeric', new AdminExtraValidation];
        $this->update_rules['email'] = 'nullable|email|unique:admins,email,'.$data['id'];
        $this->update_rules['role_id'] = ['required'];
        
        foreach(array('name', 'email', 'mobile') as $required_field){
            if((array_key_exists($required_field, $data)) && (is_null($data[$required_field])) && (empty($data[$required_field])) ){
                $this->update_rules[$required_field] = ['required'];
            }
        }
        
        if(array_key_exists("password", $data)){
            $this->update_rules['password'] = ['required', 'min:6'];
        }

        $validator = Validator::make($data, $this->update_rules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }

        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;
            /*
            * @param message - The Exception message to throw.
            * @param code - The Exception code.
            * @param previous - The previous exception used for the exception chaining.
            */
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }
    }

    public function update_admin($data){
        $method_code = "006";
        $method_name = "update-admin";

        $this->validate_update_admin($data);

        if(array_key_exists("password", $data)){
            $data['password'] = bcrypt($data['password']);
        }

        if(array_key_exists("privileges", $data)){
            $data['privileges'] = json_encode($data['privileges']);
        }

        $data['updated_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();   

        $udata = $data;
        
        unset($udata['admin_uid'], $udata['aid'], $udata['email']);
        
        try{
            DB::table($this::$admin_table)->where('uid', $data['aid'])->update($udata);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
    }

    public function validate_update_profile($data){
        $method_code = "007";
        $method_name = "validate-profile";

        $this->update_profile_rules['admin_uid'] = ['required', 'numeric', new AdminExtraValidation];
        $this->update_profile_rules['email'] = 'required|email|unique:admins,email,'.$data['admin_uid'].',uid';

        
        // $this->update_profile_rules['password'] = ['required', 'min:6'];
        // $this->update_profile_rules['confirm_password'] = ['required_with:password', 'same:password'];

        foreach(array('name', 'email', 'mobile') as $required_field){
            if((array_key_exists($required_field, $data)) && (is_null($data[$required_field])) && (empty($data[$required_field])) ){
                $this->update_profile_rules[$required_field] = ['required'];
            }
        }

        $messages = [];      
        
        $validator = Validator::make($data, $this->update_profile_rules, $messages);
        
        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }

        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;
            /*
            * @param message - The Exception message to throw.
            * @param code - The Exception code.
            * @param previous - The previous exception used for the exception chaining.
            */
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }
    }

    public function update_profile($data){
        $method_code = "008";
        $method_name = "update-profile";

        $this->validate_update_profile($data);

        if(array_key_exists("password", $data)){
            $data['password'] = bcrypt($data['password']);
        }

        if(array_key_exists("privileges", $data)){
            $data['privileges'] = json_encode($data['privileges']);
        }

        $udata = $data;
        
        unset($udata['admin_uid']);
        
        try{
            DB::table($this::$admin_table)->where('uid', $data['admin_uid'])->update($udata);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
    }

    public function get_all_admin_list($filterData){
        $method_code = "009";
        $method_name = "get-all-admin-list";
        
        if(!has_previleges("admin.admin-module.index")){
            $this->custom_err_info['code'] = $this->class_code.$method_code.'001';
            $this->custom_err_info['message'] = trans('trans.admin/privileges/error/message');
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
        try{            
            $result = DB::table($this::$admin_table)->where($filterData['asearch'])->where(function($query) use ($filterData){     
                if(!empty($filterData['qsearch'])){
                    foreach($filterData['qsearch'] as $field => $value){
                        $query->orWhere($field, '=', $value);
                    }
                }elseif(!empty($filterData['asearch_extra'])){
                    foreach($filterData['asearch_extra'] as $index => $value){                    
                        $query->orWhere($value[0], $value[1], $value[2]);
                    }
                }else{
                    $query->orWhere([]);
                }
            })->when( (!empty($filterData['sort_info'])), function($sort_query, $filterData) {
                foreach(System::$searchFilterData['sort_info'] as $sort_field => $sort_detail){
                    if(!empty($sort_detail['sort_type'])){
                        $sort_query->orderBy($sort_field, $sort_detail['sort_type']);
                    }
                }
            })->paginate(static::$item_per_page);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomQueryException($method_name, $this->class_code.$method_code.'002');
        }
    
        return $result;
    }

    public function login($data){
        $method_code = "010";
        $method_name = "login";
        
        $this->validate_login($data);

        $result = [];

        if(Auth::guard('admin')->attempt(['email' => $data['email'], 'password' => $data['password']])){
            $ws = new WebService();
            $ws_user = new WebServiceUser();

            $admin = Auth::guard('admin')->user();
            $rdata = array();
            
            // check previous time has login token or not            
            $currentTokenInfo = $ws->retrieve_user_token(static::$admin_table, $admin->uid);

            if(!empty($currentTokenInfo['api_token']) && isset($currentTokenInfo['api_token'])){
                try{
                    $tokenInfo = $ws->check_login_token(static::$admin_table, $admin->uid);
                }catch(CustomGeneralException $e){
                    $this->err_query_info['code'] = $e->getCode();
                    $this->err_query_info['message'] = $e->getMessage();
                    $this->err_query_info['params'] = $e->getBindings();
                    throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
                }

                $rdata = array(
                    "profile" => $admin->get_admin_module_detail($admin->uid),
                    "auth" => $tokenInfo,
                );                
                $result = $ws->api_result($ws::$api_code_to_status['success'], $rdata);
                return $result;
            }
            
            try{
                $loginTokenInfo = $ws->generate_new_token(static::$admin_table, $admin->uid);
            }catch(CustomGeneralException $e){
                $this->err_query_info['code'] = $e->getCode();
                $this->err_query_info['message'] = $e->getMessage();
                $this->err_query_info['params'] = $e->getBindings();
                throw new CustomGeneralException($method_name, $this->class_code.$method_code.'002');
            }

            $adata = array(
                "profile" => $this->get_admin_module_detail($admin->uid),
                "auth" => $loginTokenInfo,
            );
            
            $result = $ws->api_result($ws::$api_code_to_status['success'], $adata);            
        }
        
        return $result;
        
    }

    public function validate_login($data){
        $method_code = "010";
        $method_name = "validate-login";

        $messages = [];
        $validator = Validator::make($data, $this->login_rules, $messages);

        if($validator->fails()){
            $this->err_field_params = $validator->errors();
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }
    }

    public function update_password($data){
        $method_code = "011";
        $method_name = "update-password";

        $this->validate_update_password($data);

        $data['password'] = bcrypt($data['password']);

        $udata = $data;
        
        unset($udata['admin_uid'], $udata['wsid'], $udata['confirm_password']);
        
        try{
            DB::table($this::$admin_table)->where('uid', $data['admin_uid'])->update($udata);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
    }

    public function validate_update_password($data){
        $method_code = "012";
        $method_name = "validate-update-password";

        $this->update_password_rules['admin_uid'] = ['required', 'numeric', new AdminExtraValidation];
        
        $messages = [];        
        $validator = Validator::make($data, $this->update_password_rules, $messages);
        
        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }
    }

    public function validate_get_admin_modules_info($data){
        $method_code = "013";
        $method_name = 'validate-get-admin-modules-info';   

        $validationRules = array();
        $validationRules['admin_uid'] = ['required', 'numeric', new AdminExtraValidation];

        $messages = [];        
        $validator = Validator::make($data, $validationRules, $messages);
        
        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'001');
        }
    }

    public function get_admin_modules_info($filterData){
        $method_code = "014";
        $method_name = "get-admin-modules-info";

        $ws = new WebService();
       
        try{ 
            $result = DB::table($this::$admin_table)->where($filterData['asearch'])->where('uid', '!=', $filterData['admin_uid'])->where(function($query) use ($filterData){     
                if(!empty($filterData['qsearch'])){
                    foreach($filterData['qsearch'] as $field => $info){
                        $query->orWhere($field, $info['search_type'], $info['value']);    
                    }
                }elseif(!empty($filterData['asearch_extra'])){
                    foreach($filterData['asearch_extra'] as $index => $value){                    
                        $query->orWhere($value[0], $value[1], $value[2]);
                    }
                }else{
                    $query->orWhere([]);
                }
            })->when( (!empty($filterData['sort_info'])), function($sort_query, $filterData) {
                foreach(System::$searchFilterData['sort_info'] as $sort_field => $sort_detail){
                    if(!empty($sort_detail['sort_type'])){
                        $sort_query->orderBy($sort_field, $sort_detail['sort_type']);
                    }
                }
            })->paginate(static::$item_per_page);                
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
        
        foreach($result as $li => $info){
            $result[$li]->status = static::$code_to_status[$info->status];
        }
        
        $info = array(
            'admin_list_info' => $result,
        );
        
        $result = $ws->api_result($ws::$api_code_to_status['success'], $info); 

        return $result;
    }

    public function validate_get_admin_status_list($data){
        $method_code = '015';
        $method_name = 'validate-get-admin-status-list';

        $validationRules = array();
        $validationRules['admin_uid'] = ['required', 'numeric', new AdminExtraValidation];
        
        $messages = [];        
        $validator = Validator::make($data, $validationRules, $messages);
        
        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'001');
        }

    }

    public function get_admin_status_list($data){
        $method_code = '016';
        $method_name = 'get-admin-status-list';

        $result = array();

        try{
            $statusList = static::$code_to_status;
            foreach($statusList as $key => $value){
                array_push($result, array('id' => $key, 'name' => $value));
            }
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }

        return $result;
    }

    public function validate_get_admin_privileges_list($data){
        $method_code = "017";
        $method_name = 'validate-get-admin-privileges-list';
        
        $validationRules = array();
        $validationRules['admin_uid'] = ['required', 'numeric', new AdminExtraValidation];
        
        $messages = [];        
        $validator = Validator::make($data, $validationRules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'001');
        }
    }

    public function validate_get_admin($data){
        $method_code = '018';
        $method_name = 'validate-get-admin';

        $validationRules['admin_uid'] = ['required', 'numeric', new AdminExtraValidation];

        $messages = [];        
        $validator = Validator::make($data, $validationRules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'001');
        }
    }

    public function get_admin($data){
        $method_code = '019';
        $method_name = 'get-admin';

        $this->validate_get_admin($data);
       
        try{ 
            $result = DB::table($this::$admin_table)->where('uid', $data['aid'])->paginate(static::$item_per_page);                
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }

        return $result;
    }

    public function get_admin_privileges($data){
        $method_code = '020';
        $method_name = 'get_admin_privileges';
        
        $this->validate_get_admin_privileges($data);
		
		try{
			$result = DB::table($this::$admin_table)->select("dev", "master", "privileges")->where('uid', $data['admin_uid'])->paginate(static::$item_per_page);
		}catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
		
		return $result;
    }

    public function validate_get_admin_privileges($data)
    {
        $method_code = '021';
        $method_name = 'validate_get_admin_privileges';

        $validationRules['admin_uid'] = ['required', 'numeric', new AdminExtraValidation];

        $messages = [];    

        $validator = Validator::make($data, $validationRules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'001');
        }
    }
}
