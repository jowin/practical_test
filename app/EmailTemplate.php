<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EmailTemplateContent;

class EmailTemplate extends Model
{
    protected $table = 'email_template';

    public $timestamps = false;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'code', 'title', 'status', 'created_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function email_template_contents()
    {
        return $this->hasMany('App\EmailTemplateContent', 'eid', 'code');
    }
}
