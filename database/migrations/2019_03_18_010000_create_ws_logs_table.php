<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWsLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ws_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wsid', 100);
            $table->text('last_response');
            $table->text('url');
            $table->text('post_data');
            $table->text('get_data');
            $table->unsignedSmallInteger('last_status_code')->nullable();
            $table->datetime('created_at');
            $table->datetime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ws_logs');
    }
}