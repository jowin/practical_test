import Config from '@/js/config/app.js';
import Router from '@/js/routes';
import axios from 'axios';

export default {
    namespaced: true,
    state: {
        searchResult: '',
        privilegesList: '',
        infoSignal: {
            addSignal: '',   
            updateSignal: '',
            deleteSignal: '',
            quickSearchSignal: '',
            advanceSearchSignal: '',
        }, 
        field_error: {},
        isShowingPermissionInfo: '',
    },
    mutation: {
    },
    getters: {
    },
    actions: {
        retrivePermissionList({state, commit, rootState}, incomingData){
            axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('loginToken');
            axios.post(Config.base_url() + 'admin/admin-permission/index', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['permission_list_info'];
                }
            })
            .catch(error => console.log(error));             
        },
        retrivePrivilegesList({state, commit, rootState}, incomingData){
            axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('loginToken');
            axios.post(Config.base_url() + 'admin/admin-permission/get-privilege-list', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.privilegesList = '';
                    state.privilegesList = res['data']['data']['admin_privileges_list'];
                }
            })
            .catch(error => console.log(error));             
        },

        executeQuickSearch({state, commit, rootState}, incomingData){         
			axios.post(Config.base_url() + 'admin/admin-permission/index', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['permission_list_info'];
                    state.infoSignal.quickSearchSignal = true;
                }
            })
            .catch(error => console.log(error));            
        },

        executeAdvanceSearch({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-permission/index', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['permission_list_info'];
                    state.infoSignal.advanceSearchSignal = true;
                }
            })
            .catch(error => console.log(error));    
        },

        switchPage({state, commit, rootState}, incomingData){  
            axios.post(incomingData.url, incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['permission_list_info'];
                }
            })
            .catch(error => console.log(error));             
        },
        
        /* Create */       
        insertPermission({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-permission/add-permission', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.infoSignal.addSignal = Config.generalStatus['success'];
                    Router.push({ name: "admin-permission"});
                }else {
                    state.field_error = res['data']['field_error'];
                    state.infoSignal.addSignal = Config.generalStatus['error'];
                }
            })
            .catch(error => console.log(error)); 
        },
        /* End Create */

        /* Permission Info */
        retreivePermissionInfo({state, coimmit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-permission/get-permission-info', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.isShowingPermissionInfo = res['data']['data']['info'];
                }  
            })
            .catch(error => console.log(error));
        },
        /* End Permission Info */ 
        
        /* updatePermission */
        updatePermission({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-permission/update-permission', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.infoSignal.updateSignal = Config.generalStatus['success'];
                    Router.push({ name: "admin-permission"});
                }else {
                    state.field_error = res['data']['field_error'];
                    state.infoSignal.updateSignal = Config.generalStatus['error'];
                }
            })
            .catch(error => console.log(error)); 
        },

        /* Delete Permission */
        deletePermission({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-permission/delete-permission', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.infoSignal.deleteSignal = Config.generalStatus['success'];
                    Router.push({ name: "admin-permission"});
                }else {
                    state.field_error = res['data']['field_error'];
                    state.infoSignal.deleteSignal = Config.generalStatus['error'];
                }
            })
            .catch(error => console.log(error));
        } 
    }
}