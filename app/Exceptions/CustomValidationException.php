<?php

namespace App\Exceptions;

use Exception;

class CustomValidationException extends Exception
{
    public function report(){
    }

    public function render($request){
    	// dd($this);
    	return $this->code;
    }
}
