STEP
1) setup the database in localhost, run the practicaltest.sql that in root directory of project folder.
2) setup database configuration in config/database.php
3) run composer install in command prompt.
4) run npm install in command prompt.
5) run php artisan serve in command prompt.
6) run npm run hot in command prompt.
7) open broswer and run 127.0.0.1:8000
8) SuperAdmin account email is dev@domain.com, password is qwe123
9) Admin account email is admin@domain.com, password is qwe123
10) User account email is jack@domain.com, password is qwe123
