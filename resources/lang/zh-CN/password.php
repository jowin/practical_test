<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => '密码必须六个字以上和确定密码相同',
    'reset' => '您的密码已经成功重设',
    'sent' => '您的邮件已经收到重设密码',
    'token' => '密码记号错误',
    'user' => "邮件不存在",

];

?>