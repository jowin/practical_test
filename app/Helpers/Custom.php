<?php 

/* Start Custom Helper */
/*
* return route name filter by current language.
*/
if (! function_exists('locale_route')) {
    
    function locale_route($name, $parameters = [], $absolute = true)
    {
        $locale = Request::segment(1);

        $locale = (! in_array($locale, Config::get('app.locales')))? '' : $locale . '.';
        
        return route($locale.$name, $parameters, $absolute);
    }
}

if (! function_exists('internal_asset')) {
    
    function internal_asset($path)
    {    
		$ori_path = substr($path, 0, -3);
        $extension = substr($path, -3);

        if(in_array($extension, Config::get('app.media_extension'))){
            $path = $ori_path . Config::get('app.media_version') . "." .  $extension; 
        }
		
        if(strpos(asset($path), 'public') !== false){
            $url = str_replace('/public', '', asset('/resources/assets'));
            $complete_url = $url . '/' . $path;
        }else{
            $complete_url = asset('/resources/assets').$path;
        }
       
        return $complete_url;
    }
}

if (! function_exists('admin')) {
    
    function admin()
    {
        if(!empty(Auth()->guard('admin')->user())){
            return Auth()->guard('admin')->user();
        }
        return null;
    }
}

if (! function_exists('has_previleges')) {
    
    function has_previleges($action_id = 0)
    {
        $result = false;        
        
        if(Auth::guard('admin')->user()->master == 1){
            $result = true;
        }else{
            $admin_privileges = json_decode(Auth::guard('admin')->user()->privileges);
            
            $all_privileges = Privileges::$privileges_list; 
            
            foreach($all_privileges as $privileges_info){
                foreach($privileges_info['sub'] as $detail)
                {
                    if($detail['url_name'] == custom_route_name()){

                        if(in_array($detail['id'], $admin_privileges)){
                            $result = true;
                            break;
                        }
                    }
					if($detail['extra_url_name'] == $privileges_route_name){
						if(in_array($detail['id'], $admin_privileges))
						{
							$result = true;
							break;
						}
					}
                }
            }
        }
        
        return $result;
    }
}

if (! function_exists('d')) {
    
    function d()
    {
        $args = func_get_args();
        echo "<pre>";
        print_r($args);
        echo "</pre>";
    }
}

if(! function_exists('is_admin_master')){
    
    function is_admin_master()
    {
        if(Auth::guard('admin')->user()->master == 0){
            return false;
        }else{
            return true;
        }
    }
}

/*
* get route name by filter out en. and zh-CN.
*/
if(! function_exists('custom_route_name')){
    
    function custom_route_name()
    {
        $routeName = substr(Route::currentRouteName(), 0, 3);
        $chinese_routeName = substr(Route::currentRouteName(), 0, 5);
        $custom_route_name = Route::currentRouteName();
        
        if(strpos($routeName, '.') !== false){
            $custom_route_name = substr(Route::currentRouteName(), 3);
        }
    
        if($chinese_routeName == Config::get('app.chinese_locale')){
            $custom_route_name = substr(Route::currentRouteName(), 6);
        }

        return $custom_route_name;
    }
}

if(! function_exists('db_prefix')){
    
    function db_prefix(){
        $db_connection = config('database.default');
        $db_prefix = config('database.connections')[$db_connection]['prefix'];

        return $db_prefix;
    }
}

if(! function_exists('mysql_create_database')){
    
    function mysql_create_database()
    {
        
        $host = config('database.connections')[config('database.default')]['host'];
        $user = config('database.connections')[config('database.default')]['username'];
        $pass = config('database.connections')[config('database.default')]['password'];
        $db = config('database.connections')[config('database.default')]['database'];

        $dbh = new mysqli($host, $user, $pass);
        $sql = "CREATE DATABASE `{$db}` CHARACTER SET utf8 COLLATE utf8_general_ci";
        $dbh->query($sql);
    }
}

if(! function_exists('mysql_create_table')){
    
    function mysql_create_table()
    {
        \Artisan::call('migrate', ['--force' => true, '--seed' => true]);   
    }
}

if(! function_exists('multiple_fields_placeholder')){

    function multiple_fields_placeholder($fields = array(), $trans_prefix){
        
        $fields_placeholder = '';
        
        foreach($fields as $field){
            $fields_placeholder = $fields_placeholder . trans('trans.' . $trans_prefix . $field) . ' / ';
        }

        $fields_placeholder = substr($fields_placeholder, 0, -2);

        return $fields_placeholder;
    }
}

if(! function_exists('sort_prefix_url')){

    function sort_prefix_url(){
        
        $sort_prefix_url = str_replace('sort='.Request::get('sort'), '', Request::fullUrl());

        if ( substr($sort_prefix_url, -1) == '&' ){
            $sort_prefix_url = rtrim($sort_prefix_url,"&");
        }

        return $sort_prefix_url;
    }
}

/* End Custom Helper */
?>