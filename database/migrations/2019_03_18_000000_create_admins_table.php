<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('uid');
            $table->string('name', 191);
            $table->string('mobile', 15);
            $table->unsignedSmallInteger('master');
            $table->unsignedSmallInteger('dev');
            $table->text('privileges');
            $table->string('email')->unique();
            $table->string('password', 191);
            $table->rememberToken();
            $table->text('api_token')->nullable();
            $table->string('api_token_expire_date')->nullable();
            $table->unsignedSmallInteger('status')->comment('10-active, 90-suspend, 100-terminated');
            $table->datetime('created_at');
            $table->datetime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}