<?php 

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\WebService;
use App\Admin;
use App\User;
use Illuminate\Support\Facades\Auth; 
use App\WebServiceUser;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Lang;
use Lcobucci\JWT\Parser;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use App\Permission;
use App\Role;

class AdminController extends Controller {
    public function index(){
        return 'hello admin';
    }  

    public function updateProfile(Request $request){
        $admin = new Admin();
        $ws = new WebService();
        
        $form_fields = array("name", "password", "confirm_password", "email", "mobile", "admin_uid");
        $data = array();
        
        foreach($form_fields as $field){
            if(isset($request->$field)){
                $data[$field] = $request->$field;
            }
        }
        
        try{
            $admin->validate_update_profile($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '2000007200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $admin->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;
            }   
            
            return response()->json($result);
        }
        
        try{
            $admin->update_profile($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }
    
        try{
            $adminDetail = $admin->get_admin_module_detail($data['admin_uid']);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        return $ws->api_result($ws::$api_code_to_status['success'], array('profile' => $adminDetail));
    }

    public function updatePassword(Request $request){
        $admin = new Admin();
        $ws = new WebService();
        
        $form_fields = array("password", "confirm_password", "admin_uid");
        $data = array();

        foreach($form_fields as $field){
            if(isset($request->$field)){
                $data[$field] = $request->$field;
            }
        }
        
        try{
            $admin->validate_update_password($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '2000012200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $admin->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;
            }   
            return response()->json($result);
        }

        try{
            $admin->update_password($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        try{
            $adminDetail = $admin->get_admin_module_detail($data['admin_uid']);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        return $ws->api_result($ws::$api_code_to_status['success'], array('profile' => $adminDetail));
    }

    public function get_privileges_list(Request $request){
        $ws = new WebService();
        $admin = new Admin();

        foreach(array('admin_uid') as $field){
            $data[$field] = $request->$field;
        }

        try{
            $admin->validate_get_admin_privileges_list($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '2000017001':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $admin->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }
        
		$privilegesList = $admin::$api_privileges_list;
		$privilegesResult = [];
		$privilegesDetail = [];
		
		if(!empty($privilegesList)){
			foreach($privilegesList as $pname => $pdetail){
				if(!empty($pdetail['sub'])){
					foreach($pdetail['sub'] as $action => $actionDetail){
						$privilegesDetail[] = (object) array('id' => $actionDetail['id'], "name" => $actionDetail['name'], 'label' => $actionDetail['label']);
					}
				}				
				$privilegesResult[] = (object) array("id" => $pdetail['id'], "name" => $pname, 'label' => $pdetail['label'], "sub" => $privilegesDetail);
				$privilegesDetail = [];
			}
		}
		
        $result = $ws->api_result($ws::$api_code_to_status['success'], array('admin_privileges_list' => $privilegesResult));

        return response()->json($result);
    }

    public function getPrivileges(Request $request){
        $ws = new WebService();
        $admin = new Admin();

        $result = '';

        foreach(array('admin_uid') as $field){
            $data[$field] = $request->$field;
        }

        try{
            $admin->validate_get_admin_privileges($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '2000021001':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $admin->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;  
            }
            return response()->json($result);
        }
		
		try{			
			$privilegesInfo = $admin->get_admin_privileges($data);
		}catch(CustomGeneralException $e){
			switch ($err_code) { 
				default:
					$result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
					break; 									
			}
			
			return response()->json($result);
		}
		
		$privileges = [];
		
		foreach($privilegesInfo as $detail)
		{
			$privileges = array(
				"dev" => $detail->dev, 
				"master" => $detail->master,
				"detail" => [],
			);
			
			if(!$detail->dev && !$detail->master && empty($detail->privileges)){
			}
			elseif(!$detail->dev && !$detail->master){
				$adminPrivileges = $detail->privileges;
				
				foreach($admin::$api_privileges_list as $privilegesName => $privilegesDetail){
					if(in_array($privilegesDetail['id'], json_decode($detail->privileges, true))){
						foreach($privilegesDetail['sub'] as $action => $subDetail){
							$insideDetail = in_array($subDetail['id'], json_decode($detail->privileges))? $privilegesName . "-" . $action : '';
							array_push($privileges['detail'], $insideDetail);
                            if($action == "view" && in_array($subDetail['id'], json_decode($detail->privileges)) ){
                                array_push($privileges['detail'], $privilegesName);
                            }
						}

					}
				}
				
			}
		}
	
        return response()->json($privileges);
    }

    public function get_privile_list(Request $request)
    {
        $list = Permission::$api_privileges_list;

        $result = array();

        foreach($list as $name => $detail){
            if(!empty($detail['sub'])){
                foreach($detail['sub'] as $action => $value)
                {
                    $result[$name] = $detail['id'];
                    $result[$name.'-'.$action] = $value['id'];   
                }

            }
        }
        
        return response()->json($result);
    }

    public function get_admin_permission(Request $request)
    {
        $admin = Admin::where('uid',$request->admin_uid)->first();
        $role = Role::find($admin->role_id);
        $permission = Permission::find($role->permission_id)->toArray();
        
        $result = $permission['privileges'];

        return response()->json($result);
    }
}