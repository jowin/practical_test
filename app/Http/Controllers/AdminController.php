<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use Lang;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin-api');
    }

    public function index(Request $request){
        dd($request->user()->uid);
        return view('demo/admin');
    }

    public function profile(){
        return view('demo/admin/profile');
    }

    public function save_profile(Request $request){
        $form_fields = array("name", "mobile", "email");
        $password_fields = array("password");

        if(($request->form == 1) && (Input::get('_token') === $request->session()->token()) ){
            foreach($form_fields as $field){
                $data[$field] = Input::get($field);
            }
        }elseif(($request->form == 2) && (Input::get('_token') == $request->session()->token()) ){
            foreach($password_fields as $field){                
                $data[$field] = Input::get($field);
            }
        }else{
            return redirect()->back()->withErrors($err = true, 'field_error')->withInput();     
        }
        
        $data['admin_uid'] = admin()->uid;

        try{
            admin()->validate_update_profile($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();

            switch ($err_code) {
                case '2000007200':
                    $err = admin()->err_field_params;
                    break;                
                default:
                    $err = true;
                    d($err_code);
                    d($e->getMessage());
                    break;
            }  

            return redirect()->back()->withErrors($err, 'field_error')->withInput();          
        }

        try{
            admin()->update_profile($data);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $err = admin()->err_query_info;
            $err_msg = "";
            switch ($err_code) {
                default:
                    $err_msg = trans('trans.general/error/message');
                    break;
            }    
            
            return redirect()->back()->withErrors($err, 'query_error')->withInput();          
        }
        
        return redirect(locale_route('admin.profile'))->with(['succ_edit_msg' => trans('trans.admin/profile/success/message') ]);
    }
}
