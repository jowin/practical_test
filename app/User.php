<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use Illuminate\Database\QueryException;
use Validator;
use App;
use Carbon\Carbon;
use DB;
use Auth;
use App\Rules\UserExtraValidation;
use App\Rules\AuthExtraValidation;
use App\Rules\AdminExtraValidation;
use Illuminate\Validation\Rule;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'remember_token', 'api_token', 'api_token_expire_date', 'status', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static $user_table = "users";

    public static $status_to_code = array(
        'active' => 10,
        'suspend' => 90,
        'terminited' => 100,
    );

    public static $code_to_status = array(
        10 => 'active',
        90 => 'suspend',
        100 => 'terminited',
    );

    protected $class_code = "4000";

    public static $prefix = "member";

    protected $insert_rules = [
        'name' => 'required',
        'password' => 'required|min:6',
    ];

    protected $update_rules = [
        'name' => 'required',
        'password' => 'required|min:6',
    ];

    protected $update_profile_rules = [
        'name' => 'required',
        'id' => 'required|numeric'
    ];

    protected $update_password_rules = [
        'password' => "required|min:6",
        'confirm_password' => "required_with:password|same:password", 
    ];

    public $err_query_info = ["code" => "", "message" => "", "params" => array()];
    
    public $custom_err_info = ["code" => "", "message" => "", "params" => array()];

    public $cust_err_field_params = array();

    protected $login_rules = [
        'email' => 'required|email', 
        'password' => 'required|min:6', 
    ];

    protected $reset_password_rules = [
        'email' => 'required|email',
        'password' => 'required|min:6',
        'code' => 'required',
        'confirm_password' => 'required|min:6|same:password',
    ];

    public static $item_per_page = 5;

    public function validate_add_member($data){
        $method_code = "001";
        $method_name = "validate-add-member";

        $messages = [];
        
        $this->insert_rules['email'] = ['required', 'email', 'unique:users', new AuthExtraValidation];
        $validator = Validator::make($data, $this->insert_rules, $messages); 

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;      
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }        
    }

    public function generate_unique_id()
    {
        $random_id = 0;

        do{
            $random_id = mt_rand(00000000, 99999999);
            $info = DB::table(static::$user_table)->select('uid')->where('uid', $random_id)->get();
        }while(!$info->isEmpty());

        return $random_id;
    }

    public function add_member($data){
        $method_code = "002";
        $method_name = "add-member";
    
        $data['uid'] = $this->generate_unique_id();
        $data['created_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();   
        $data['updated_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();
        $data['status'] = static::$status_to_code['active'];
        
        $this->validate_add_member($data);

        $data['password'] = bcrypt($data['password']);
        $data['remember_token'] = "";
        $data['api_token'] = '';
        $data['api_token_expire_date'] = '';

        try{
            $aid = DB::table($this::$user_table)->insertGetId($data);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }  

        return $aid;
    }

    public function get_member_detail($uid = '',$id = '')
    {
        $method_code = "003";
        $method_name = "get-member-module-detail";

        try{
            $result = User::where(['uid'=> $uid])->orWhere(['id' => $id])->first();
            $result = json_decode($result, true);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomQueryException($method_name, $this->class_code.$method_code.'001');
        }

        return $result;
    }

    public function validate_login($data){
        $method_code = "004";
        $method_name = "validate-login";

        $messages = [];
        $validator = Validator::make($data, $this->login_rules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;      
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }  
    }

    public function login($data){
        $method_code = "005";
        $method_name = "login";
        
        $this->validate_login($data);

        $result = [];

        if(Auth::guard('web')->attempt(['email' => $data['email'], 'password' => $data['password']])){
            $ws = new WebService();
            $ws_user = new WebServiceUser();

            $user = Auth::guard('web')->user();
            $rdata = array();
            
            // check previous time has login token or not            
            $currentTokenInfo = $ws->retrieve_user_token(static::$user_table, $user->uid);

            if(!empty($currentTokenInfo['api_token']) && isset($currentTokenInfo['api_token'])){
                try{
                    $tokenInfo = $ws->check_login_token(static::$user_table, $user->uid);
                }catch(CustomGeneralException $e){
                    $this->err_query_info['code'] = $e->getCode();
                    $this->err_query_info['message'] = $e->getMessage();
                    $this->err_query_info['params'] = $e->getBindings();
                    throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
                }

                $rdata = array(
                    "profile" => $user->get_member_detail($user->uid),
                    "auth" => $tokenInfo,
                );                
                $result = $ws->api_result($ws::$api_code_to_status['success'], $rdata);
                return $result;
            }
            
            try{
                $loginTokenInfo = $ws->generate_new_token(static::$user_table, $user->uid);
            }catch(CustomGeneralException $e){
                $this->err_query_info['code'] = $e->getCode();
                $this->err_query_info['message'] = $e->getMessage();
                $this->err_query_info['params'] = $e->getBindings();
                throw new CustomGeneralException($method_name, $this->class_code.$method_code.'002');
            }

            $adata = array(
                "profile" => $this->get_member_detail($user->uid),
                "auth" => $loginTokenInfo,
            );
            
            $result = $ws->api_result($ws::$api_code_to_status['success'], $adata);            
        }
        
        return $result;
        
    }

    public function validate_forgot_password($data){
        $method_code = "006";
        $method_name = "validate-forgot-password";

        $messages = [];
        $validateField['email'] = ['required', 'email', new UserExtraValidation];
        $validator = Validator::make($data, $validateField, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;     
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }   
    }

    public function forgot_password($data){
        $method_code = "007";
        $method_name = "forgot-password";

        $this->validate_forgot_password($data);
        
        $code = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 16);
        $system = new System();
        $third_party_mail = new ThirdPartyMail($system->pickEmailProvider());
        $ws = new WebService();
        $emailParam = array();
        $emailParam['email'] = $data['email'];

        try{
            User::where('email', $data["email"])->update(['forgot_password_vcode' => $code]);
            $info = User::where('email', $data['email'])->first();
            $emailParam['forgot_password_link'] = url('reset-password?email='. $info['email'] . '&vcode=' . $info['forgot_password_vcode']);                      
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }

        try{            
            $third_party_mail->sendForgotPassword($emailParam);   
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'002');
        }

        $adata = array(
            "url" => $emailParam['forgot_password_link'],
        );

        $result = $ws->api_result($ws::$api_code_to_status['success'], $adata);    

        return $result;
    }

    public function validate_reset_password($data){
        $method_code = '008';
        $method_name = "validate-reset-password";

        $messages = [];
        $validator = Validator::make($data, $this->reset_password_rules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;      
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        } 
    }

    public function reset_password($data){
        $method_code = '009';
        $method_name = "reset-password";

        $this->validate_reset_password($data);

        $info = User::where('email', $data['email'])->where('forgot_password_vcode', $data['code'])->first();

        if(empty($info)){
            $this->err_query_info['message'] = 'Invalid Code';
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }

        try{
            User::where('email', $data['email'])->where('forgot_password_vcode', $data['code'])->update(['password' => bcrypt($data['password'])]);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'002');
        }
    }

    public function validate_get_user_info($data){
        $method_code = "010";
        $method_name = 'validate-get-user-info';   

        $validationRules = array();
        $validationRules['admin_uid'] = ['required', 'numeric', new AdminExtraValidation];

        $messages = [];        
        $validator = Validator::make($data, $validationRules, $messages);
        
        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'001');
        }
    }

    public function get_user_info($filterData){
        $method_code = "011";
        $method_name = "get-user-info";

        $ws = new WebService();
       
        try{
            $result = DB::table("users")->where($filterData['asearch'])->where(function($query) use ($filterData){     
                if(!empty($filterData['qsearch'])){
                    foreach($filterData['qsearch'] as $field => $info){
                        $query->orWhere($field, $info['search_type'], $info['value']);    
                    }
                }elseif(!empty($filterData['asearch_extra'])){
                    foreach($filterData['asearch_extra'] as $index => $value){                    
                        $query->orWhere($value[0], $value[1], $value[2]);
                    }
                }else{
                    $query->orWhere([]);
                }
            })->paginate(static::$item_per_page);  
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
        
        $info = array(
            'user_list_info' => $result,
        );
        
        $result = $ws->api_result($ws::$api_code_to_status['success'], $info); 

        return $result;
    }

    public function validate_get_user($data){
        $method_code = '012';
        $method_name = 'validate-get-user';

        $validationRules['id'] = ['required', 'numeric'];

        $messages = [];        
        $validator = Validator::make($data, $validationRules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'001');
        }
    }

    public function get_user_detail($id = ''){
        $method_code = "014";
        $method_name = "get-user-detail";

        try{
            $result = User::where(['id' => $id])->first();
            $result = json_decode($result, true);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomQueryException($method_name, $this->class_code.$method_code.'001');
        }

        return $result;
    }

    public function validate_update_user($data){
        $method_code = "015";
        $method_name = "validate-update-user";

        $messages = [];

        // $this->update_rules['email'] = ['required', 'email', 'unique:users', new AuthExtraValidation];
        $this->update_rules['email'] = ['required', 'email', Rule::unique('users','email')->ignore($data['id'])];
        $validator = Validator::make($data, $this->update_rules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }

        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;
            /*
            * @param message - The Exception message to throw.
            * @param code - The Exception code.
            * @param previous - The previous exception used for the exception chaining.
            */
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }
    }

    public function update_user($data){
        $method_code = "016";
        $method_name = "update-user";

        $this->validate_update_user($data);

        $data['updated_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();   

        $udata = $data;
       
        try{
            User::where('id', $data['id'])->update(['name' => $data['name'], 'email' => $data['email'], 'password' => bcrypt($data['password']), 'updated_at' => $data['updated_at']]);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        } 
    }

    public function delete_user($data){
        $method_code = "017";
        $method_name = "delete-user";

        $this->validate_get_user($data);

        try{
            User::where('id', $data['id'])->delete();
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        } 
    }

    public function validate_update_profile($data)
    {
        $method_code = "018";
        $method_name = "validate-update-profile";

        $this->update_profile_rules['email'] = 'required|email|unique:admins,email,'.$data['id'].',id';

        foreach(array('name', 'email') as $required_field){
            if((array_key_exists($required_field, $data)) && (is_null($data[$required_field])) && (empty($data[$required_field])) ){
                $this->update_profile_rules[$required_field] = ['required'];
            }
        }

        $messages = [];      
        
        $validator = Validator::make($data, $this->update_profile_rules, $messages);
        
        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }

        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;
            /*
            * @param message - The Exception message to throw.
            * @param code - The Exception code.
            * @param previous - The previous exception used for the exception chaining.
            */
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }
    }

    public function update_profile($data){
        $method_code = "019";
        $method_name = "update-profile";

        $this->validate_update_profile($data);

        if(array_key_exists("password", $data)){
            $data['password'] = bcrypt($data['password']);
        }

        $udata = $data;
        
        try{
            DB::table(static::$user_table)->where('id', $data['id'])->update($udata);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
    }

    public function validate_update_password($data){
        $method_code = "020";
        $method_name = "validate-update-password";

        $this->update_password_rules['id'] = ['required', 'numeric'];
        
        $messages = [];        
        $validator = Validator::make($data, $this->update_password_rules, $messages);
        
        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }
    }

    public function update_password($data){
        $method_code = "021";
        $method_name = "update-password";

        $this->validate_update_password($data);

        $data['password'] = bcrypt($data['password']);

        $udata = $data;
        
        unset($udata['id'], $udata['wsid'], $udata['confirm_password']);
        
        try{
            DB::table(static::$user_table)->where('id', $data['id'])->update($udata);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
    }
}