<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Auth\AuthenticationException;
use App\WebService;
use Lang;
use Illuminate\Http\RedirectResponse;
use Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        header('Content-Type: Application/json');
        $ws = new WebService();
        $status = $ws::$api_code_to_status['internal-server-error'];
        $data = array(
            "dev_status" => $ws::$api_error_detail_to_code['internal-server-error']['user-not-found'],
            "general_msg" => Lang::get('trans.api/authenticated/error'),
            "success" => "",
            "dev_msg" => "Unauthenticated.",
        );
        $result = $ws->api_result($status, $data);
        echo json_encode($result);
        exit();
    }
}
