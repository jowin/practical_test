import axios from 'axios';
import Config from '@/js/config/app.js';

const instance = axios.create({
    baseURL: Config.base_url(),
})

instance.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('loginToken');
instance.defaults.headers.common['Content-Type'] = "application/json";

export default instance