<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Route::prefix('admin')->group(function () {
    Route::get('/', 'Auth\AdminLoginController@showLoginForm');
    Route::get('{any}', 'Auth\AdminLoginController@showLoginForm')->where('any', '.*');
});

Route::prefix('member')->group(function () {
    Route::get('/', 'Auth\MemberController@showLoginForm');
    Route::get('{any}', 'Auth\MemberController@showLoginForm')->where('any', '.*');
});