import Router from '@/js/routes';

export default {
    namespaced: true,
    state: {
        
    },
    mutations: {
        
    },
    getters: {        
    },
    actions: {   
        logout({commit}){
            commit('clearAuthInfo');
            localStorage.removeItem('loginTokenExpireDate');
            localStorage.removeItem('loginToken');
            localStorage.removeItem('adminInfo');
            Router.replace({name: 'admin.login'});
            Router.go({name: "admin.login"});
        },
    },

}