<?php

use Illuminate\Http\Request;
use App\Admin;
use App\Merchant;
use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

$locales = config('app.locales'); // en, zh-CN, /
foreach ($locales as $locale) {
	$name = ($locale == '/')? '' : $locale .'.';
	Route::prefix($locale)->name($name)->group(function () {
		$api_version = Config::get('app.api_versions');
		foreach ($api_version as $version) {
			Route::prefix($version)->group(function () {
				Route::prefix(Admin::$prefix)->group(function() {
					Route::post('register', 'API\Auth\AdminLoginController@register');
					Route::post('login', 'API\Auth\AdminLoginController@login')->name('admin.login');
					/*
					Route::middleware('auth:admin-api')->post('/index', function(Request $request) {
						dump($request->user()->uid);
						dd('admin index');						
						// dd($request->user()->uid); 
					});
					*/
					Route::group(['middleware' => 'auth:admin-api'], function(){
						Route::post('/index', 'API\Admin\AdminController@index');
						Route::post('/update-profile', 'API\Admin\AdminController@updateProfile');
						Route::post('/update-password', 'API\Admin\AdminController@updatePassword');
						Route::post('/get-admin-status-list', 'API\Admin\AdminModuleController@getAdminStatusList');
						Route::post('/get-admin-permission', 'API\Admin\AdminController@get_admin_permission');
						Route::post('/get-privileges-list', 'API\Admin\AdminController@get_privile_list');
						//Route::post('/check-permission', 'API\Admin\AdminController@check_permission');
						
						/* Admin Modules */
						/*Route::post('/admin-modules', 'API\Admin\AdminModuleController@index');
						Route::post('/get-admin-privileges-list', 'API\Admin\AdminController@get_privileges_list');
						Route::post('/get-admin-privileges', 'API\Admin\AdminController@getPrivileges');*/

						Route::prefix('admin-modules')->group(function(){
							Route::post('/index', 'API\Admin\AdminModuleController@index');
							Route::post('/get-admin-privileges-list', 'API\Admin\AdminController@get_privileges_list');
							Route::post('/add-admin', 'API\Admin\AdminModuleController@create');
							Route::post('/get-admin-info', 'API\Admin\AdminModuleController@detail');
							Route::post('/update-admin', 'API\Admin\AdminModuleController@update');
							Route::post('/get-role-list', 'API\Admin\AdminModuleController@get_role_list');
						});

						Route::prefix('admin-permission')->group(function(){
							Route::post('/index', 'API\Admin\AdminPermissionController@index');
							Route::post('/get-privilege-list', 'API\Admin\AdminPermissionController@get_privileges_list');
							Route::post('/add-permission', 'API\Admin\AdminPermissionController@create');
							Route::post('/get-permission-info', 'API\Admin\AdminPermissionController@detail');
							Route::post('/update-permission', 'API\Admin\AdminPermissionController@update');
							Route::post('/delete-permission', 'API\Admin\AdminPermissionController@delete');
						});

						Route::prefix('admin-role')->group(function(){
							Route::post('/index', 'API\Admin\AdminRoleController@index');
							Route::post('/add-role', 'API\Admin\AdminRoleController@create');
							Route::post('/get-permission-list', 'API\Admin\AdminRoleController@get_permission_list');
							Route::post('/get-role-info', 'API\Admin\AdminRoleController@detail');
							Route::post('/update-role', 'API\Admin\AdminRoleController@update');
							Route::post('/delete-role', 'API\Admin\AdminRoleController@delete');
						});

						Route::prefix('user-modules')->group(function(){
							Route::post('/index', 'API\Admin\UserModuleController@index');
							Route::post('/add-user', 'API\Admin\UserModuleController@create');
							Route::post('/get-user-info', 'API\Admin\UserModuleController@detail');
							Route::post('/update-user', 'API\Admin\UserModuleController@update');
							Route::post('/delete-user', 'API\Admin\UserModuleController@delete');
							Route::post('/get-status-list', 'API\Admin\UserModuleController@get_status_list');
						});
					});
				});
				
				Route::prefix(Merchant::$prefix)->group(function (){
					Route::post('register', 'API\Auth\MerchantController@register');
					Route::post('login', 'API\Auth\MerchantController@login');
					Route::post('logout', 'API\Auth\MerchantController@logout');
				});

				Route::prefix(User::$prefix)->group(function (){
					Route::post('register', 'API\Auth\MemberController@register');
					Route::post('login', 'API\Auth\MemberController@login');
					Route::post('logout', 'API\Auth\MemberController@logout');
					Route::post('update-profile','API\Auth\MemberController@update_profile');
					Route::post('update-password','API\Auth\MemberController@update_password');
				});

				Route::post('forgot-password', 'API\Auth\GeneralAuth@forgot_password');
				Route::post('reset-password', 'API\Auth\GeneralAuth@reset_password');
			});
		}
	});
}