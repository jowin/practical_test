import Config from '@/js/config/app.js';
import Router from '@/js/routes';
import axios from 'axios';

export default {
    namespaced: true,
    state: {
        searchResult: '',
        adminStatusList: '',
        adminPrivilegesList: '',
        infoSignal: {
            addSignal: '',   
            updateSignal: '',
            quickSearchSignal: '',
            advanceSearchSignal: '',
        },
        field_error: {},
        isShowingAdminInfo: '',
        roleList: '',
    },
    mutation: {
    },
    getters: {
        adminList({state}){
            return state.searchResult;
        }
    },
    actions: {
        getAdminPrivilegesList({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-modules/get-admin-privileges-list', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.adminPrivilegesList = res['data']['data']['admin_privileges_list'];
                }
            })
            .catch(error => console.log(error));   
        },
        
        /* Index */
        executeQuickSearch({state, commit, rootState}, incomingData){         
			axios.post(Config.base_url() + 'admin/admin-modules/index', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['admin_list_info'];
                    state.infoSignal.quickSearchSignal = true;
                }
            })
            .catch(error => console.log(error));            
        },
        retreiveAdminList({state, commit, rootState}, incomingData){
            axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('loginToken');
            axios.post(Config.base_url() + 'admin/admin-modules/index', incomingData)
                .then(res => {
                    console.log(res);
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['admin_list_info'];
                }
            })
            .catch(error => console.log(error));             
        },
        switchPage({state, commit, rootState}, incomingData){  
            axios.post(incomingData.url, incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['admin_list_info'];
                }
            })
            .catch(error => console.log(error));             
        },
        getStatusList({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/get-admin-status-list', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.adminStatusList = res['data']['data']['status_list'];
                }
            })
            .catch(error => console.log(error));        
        },
        executeAdvanceSearch({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-modules/index', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['admin_list_info'];
                    state.infoSignal.advanceSearchSignal = true;
                }
            })
            .catch(error => console.log(error));    
        },
        retriveRoleList({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-modules/get-role-list', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.roleList = '';
                    state.roleList = res['data']['data']['info'];
                }
            })
            .catch(error => console.log(error));    
        },
        /* End Index */

        /* Create */       
        insertAdmin({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-modules/add-admin', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.infoSignal.addSignal = Config.generalStatus['success'];
                    Router.push({ name: "admin-modules"});
                }else {
                    state.field_error = res['data']['field_error'];
                    state.infoSignal.addSignal = Config.generalStatus['error'];
                }
            })
            .catch(error => console.log(error)); 
        },
        /* End Create */

        /* Admin Info */
        retreiveAdminInfo({state, coimmit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-modules/get-admin-info', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.isShowingAdminInfo = res['data']['data']['admin_info']['data'];
                }  
            })
            .catch(error => console.log(error));
        },
        /* End Admin Info */  

        updateAdmin({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-modules/update-admin', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.infoSignal.updateSignal = Config.generalStatus['success'];
                    Router.push({ name: "admin-modules"});
                }else {
                    state.field_error = res['data']['field_error'];
                    state.infoSignal.updateSignal = Config.generalStatus['error'];
                }
            })
            .catch(error => console.log(error)); 
        },

        sortField({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-modules/index', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['admin_list_info'];
                }
            })
            .catch(error => console.log(error)); 
        },
    },

}  