<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\WebService;
use Validator;
use Carbon\Carbon;
use App\Exceptions\CustomValidationException;
use DB;

class Permission extends Model
{
    protected $table = "permissions";

    public $err_query_info = ["code" => "", "message" => "", "params" => array()];
    
    public $custom_err_info = ["code" => "", "message" => "", "params" => array()];

    public $cust_err_field_params = array();

    protected $fillable = [
        'id', 'name', 'privileges', 'created_at', 'updated_at'
    ];

    public static $item_per_page = 5;

    protected $class_code = "6000";

    public static $api_privileges_list = [
        "admin-modules" => array(
			"id" => 1000,
            "label" => "admin/privileges/admin-module", // translate key in js files.
            "sub" => array(
                "view" => array(
                    "id" => 1000,
                    "name" => "view",
                    "label" => "admin/privileges/label/view", // translate key in js files.
                    "url_name" => "admin.admin-module.index", // name in routes.js
                    "extra_url_name" => ["admin.admin-module.indexData", "admin.admin-module.detail", "admin-module.listIndex"], // name in routes.js
                ),
                "insert" => array(
                    "id" => 1001,
                    "name" => "insert",
                    "label" => "admin/privileges/label/insert", // translate key in js files.
                    "url_name" => "admin.admin-module.create", // name in routes.js
                    'extra_url_name' => '',
                ),                
                "edit" => array(
                    "id" => 1002,
                    "name" => "edit",
                    "label" => "admin/privileges/label/edit",  // translate key in js files.
                    "url_name" => "admin.admin-module.edit", // name in routes.js
                    'extra_url_name' => '',
                ),
            ),
        ),

        "user-modules" => array(
            "id" => 1100,
            "label" => "admin/privileges/user-module", // translate key in js files.
            "sub" => array(
                "view" => array(
                    "id" => 1100,
                    "name" => "view",
                    "label" => "admin/privileges/label/view", // translate key in js files.
                    "url_name" => "admin.user-module.index", // name in routes.js
                    "extra_url_name" => ["admin.user-module.indexData", "admin.user-module.detail", "user-module.listIndex"], // name in routes.js
                ),
                "insert" => array(
                    "id" => 1101,
                    "name" => "insert",
                    "label" => "admin/privileges/label/insert", // translate key in js files.
                    "url_name" => "admin.user-module.create", // name in routes.js
                ),                
                "edit" => array(
                    "id" => 1102,
                    "name" => "edit",
                    "label" => "admin/privileges/label/edit",  // translate key in js files.
                    "url_name" => "admin.user-module.edit", // name in routes.js
                ),
                "delete" => array(
                    "id" => 1103,
                    "name" => "edit",
                    "label" => "admin/privileges/label/delete",  // translate key in js files.
                    "url_name" => "admin.user-module.delete", // name in routes.js
                )
            ),
        ),

        "admin-role" => array(
            "id" => 1200,
            "label" => "admin/privileges/admin-role", // translate key in js files.
            "sub" => array(
                "view" => array(
                    "id" => 1200,
                    "name" => "view",
                    "label" => "admin/privileges/label/view", // translate key in js files.
                    "url_name" => "admin.admin-role.index", // name in routes.js
                    "extra_url_name" => ["admin.admin-role.indexData", "admin.admin-role.detail", "admin-role.listIndex"], // name in routes.js
                ),
                "insert" => array(
                    "id" => 1201,
                    "name" => "insert",
                    "label" => "admin/privileges/label/insert", // translate key in js files.
                    "url_name" => "admin.admin-role.create", // name in routes.js
                ),                
                "edit" => array(
                    "id" => 1202,
                    "name" => "edit",
                    "label" => "admin/privileges/label/edit",  // translate key in js files.
                    "url_name" => "admin.admin-role.edit", // name in routes.js
                ), 
                "delete" => array(
                    "id" => 1203,
                    "name" => "edit",
                    "label" => "admin/privileges/label/delete",  // translate key in js files.
                    "url_name" => "admin.admin-role.delete", // name in routes.js
                )
            ),
        ),

        "admin-permission" => array(
            "id" => 1300,
            "label" => "admin/privileges/admin-permission", // translate key in js files.
            "sub" => array(
                "view" => array(
                    "id" => 1300,
                    "name" => "view",
                    "label" => "admin/privileges/label/view", // translate key in js files.
                    "url_name" => "admin.admin-permission.index", // name in routes.js
                    "extra_url_name" => ["admin.admin-permission.indexData", "admin.admin-permission.detail", "admin-permission.listIndex"], // name in routes.js
                ),
                "insert" => array(
                    "id" => 1301,
                    "name" => "insert",
                    "label" => "admin/privileges/label/insert", // translate key in js files.
                    "url_name" => "admin.admin-permission.create", // name in routes.js
                ),                
                "edit" => array(
                    "id" => 1302,
                    "name" => "edit",
                    "label" => "admin/privileges/label/edit",  // translate key in js files.
                    "url_name" => "admin.admin-permission.edit", // name in routes.js
                ),
                "delete" => array(
                    "id" => 1303,
                    "name" => "edit",
                    "label" => "admin/privileges/label/delete",  // translate key in js files.
                    "url_name" => "admin.admin-permission.delete", // name in routes.js
                )
            ),
        ),

        /*
        "anouncement" => array(
            "id" => 1100,
            "label" => "admin/privileges/anouncement",
            "sub" => array(
                "view" => array(
                    "id" => 1100,
                    "name" => "view",
                    "label" => "admin/privileges/label/view",
                    "url_name" => "admin.anouncement.index",
                    "extra_url_name" => "admin.anouncement.detail",
                ),
                "insert" => array(
                    "id" => 1101,
                    "name" => "insert",
                    "label" => "admin/privileges/label/insert",
                    "url_name" => "admin.anouncement.create",
                ),                
                "edit" => array(
                    "id" => 1102,
                    "name" => "edit",
                    "label" => "admin/privileges/label/edit",
                    "url_name" => "admin.anouncement.edit",
                )
            ),
        ),
        */ 
    ];

    protected $insert_rules = [
        'name' => 'required',
    ];

    protected $update_rules = [
        'name' => 'required',
    ];

    public function get_permission_info($filterData){
        $method_code = "001";
        $method_name = "get-permission-info";

        $ws = new WebService();
       
        try{
            $result = DB::table("permissions")->where($filterData['asearch'])->where(function($query) use ($filterData){     
                if(!empty($filterData['qsearch'])){
                    foreach($filterData['qsearch'] as $field => $info){
                        $query->orWhere($field, $info['search_type'], $info['value']);    
                    }
                }elseif(!empty($filterData['asearch_extra'])){
                    foreach($filterData['asearch_extra'] as $index => $value){                    
                        $query->orWhere($value[0], $value[1], $value[2]);
                    }
                }else{
                    $query->orWhere([]);
                }
            })->paginate(static::$item_per_page);  
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }
        
        $info = array(
            'permission_list_info' => $result,
        );
        
        $result = $ws->api_result($ws::$api_code_to_status['success'], $info); 

        return $result;
    }

    public function validate_add_permission($data){
        $method_code = "001";
        $method_name = "validate-add-permission";

        $messages = [];     
        $validator = Validator::make($data, $this->insert_rules, $messages); 

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }
    }

    public function add_permission($data){
        $method_code = "002";
        $method_name = "add-permission";
    
        $data['created_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();   
        $data['updated_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();
        
        $this->validate_add_permission($data);

        $data['privileges'] = json_encode($data['privileges']);
        try{
            $pid = Permission::insertGetId($data);
        }catch(QueryException $e){
            dd($e->getMessage());
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }  
        
        return $pid;
    }

    public function get_permission_detail($id = ''){
        $method_code = "003";
        $method_name = "get-permission-detail";

        try{
            $result = Permission::where(['id' => $id])->first();
            $result = json_decode($result, true);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomQueryException($method_name, $this->class_code.$method_code.'001');
        }

        return $result;
    }
    
    public function validate_get_permission($data){
        $method_code = '004';
        $method_name = 'validate-get-permission';

        $validationRules['id'] = ['required', 'numeric'];

        $messages = [];        
        $validator = Validator::make($data, $validationRules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;          
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'001');
        }
    }

    public function get_permission($data){
        $method_code = '005';
        $method_name = 'get-permission';

        $this->validate_get_permission($data);
       
        try{ 
            $result = Permission::paginate(static::$item_per_page);                
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }

        return $result;
    }

    public function validate_update_permission($data){
        $method_code = "006";
        $method_name = "validate-update-permission";

        $messages = [];

        $validator = Validator::make($data, $this->update_rules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }

        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;
            /*
            * @param message - The Exception message to throw.
            * @param code - The Exception code.
            * @param previous - The previous exception used for the exception chaining.
            */
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }
    }

    public function update_permission($data){
        $method_code = "007";
        $method_name = "update-permission";

        $this->validate_update_permission($data);

        if(array_key_exists("privileges", $data)){
            $data['privileges'] = json_encode($data['privileges']);
        }

        $data['updated_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();   

        $udata = $data;
        
        try{
            Permission::where('id', $data['id'])->update(['name' => $data['name'], 'privileges' => $data['privileges'], 'updated_at' => $data['updated_at']]);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        } 
    }

    public function delete_permission($data)
    {
        $method_code = "008";
        $method_name = "delete-permission";

        $this->validate_get_permission($data);

        try{
            Permission::where('id', $data['id'])->delete();
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        } 
    }
}
