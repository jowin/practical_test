import Config from '@/js/config/app.js';
import Router from '@/js/routes';
import axios from 'axios';

export default {
    namespaced: true,
    state: {
        searchResult: '',
        infoSignal: {
            addSignal: '',   
            updateSignal: '',
            deleteSignal: '',
            quickSearchSignal: '',
            advanceSearchSignal: '',
        }, 
        field_error: {},
        isShowingUserInfo: '',
        statusList: '',
    },
    mutation: {
    },
    getters: {
    },
    actions: {
        retriveUserList({state, commit, rootState}, incomingData){
            axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('loginToken');
            axios.post(Config.base_url() + 'admin/user-modules/index', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['user_list_info'];
                }
            })
            .catch(error => console.log(error));             
        },
       
        executeQuickSearch({state, commit, rootState}, incomingData){         
			axios.post(Config.base_url() + 'admin/user-modules/index', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['user_list_info'];
                    state.infoSignal.quickSearchSignal = true;
                }
            })
            .catch(error => console.log(error));            
        },

        executeAdvanceSearch({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/user-modules/index', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['user_list_info'];
                    state.infoSignal.advanceSearchSignal = true;
                }
            })
            .catch(error => console.log(error));    
        },
        switchPage({state, commit, rootState}, incomingData){  
            axios.post(incomingData.url, incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['user_list_info'];
                }
            })
            .catch(error => console.log(error));             
        },
        getStatusList({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/user-modules/get-status-list', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.statusList = '';
                    state.statusList = res['data']['data']['info'];
                }
            })
            .catch(error => console.log(error));    
        },
        
        /* Create */       
        insertUser({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/user-modules/add-user', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.infoSignal.addSignal = Config.generalStatus['success'];
                    Router.push({ name: "user-module"});
                }else {
                    state.field_error = res['data']['field_error'];
                    state.infoSignal.addSignal = Config.generalStatus['error'];
                }
            })
            .catch(error => console.log(error)); 
        },
        /* End Create */

        /* Retrieve User Info */
        retreiveUserInfo({state, coimmit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/user-modules/get-user-info', incomingData)
            .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.isShowingUserInfo = res['data']['data']['info'];
                }  
            })
            .catch(error => console.log(error));
        },
        /* End User Info */ 
        
        /* Update User Info */
        updateUser({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/user-modules/update-user', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.infoSignal.updateSignal = Config.generalStatus['success'];
                    Router.push({ name: "user-module"});
                }else {
                    state.field_error = res['data']['field_error'];
                    state.infoSignal.updateSignal = Config.generalStatus['error'];
                }
            })
            .catch(error => console.log(error)); 
        },

        /* Delete User */
        deleteUser({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/user-modules/delete-user', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.infoSignal.deleteSignal = Config.generalStatus['success'];
                    Router.push({ name: "user-module"});
                }else {
                    state.field_error = res['data']['field_error'];
                    state.infoSignal.deleteSignal = Config.generalStatus['error'];
                }
            })
            .catch(error => console.log(error)); 
        },
    }
}