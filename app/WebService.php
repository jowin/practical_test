<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Lang;
use \App\ApiLogActivity;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use Illuminate\Support\Str;

class WebService extends Model
{	
	/* 
		Sample encrypte and decrypted
		******************************
		$data_key = "1jalfjsf"; // android developer give it.
		$data_iv = "9sJ1@rn292msiw23"; // must 16 length, give to android developer.
		$data_data = array("address" => "cheras");
		$test = $ws->encrypt($data_key, $data_iv, json_encode($data_data));
		d("encrypted:".$test);
		$decrypt_test = $ws->decrypt($data_key, $test);
		var_dump(json_decode($decrypt_test));	
	*/
	
	public static $oauth_client_id = 2;
	public static $oauth_client_secret = 'yDh8iPFhmZXUMDrrGsL0NBlDWlMJUeT0wAaCp0Us';
	
	private static $OPENSSL_CIPHER_NAME = "aes-128-cbc"; //Name of OpenSSL Cipher 
    private static $CIPHER_KEY_LEN = 16; //128 bits
	private static $iv = "9sJ1@rn292msiw23";
	private static $key = "sjaldf238013s2f5";
	
	private static $revoked_to_code = array(
		"yes" => 1,
		"no" => 0,
	);
	
    public static $api_code_to_status = array(
		"field-error" => 600,
		"internal-server-error" => 700,
		"maintenance" => 900,
		"success" => 1000,
	);
	
	public static $api_status_to_code = array(
		600 => "field-error",
		700 => "internal-server-error",
		900 => "maintenance",
		1000 => "success",
	);
	
	public static $api_error_code_to_detail = array(
		"internal-server-error" => array(
			701 => "invalid-api-version",
			702 => "invalid-signature",
			703 => "user-not-found",
			704 => "system-error",
			705 => "send-reset-password-link",
		),
		"field-error" => array(
			601 => "empty-wsid",
			602 => "input-field-error",
		),
		"maintenance" => array(
			901 => "system-maintenance"
		),
	);
	
	public static $api_error_detail_to_code = array(
		"internal-server-error" => array(
			"invalid-api-version" => 701,
			"invalid-signature" => 702,
			"user-not-found" => 703,
			"system-error" => 704,
			"send-reset-password-link" => 705,
		),
		"field-error" => array(
			"empty-wsid" => 601,
			"input-field-error" => 602,
		),
		"maintenance" => array(
			"system-maintenance" => 901,
		),
	);
	
	public static $api_error_message = array(
		"internal-server-error" => array(
			"invalid-api-version" => "trans.api/general/internal-server-error",
			"invalid-signature" => "trans.api/general/internal-server-error",
			"user-not-found" => "trans.api/general/internal-server-error",
			"system-error" => "trans.api/general/internal-server-error",
			"send-reset-password-link" => "trans.api/general/internal-server-error",
		),
		"field-error" => array(
			"input-field-error" => 'trans.api/general/field-error',
			"empty-wsid" => 'trans.api/general/empty-wsid',
		),
		"maintenance" => array(
			"system-maintenance" => 'trans.api/general/system-maintenance',
		),
	);
	
	public static $token_type = "Bearer";

	protected $class_code = '4500';

	public $err_query_info = ["code" => "", "message" => "", "params" => array()];
    
    public $custom_err_info = ["code" => "", "message" => "", "params" => array()];
	
	/*
	Format will be like following:
	Array(
		"status": 700,
		"message": "Internal Server Error",
		"data": "",
		"internal": {
			"dev_status": 701,
			"dev_msg": "Invalid API Version"
		},
		"field": []
	)
	*/
	
	public function api_result($status = null, $data = array(), $field_error = array()){
		$method_code = '001';
		$method_name = "api-result";

		$last_status_code = ( (isset($data['last_status_code'])) && (!empty($data['last_status_code'])) )? $data['last_status_code'] : 0;
		$result = array();
		$transformed = array();

		if((!empty($field_error)) && ($status == static::$api_error_detail_to_code['field-error']['input-field-error'])){
			$last_status_code = static::$api_error_detail_to_code['field-error']['input-field-error'];
			$result['status'] = static::$api_code_to_status['field-error'];
			$result['message'] = Lang::get(static::$api_error_message['field-error']['input-field-error']);
			$result["data"] = "";
			$result['internal'] = array(
				"dev_status" => static::$api_error_detail_to_code['field-error']['input-field-error'],
				"dev_msg" => "Field Error",
			);
			$id = 1;
			
			foreach ($field_error as $lang => $messageBag) {
				foreach($messageBag->toArray() as $field => $message ){
					$transformed[$lang][] = [
						'field' => $field,
						'message' => $message[0],
						'id' => $id,
					];	
					$id++;
				}
			}	
			$result['field_error'] = $transformed;
		}elseif($status == static::$api_error_detail_to_code['field-error']['empty-wsid']){
			$last_status_code = $status;
			$result['status'] = static::$api_code_to_status['field-error'];
			$result['message'] = Lang::get(static::$api_error_message['field-error']['empty-wsid']);
			$result["data"] = '';
			$result['internal'] = array(
				"dev_status" => static::$api_error_detail_to_code['field-error']['empty-wsid'],
				"dev_msg" => "Web Serive ID is required",
			);	
			$result['field_error'] = $field_error;
		}elseif($status == static::$api_error_detail_to_code['internal-server-error']['invalid-signature']){
			$last_status_code = $status;
			$result['status'] = static::$api_code_to_status['internal-server-error'];
			$result['message'] = Lang::get(static::$api_error_message['internal-server-error']['invalid-signature']);
			$result["data"] = '';
			$result['internal'] = array(
				"dev_status" => static::$api_error_detail_to_code['internal-server-error']['invalid-signature'],
				"dev_msg" => "Invalid Signature",
			);	
			$result['field_error'] = $field_error;
		}elseif($status == static::$api_error_detail_to_code['internal-server-error']['send-reset-password-link']){
			$last_status_code = $status;
			$result['status'] = static::$api_code_to_status['internal-server-error'];
			$result['message'] = Lang::get(static::$api_error_message['internal-server-error']['send-reset-password-link']);
			$result["data"] = '';
			$result['internal'] = array(
				"dev_status" => static::$api_error_detail_to_code['internal-server-error']['send-reset-password-link'],
				"dev_msg" => $data['dev_msg'],
			);	
			$result['field_error'] = $field_error;
		}elseif($status == static::$api_code_to_status['success']){	
			$last_status_code = static::$api_code_to_status['success'];	
			$result['status'] = $status;
			$result['message'] = "success";
			$result["data"] = $data;
			$result['internal'] = array(
				"dev_status" => "",
				"dev_msg" => "",
			);	
			$result['field_error'] = $field_error;
		}elseif($status == static::$api_code_to_status['maintenance']){
			$last_status_code = static::$api_error_detail_to_code['maintenance']['system-maintenance'];	
			$result['status'] = $status;
			$result['message'] = Lang::get(static::$api_error_message['maintenance']['system-maintenance']);
			$result["data"] = "";
			$result['internal'] = array(
				"dev_status" => static::$api_error_detail_to_code['maintenance']['system-maintenance'],
				"dev_msg" => "System Maintenance",
			);	
			$result['field_error'] = $field_error;
		}elseif($status == static::$api_error_detail_to_code['internal-server-error']['system-error']){
			$last_status_code = static::$api_error_detail_to_code['internal-server-error']['system-error'];	
			$result['status'] = $status;
			$result['message'] = Lang::get(static::$api_error_message['internal-server-error']['system-error']);
			$result["data"] = '';
			$result['internal'] = array(
				"dev_status" => static::$api_error_detail_to_code['internal-server-error']['system-error'],
				"dev_msg" => $data['dev_msg'],
			);	
			$result['field_error'] = $field_error;
		}else{
			$last_status_code = $data['dev_status'];				
			$result['status'] = $status;
			$result['message'] = $data['general_msg'];
			$result["data"] = $data;
			$result['internal'] = array(
				"dev_status" => $data['dev_status'],
				"dev_msg" => $data['dev_msg'],
			);	
			$result['field_error'] = $field_error;
		}

		# update api log record
		if($status != static::$api_error_detail_to_code['field-error']['empty-wsid']){
			$apilog = new ApiLogActivity();
			$apilog::where('wsid', \Request::get('global_wsid'))
					->where('url', \Request::get('global_url'))
					->update(['last_status_code' => $last_status_code, 'last_response' => json_encode($result)]);
		}

		return $result;
	}
	
	public function retreive_ws_record($data){
		$api = ApiLogActivity::where('wsid', $data['wsid'])->first();
		
		return $api;
	}
	
	/**
     * Encrypt data using AES Cipher (CBC) with 128 bit key
     * 
     * @param type $key - key to use should be 16 bytes long (128 bits)
     * @param type $iv - initialization vector
     * @param type $data - data to encrypt
     * @return encrypted data in base64 encoding with iv attached at end after a :
    */

    public static function encrypt($key, $iv, $data) {
		$method_code = '002';
		$method_name = "encrypt";

		$iv = ( empty($iv) )? static::$iv : $iv;
		$key = ( empty($key) )? static::$key : $key;
        if (strlen($key) < static::$CIPHER_KEY_LEN) {
            $key = str_pad("$key", static::$CIPHER_KEY_LEN, "0"); //0 pad to len 16
        } else if (strlen($key) > static::$CIPHER_KEY_LEN) {
            $key = substr($str, 0, static::$CIPHER_KEY_LEN); //truncate to 16 bytes
        }

        $encodedEncryptedData = base64_encode(openssl_encrypt($data, static::$OPENSSL_CIPHER_NAME, $key, OPENSSL_RAW_DATA, $iv));
        $encodedIV = base64_encode($iv);
        $encryptedPayload = $encodedEncryptedData.":".$encodedIV;

        return $encryptedPayload;

    }
	
	/**
     * Decrypt data using AES Cipher (CBC) with 128 bit key
     * 
     * @param type $key - key to use should be 16 bytes long (128 bits)
     * @param type $data - data to be decrypted in base64 encoding with iv attached at the end after a :
     * @return decrypted data
    */
    public static function decrypt($key, $data) {
		$method_code = '003';
		$method_name = "decrypt";

		$key = ( empty($key) )? static::$key : $key;
		
        if (strlen($key) < static::$CIPHER_KEY_LEN) {
            $key = str_pad("$key", static::$CIPHER_KEY_LEN, "0"); //0 pad to len 16
        } else if (strlen($key) > static::$CIPHER_KEY_LEN) {
            $key = substr($str, 0, static::$CIPHER_KEY_LEN); //truncate to 16 bytes
        }

        $parts = explode(':', $data); //Separate Encrypted data from iv.
        $decryptedData = openssl_decrypt(base64_decode($parts[0]), static::$OPENSSL_CIPHER_NAME, $key, OPENSSL_RAW_DATA, base64_decode($parts[1]));
		
        return $decryptedData;
    }
	
	public function getTokenDetailInfo($uid){
		$info = DB::table('oauth_access_tokens')->join('oauth_refresh_tokens', 'oauth_access_tokens.id', '=', 'oauth_refresh_tokens.access_token_id')->where('oauth_access_tokens.user_id', $uid)->select('oauth_access_tokens.id as oaccess_token', 'oauth_refresh_tokens.id as orefresh_token')->where('oauth_access_tokens.revoked', static::$revoked_to_code['no'])->where('oauth_access_tokens.expires_at', '>', Carbon::now('Asia/Kuala_Lumpur')->toDateTimeString())->where('oauth_access_tokens.revoked', static::$revoked_to_code['no'])->first();
		
		return $info;
	}
	
	public function update_refresh_token($data){
		$access_token_id = $data['access_token_id'];		
		DB::table('oauth_refresh_tokens')->where('access_token_id', $access_token_id)->update(['revoked' => static::$revoked_to_code['yes']]);
	}

	public function retrieve_user_token($table, $uid){
		$method_code = '004';
		$method_name = "retrive-user-token";

		try{
			$result = DB::table($table)->select('api_token', 'api_token_expire_date')->where('uid', $uid)->first();
			$result = (array)$result;
		}catch(CustomQueryException $e){
			$this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
		}

		return $result;
	}

	public function generate_new_token($table, $uid){
		$method_code = '005';
		$method_name = 'generate-new-token';

		$login_info = array(
			"login_token" => Str::random(60),
			"login_token_expire_date" => Carbon::now(config('app.server_timezone'))->addYear(1)->toDateTimeString(),
		);

		try{
			DB::table($table)->where('uid', $uid)->update( [
				'api_token' => $login_info['login_token'],
				'api_token_expire_date' => $login_info['login_token_expire_date'],
			]);
		}catch(CustomQueryException $e){
			$this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
		}

		return $login_info;
	}

	public function check_login_token($table, $uid){
		$method_code = "006";
		$method_name = "check-login_token";
		
		$tokenInfo = [];
		
		try{
			$tokenInfo = DB::table($table)->where('uid', $uid)->select('api_token', 'api_token_expire_date')->first();
			$tokenInfo = (array)$tokenInfo;
		}catch(CustomQueryException $e){
			$this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
		}
		
		if(!empty($tokenInfo)){
			if( Carbon::now(config('app.server_timezone'))->timestamp  >= strtotime($tokenInfo['api_token_expire_date']) ){
				try{
					$tokenInfo = $this->generate_new_token($table, $uid);
				}catch(CustomQueryException $e){
					$this->err_query_info['code'] = $e->getCode();
					$this->err_query_info['message'] = $e->getMessage();
					$this->err_query_info['params'] = $e->getBindings();
					throw new CustomGeneralException($method_name, $this->class_code.$method_code.'002');
				}
			}
		}

		return $tokenInfo;
	}
}
