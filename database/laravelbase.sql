-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 23, 2019 at 01:18 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `webbuildervue`
--

-- --------------------------------------------------------

--
-- Table structure for table `wb_actlogs`
--

DROP TABLE IF EXISTS `wb_actlogs`;
CREATE TABLE IF NOT EXISTS `wb_actlogs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `ip` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `get_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wb_admins`
--

DROP TABLE IF EXISTS `wb_admins`;
CREATE TABLE IF NOT EXISTS `wb_admins` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master` smallint(5) UNSIGNED NOT NULL,
  `dev` smallint(5) UNSIGNED NOT NULL,
  `privileges` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token_expire_data` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL COMMENT '10-active, 90-suspend, 100-terminated',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wb_admins_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wb_admins`
--

INSERT INTO `wb_admins` (`id`, `uid`, `name`, `mobile`, `master`, `dev`, `privileges`, `email`, `password`, `remember_token`, `api_token`, `api_token_expire_data`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'Developer', '0129232123', 1, 1, '', 'dev@', '$2y$10$Nkd.U4L2p.TvuD.G/dobkOP3Oaqv8/Uy0sw9soIGf8AZ00veqmQZS', '', 'cQw7QlNKgxnVLOG4dMr2rW2mupsrY6Oiadmh8FtCCIRj8cOEJsSAxryXryl5', '2020-07-23 21:16:30', 10, '2019-07-23 21:16:30', '2019-07-23 21:16:30'),
(2, 3, 'client', '0119283920', 1, 0, '', 'client@', '$2y$10$s3URkC/tYTrvSfJo3HRulu8sMg4SWyjL0EhWvmNESSv07.U3liAuC', '', 'G5dGqZB7PhlfbVj1Vw0kNbRCAuLnL54QwiI0ATSzIIKYgCyMfgtmNmb2XqPQ', '2020-07-23 21:16:30', 10, '2019-07-23 21:16:30', '2019-07-23 21:16:30'),
(3, 4, 'admin', '0119283920', 0, 0, '', 'admin@', '$2y$10$h9YpDiAj8YoXeLwB0GIBE.l0S2MOgxU5iXwfJuzdJ2Z3w03KaqW0.', '', 'vfvjW6cnj2kyPxtbalbkVW9EDi3bWz6MGsSbq9tXucBixJ9RXw1xcNtJ1W29', '2020-07-23 21:16:30', 10, '2019-07-23 21:16:30', '2019-07-23 21:16:30');

-- --------------------------------------------------------

--
-- Table structure for table `wb_crons`
--

DROP TABLE IF EXISTS `wb_crons`;
CREATE TABLE IF NOT EXISTS `wb_crons` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` smallint(5) UNSIGNED NOT NULL,
  `success` smallint(5) UNSIGNED NOT NULL,
  `failed` smallint(5) UNSIGNED NOT NULL,
  `rdate` datetime NOT NULL COMMENT 'run date',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wb_err_logs`
--

DROP TABLE IF EXISTS `wb_err_logs`;
CREATE TABLE IF NOT EXISTS `wb_err_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `ip` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `get_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `trace` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `err_msg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wb_merchants`
--

DROP TABLE IF EXISTS `wb_merchants`;
CREATE TABLE IF NOT EXISTS `wb_merchants` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ic` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_account_num` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token_expire_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `forgot_password_vcode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `register_vcode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL COMMENT '10-active, 90-suspend, 100-terminated',
  `register_vcode_cdate` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wb_merchants_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wb_merchants`
--

INSERT INTO `wb_merchants` (`id`, `uid`, `name`, `email`, `password`, `remember_token`, `api_token`, `api_token_expire_data`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Merchant', 'merchant@', '$2y$10$XKb9ixPTMRjphsbMhtBHo.EYl3TLL8VlMGn.DXtdFYXXhYHVCY.ru', '', 'iCeEeguOP42iDKJbn4eFbyOmHSRsVo2yCqhtGCLcKXvBkPlDACPpm2KM56gf', '2020-07-23 21:16:31', 10, '2019-07-23 21:16:31', '2019-07-23 21:16:31');

-- --------------------------------------------------------

--
-- Table structure for table `wb_migrations`
--

DROP TABLE IF EXISTS `wb_migrations`;
CREATE TABLE IF NOT EXISTS `wb_migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=155 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wb_migrations`
--

INSERT INTO `wb_migrations` (`id`, `migration`, `batch`) VALUES
(141, '2014_03_17_000000_create_act_logs_table', 1),
(142, '2014_03_18_000000_create_error_logs_table', 1),
(143, '2014_10_12_000000_create_users_table', 1),
(144, '2014_10_12_100000_create_password_resets_table', 1),
(145, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(146, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(147, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(148, '2016_06_01_000004_create_oauth_clients_table', 1),
(149, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(150, '2019_03_18_000000_create_admins_table', 1),
(151, '2019_03_18_010000_create_ws_logs_table', 1),
(152, '2019_03_18_020000_create_ws_users_table', 1),
(153, '2019_03_18_030000_create_crons_table', 1),
(154, '2019_03_18_040000_create_merchants_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wb_oauth_access_tokens`
--

DROP TABLE IF EXISTS `wb_oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `wb_oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wb_oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wb_oauth_auth_codes`
--

DROP TABLE IF EXISTS `wb_oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `wb_oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wb_oauth_clients`
--

DROP TABLE IF EXISTS `wb_oauth_clients`;
CREATE TABLE IF NOT EXISTS `wb_oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wb_oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wb_oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `wb_oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `wb_oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wb_oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wb_oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `wb_oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `wb_oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wb_oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wb_password_resets`
--

DROP TABLE IF EXISTS `wb_password_resets`;
CREATE TABLE IF NOT EXISTS `wb_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  KEY `wb_password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wb_users`
--

DROP TABLE IF EXISTS `wb_users`;
CREATE TABLE IF NOT EXISTS `wb_users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ic` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_account_num` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token_expire_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `forgot_password_vcode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `register_vcode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL COMMENT '10-active, 90-suspend, 100-terminited',
  `register_vcode_cdate` datetime NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `wb_users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wb_users`
--

INSERT INTO `wb_users` (`id`, `uid`, `name`, `email`, `mobile`, `ic`, `bank_name`, `bank_account_num`, `password`, `remember_token`, `api_token`, `api_token_expire_date`, `forgot_password_vcode`, `register_vcode`, `status`, `register_vcode_cdate`, `created_at`, `updated_at`) VALUES
(1, 1, 'Jack', 'jack@domain.com', '0129873878', '920901029581', 'PUBLIC BANK', '6489202383740', '$2y$10$XMALZpWQVDJq4BDsoKwix.EJFAoaSE052b0/ahXuKgsMVGcoRNnva', '', 'kujDETPueHvlJPd3LODLcgGT6ts6nsV5pqXPIRyEJEr4FV8PBDgLOmp4INDp', '2020-07-23 21:16:31', '', '', 10, NULL, '2019-07-23 21:16:31', '2019-07-23 21:16:31');

-- --------------------------------------------------------

--
-- Table structure for table `wb_ws_logs`
--

DROP TABLE IF EXISTS `wb_ws_logs`;
CREATE TABLE IF NOT EXISTS `wb_ws_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `wsid` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_response` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `get_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_status_code` smallint(5) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wb_ws_users`
--

DROP TABLE IF EXISTS `wb_ws_users`;
CREATE TABLE IF NOT EXISTS `wb_ws_users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL,
  `notification_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_type` smallint(5) UNSIGNED NOT NULL COMMENT '10-Android, 20-IOS',
  `device_agent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

-- --------------------------------------------------------

--
-- Table structure for table `wb_email_template`
--

DROP TABLE IF EXISTS `wb_email_template`;
CREATE TABLE IF NOT EXISTS `wb_email_template` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` int(5) UNSIGNED NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `status` INTEGER(3) UNSIGNED NOT NULL COMMENT '1-active, 0-inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wb_email_template_content`
--

DROP TABLE IF EXISTS `wb_email_template_content`;
CREATE TABLE IF NOT EXISTS `wb_email_template_content` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `eid` int(10) UNIQUE NOT NULL COMMENT 'email-template-code',
  `language` VARCHAR(10) NOT NULL,
  `subject` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP INDEX `eid` ON wb_email_template_content