<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ErrorLog extends Model
{
    protected $table = 'err_logs';

    protected $class_code = "9000";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'uid', 'ip', 'url', 'post_data', 'get_data', 'err_msg', 'trace', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function add_err_log($data = array())
    {
        try{
            $this->ip = \Request::getClientIp(true);
            $this->post_data = json_encode($_POST);
            $this->get_data = json_encode($_GET);
            $this->err_msg = $data["message"];
            $this->url = \Request::fullUrl();
            
            if(!empty(Auth()->guard('web')->user())){
                $uid = Auth()->guard('web')->user()->id;
            }elseif(!empty(Auth()->guard('admin')->user())){
                $uid = Auth()->guard('admin')->user()->id;
            }else{
                $uid = 0;
            }

            $this->created_at = Carbon::now(config('app.system_timezone'))->toDateTimeString();
            $this->updated_at = Carbon::now(config('app.system_timezone'))->toDateTimeString();
            $this->uid = $uid;
            $this->trace = (empty($data['trace']))? "" : $data['trace'];
            $this->save();
        }catch(Exception $ex){
            throw new Exception($this->class_code.$method_code.":".$ex->getMessage());
        }
    }
}
