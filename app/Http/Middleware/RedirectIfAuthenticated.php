<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard) {
            case 'admin-api':
				if(Auth::guard($guard)->check())
				{
					$ws = new WebService();
					$data['success'] = "authenticated";
					$ws->api_result($ws::$api_code_to_status['success'], $data);
					return response()->json($result);
				}
                break;
            case 'merchant-api':
				if(Auth::guard($guard)->check())
				{
					$ws = new WebService();
					$data['success'] = "authenticated";
					$ws->api_result($ws::$api_code_to_status['success'], $data);
					return response()->json($result);
				}
				break;
			case 'api':
				if(Auth::guard($guard)->check())
				{
					$ws = new WebService();
					$data['success'] = "authenticated";
					$ws->api_result($ws::$api_code_to_status['success'], $data);
					return response()->json($result);
				}
				break;
            default:
                if(Auth::guard($guard)->check())
                {
                    $ws = new WebService();
                    $data['success'] = "authenticated";
                    $ws->api_result($ws::$api_code_to_status['success'], $data);
                    return response()->json($result);
                }
                break;
        }

        return $next($request);
    }
}
