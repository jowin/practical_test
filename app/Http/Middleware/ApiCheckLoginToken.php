<?php

namespace App\Http\Middleware;

use Closure;
use App\WebService;
use Illuminate\Http\Request;
use Lang;
use App\Admin;
use App\Merchant;
use App\User;

class ApiCheckLoginToken
{
	public function __construct(Request $request) {
        $this->request = $request;
    }
	
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$ws = new WebService();

		if($this->request->has('login_token')){
			$login_token = $this->request->login_token;
			$comingPrefix = $request->route()->getPrefix();
			if(strpos($comingPrefix, Admin::$prefix)){
				$model_table = Admin::$admin_table;
			}elseif(strpos($comingPrefix, Merchant::$prefix)){
				$model_table = Merchant::$merchant_table;
			}elseif(strpos($comingPrefix, User::$prefix)){
				$model_table = User::$user_table;
			}else{
				$model_table = '';
			}
			$ws->check_login_token($model_table, $login_token);
		}
        return $next($request);
    }
}
