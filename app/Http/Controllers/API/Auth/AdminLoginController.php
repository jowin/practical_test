<?php 

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\WebService;
use App\Admin;
use App\User;
use Illuminate\Support\Facades\Auth; 
use App\WebServiceUser;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Lang;
use Lcobucci\JWT\Parser;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;


class AdminLoginController extends Controller
{
	public $successStatus = 200;

	/*
		$rules = ['inactive_bank_id' => 'required|valid_bank'];
		$message = ['inactive_bank_id.valid_bank' => "trans.admin/bank/inactive/error/message"];
		Validator::make($request->all(), $rules, $message);
	*/

	public function test_field_error(Request $request){
		$ws = new WebService();	
		$rules = [
			'fname' => 'required|max:191',     		
			'lname' => 'required|max:191',     		
			'phone' => 'required|numeric',
		];
		
		$validator  = Validator::make($request->all(),$rules);
		
		if ($validator ->fails()) {
			$result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $validator->errors()->toArray());
			return response()->json($result);
        }
		
		# return success json
		$status = $ws::$api_code_to_status['success'];
		$api_data["success"] = array(
			"fname" => $request->fname,
			"lname" => $request->lname,
			"phone" => $request->phone,
		); 
		
		$result = $ws->api_result($status, $api_data);
		
		return response()->json($result);
	}	

    public function logout(Request $request){
		$ws = new WebService();
		$value = $request->bearerToken();
		$id= (new Parser())->parse($value)->getHeader('jti');
        $token= $request->user()->tokens->find($id);
        $token->revoke();
		$udata['access_token_id'] = $id;
		$ws->update_refresh_token($udata);
		$data = array();		
		$data['success'] = Lang::get('trans.api/logout/success/message');
		$result = $ws->api_result($status = $ws::$api_code_to_status['success'], $data);
        
		return response()->json($result);
	}

	public function register(Request $request) { 
		$ws = new WebService();
		$ws_user = new WebServiceUser();
		$admin = new Admin();
		
		$form_fields = array("name", "password", "email", "mobile", "privileges");

        foreach($form_fields as $field){
            $data[$field] = $request->$field;
        }

        try{
            $admin->validate_add_admin($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '2000001200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $admin->err_field_params->toArray());
                    return response()->json($result);  
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array(), $field_error = array());
                	return response()->json($result);  
                    break;
            }   
        }

        try{
            $aid = $admin->add_admin($data);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;
            }    
            return response()->json($result);  
        }

        try{
            $adminInfo = $admin->get_admin_module_detail($uid='',$id=$aid);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;
            }    
            return response()->json($result);  
        }
        
        try{
        	$wsdata = array("uid" => $adminInfo['uid']);
            $ws_user->add_ws_user($wsdata);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $admin->err_field_params['message']))), $field_error = array());
                    break;
            }    
            return response()->json($result);  
        }

		$adata = array();
		
		$adata['success'] = array(
			"profile" => $adminInfo,
		);
		
		$result = $ws->api_result($ws::$api_code_to_status['success'], $adata);
		
		return response()->json($result);
	}

	public function login(Request $request){
		$admin = new Admin();
		$ws = new WebService();

		$result = [];

		$form_fields = array("email", "password");

        foreach($form_fields as $field){
            $data[$field] = $request->$field;
		}
		
		try{
			$admin->validate_login($data);
		}catch(CustomValidationException $e){
			$err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '2000010200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $admin->err_field_params->toArray());
                    return response()->json($result);  
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array(), $field_error = array());
                	return response()->json($result);  
                    break;
            }   
		}

		$result = $admin->login($data);

		if(empty($result)){
			$status = $ws::$api_code_to_status['internal-server-error'];
			$data = array(
				"dev_status" => $ws::$api_error_detail_to_code['internal-server-error']['user-not-found'],
				"general_msg" => Lang::get('trans.api/login/error'),
				"success" => "",
				"dev_msg" => "User Not Found.",
			); 
			$result = $ws->api_result($status, $data);
            return response()->json($result); 
		}

		return response()->json($result);
	}
}

?>