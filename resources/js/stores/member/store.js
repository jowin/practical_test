import Vue from 'vue'
import Vuex from 'vuex'
import Config from '@/js/config/app.js';
import Router from '@/js/routes';
import langEnglish from '@/js/lang/en.js';
import langChinese from '@/js/lang/zh-CN.js';
import validateLangEnglish from '@/js/lang/validation-en.js';
import validateLangChinese from '@/js/lang/validation-zh-CN.js';
import LoginModules from '@/js/stores/modules/member/login.js';
import LogoutModule from '@/js/stores/modules/member/logout.js';
import MemberProfileModules from '@/js/stores/modules/member/profile.js';

Vue.use(Vuex)

console.log('here');

export default new Vuex.Store({
    modules: {
        LoginModules: LoginModules,
        LogoutModule: LogoutModule,
        MemberProfileModules: MemberProfileModules,
    }, 
    state: {
        result: {
           status: null,
           message: null,
        },
        authInfo: {
            token: null,
            expireDate: null,
            isAuthenticated: null,
            status: null,
            message: null,
        },
        memberInfo: { 
            id: null,
            uid: null,
            name: null,
            email: null,
        },
    },
    mutations: {
        saveAuthInfo(state, memberData){
            localStorage.setItem('loginToken', memberData.loginToken); 
            localStorage.setItem('loginTokenExpireDate', memberData.loginTokenExpireDate); 

            state.authInfo.token = memberData.loginToken;
            state.authInfo.expireDate = memberData.loginTokenExpireDate;
            state.authInfo.status = Config.generalSuccessStatus;
            state.authInfo.message = 'success';
            state.authInfo.isAuthenticated = true;  
        },
        saveMemberInfo(state, memberData){
            localStorage.setItem('memberInfo', JSON.stringify(memberData.memberInfo));
            state.memberInfo.id = memberData.memberInfo.id;
            state.memberInfo.uid = memberData.memberInfo.uid;
            state.memberInfo.name = memberData.memberInfo.name;  
            state.memberInfo.email = memberData.memberInfo.email;
        },

    }, 
    actions: {
        tryAutoLogin ({commit}) {
            const token = localStorage.getItem('loginToken');
            if (!token) {
                return;
            }
            const expirationDate = localStorage.getItem('loginTokenExpireDate');
            const now = new Date()
            if (now >= expirationDate) {
                return;
            }
            let aInfo = localStorage.getItem('memberInfo');
            aInfo = JSON.parse(aInfo);
            
            commit('saveAuthInfo', {
                loginToken: token,
                loginTokenExpireDate: expirationDate,
            })
           
            commit('saveMemberInfo', {memberInfo: aInfo, saveLocale: false});
           
            if (Router.currentRoute.name == 'member.login' || Router.currentRoute.name == 'member' || !Router.currentRoute.name){
                Router.replace({name: 'member-dashboard'});
            } 
        },
        swtichLanguage({commit, dispatch}, ldata){
            if(localStorage.getItem('language') != ldata.choosed){
                localStorage.setItem('language', Config.locale[ldata.choosed]);
                ldata.app.$root.langValue = Config.locale[ldata.choosed]
                if(ldata.choosed == Config.locale_name['en']){
                    ldata.app.$root.language = langEnglish;
                    ldata.app.$root.validateLanguage = validateLangEnglish;
                }else{
                    ldata.app.$root.language = langChinese;
                    ldata.app.$root.validateLanguage = validateLangChinese;
                }
            }
        },
    },
    getters: {
        authResult(state){
            return state.authInfo;
        },
        isAuthenticated(state){
            return state.authInfo.isAuthenticated;
        },
        memberInfo(state){
            return state.memberInfo;
        },
    },
});

