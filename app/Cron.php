<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cron extends Model
{
    protected $table = "crons";

    public $timestamps = false;

    protected $fillable = [
        'id', 'type', 'success', 'failed', 'rdate', 'created_at', 'updated_at',
    ];

    public static $success_code = 1;

    public static $failed_code = 1;

    public static $type_to_code = array(
        "send-email" => 1000,
        "insert-record" => 1001,
    );

    public static $local_timezone = "Asia/Kuala_Lumpur";
}
