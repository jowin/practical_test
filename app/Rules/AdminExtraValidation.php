<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Admin;

class AdminExtraValidation implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $confirm_valid = true;
        $privileges_list = array();

        switch ($attribute) {
            case 'privileges':
                foreach(Admin::$privileges_list as $privileges_name => $privileges_info){
                    foreach($privileges_info['sub'] as $action_name => $detail){
                        array_push($privileges_list, $detail['id']);
                    }
                }

                foreach($value as $posted_privileges){
                    if(!in_array($posted_privileges, $privileges_list) || !is_numeric($posted_privileges)){
                        $confirm_valid = false;
                        break;
                    }
                }
                break;
                
            case 'admin_uid':
                $confirm_valid = false;
                $valid_admin = Admin::select('uid')->where('uid', $value)->first();
                if(empty($valid_admin)){
                    break;
                }
                if(!empty($valid_admin->toArray())){
                    $confirm_valid = true;
                }
                break;
            case 'status':
                $confirm_valid = false;
                if(in_array($value, Admin::$status_to_code)){
                    $confirm_valid = true;
                    break;
                }
                break; 
            default:
                break;
        }

        return $confirm_valid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.valid_data');
    }
}
