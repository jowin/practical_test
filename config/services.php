<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

    'pepipost' => [
        'driver' => "smtp",
        'host' => "sg2plcpnl0186.prod.sin2.secureserver.net", 
        'port' => 465,
        'from' => [
            'address' => "support@jomwin8.com", 
            'name' => config('app.name'),
        ],
        'encryption' => "ssl",
        'username' => "support@jomwin8.com",
        'password' => "CZst&S@g*j7m",
        'sendmail' => '/usr/sbin/sendmail -bs',
        'markdown' => [
            'theme' => 'default',
            'paths' => [
                resource_path('views/vendor/mail'),
            ],
        ],

    ],

    /* Development */
    'mailtrap' => [
        'driver' => 'smtp',
        'host' => 'smtp.mailtrap.io',
        'port' => '2525',
        'from' => [
            'address' => 'support@domain.com',
            'name' => config('app.name'),
        ],
        'encryption' => 'tls',
        'username' => "1388c1a4a0a05a",
        'password' => "3bbd10fb13ad63",
        'sendmail' => '/usr/sbin/sendmail -bs',
        'markdown' => [
            'theme' => 'default',
            'paths' => [
                resource_path('views/vendor/mail'),
            ],
        ],
    ],
];
