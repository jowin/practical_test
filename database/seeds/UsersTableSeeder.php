<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(User::$user_table)->insert([
            'uid' => 1,
            'name' => "Jack",
            'email' => 'jack@'.strtolower(config('app.domain')),
            'password' => bcrypt('qwe123'),
            'remember_token' => '',
            'api_token' => Str::random(60),
            'api_token_expire_date' => Carbon::now(config('app.server_timezone'))->addYear(1)->toDateTimeString(),
            'forgot_password_vcode' => '',
            'register_vcode' => '',
            'register_vcode_cdate' => null,
            'status' => User::$status_to_code['active'],
            'created_at' => Carbon::now(config('app.server_timezone'))->toDateTimeString(),
            'updated_at' => Carbon::now(config('app.server_timezone'))->toDateTimeString(),
        ]);
    }
}
