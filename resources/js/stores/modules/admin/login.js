import Config from '@/js/config/app.js';
import axios from 'axios';
import Router from '@/js/routes';

export default {
    namespaced: true,
    state: {
        authInfo: {
            token: null,
            expireDate: null,
            isAuthenticated: null,
            status: null,
            message: null,
        },
        adminInfo: { 
            id: null,
            uid: null,
            name: null,
            mobile: null,
            master: null,
            dev: null,
            privileges: null,
            email: null,
            status: null,
        },
    },
    mutations: {
        saveAuthInfo(state, adminData){
            localStorage.setItem('loginToken', adminData.loginToken); 
            localStorage.setItem('loginTokenExpireDate', adminData.loginTokenExpireDate); 

            state.authInfo.token = adminData.loginToken;
            state.authInfo.expireDate = adminData.loginTokenExpireDate;
            state.authInfo.status = Config.generalStatus["success"];
            state.authInfo.message = 'success';
            state.authInfo.isAuthenticated = true;           
        },
        saveAdminInfo(state, adminData){
            if(adminData.saveLocal){   
                localStorage.setItem('adminInfo', JSON.stringify(adminData.adminInfo));
            }
            state.adminInfo.id = (adminData.adminInfo.id != undefined)? adminData.adminInfo.id : state.adminInfo.id;
            state.adminInfo.uid = (adminData.adminInfo.uid != undefined)? adminData.adminInfo.uid : state.adminInfo.uid;
            state.adminInfo.name = (adminData.adminInfo.name != undefined)? adminData.adminInfo.name : state.adminInfo.name;
            state.adminInfo.mobile = (adminData.adminInfo.mobile != undefined)? adminData.adminInfo.mobile : state.adminInfo.mobile;
            state.adminInfo.master = (adminData.adminInfo.master != undefined)? adminData.adminInfo.master : state.adminInfo.master;
            state.adminInfo.dev = (adminData.adminInfo.dev != undefined)? adminData.adminInfo.dev : state.adminInfo.dev;
            state.adminInfo.privileges = (adminData.adminInfo.privileges != undefined)? adminData.adminInfo.privileges : state.adminInfo.privileges;
            state.adminInfo.status = (adminData.adminInfo.status != undefined)? adminData.adminInfo.status : state.adminInfo.status;   
            state.adminInfo.email = (adminData.adminInfo.email != undefined)? adminData.adminInfo.email : state.adminInfo.email;
        },
        errorAuthInfo(state, validateLanguage){
            state.authInfo.token = null;
            state.authInfo.expireDate = null;
            state.authInfo.status = Config.generalErrorStatus;
            state.authInfo.message = validateLanguage['auth/failed']; 
            state.authInfo.isAuthenticated = false;
        },
    },
    getters: {        
    },
    actions: {   
        login({state, commit, rootState}, authData){      
            axios.post(Config.base_url() + 'admin/login', authData)
                .then(res => {
            	if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    const loginToken = res['data']['data']['auth']['api_token'];
                    const loginTokenExpireDate = res['data']['data']['auth']['api_token_expire_date'];
                    const aInfo = res['data']['data']['profile'];            
                    commit('saveAuthInfo', {loginToken: loginToken, loginTokenExpireDate: loginTokenExpireDate});
                    commit('saveAdminInfo', {adminInfo: aInfo, saveLocal: true});
                    Router.replace({name: "admin-index"});
                    Router.go({name: "admin-index"});
            	} else {
                    commit('errorAuthInfo', authData.vlanguage);
                }
                
            })
            .catch(error => console.log(error));
        },
    },

}