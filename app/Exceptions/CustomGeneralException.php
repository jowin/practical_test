<?php

namespace App\Exceptions;

use Exception;

class CustomGeneralException extends Exception
{
   	public function report(){
    }

    public function render($request){
    	return $this->code;
    }
}
