<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;
use App\Admin;

class IncAction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Auth::guard('admin')->user());
        // dd(Auth::guard('web')->user());
        # admin side
        if($request->segment(1) || $request->segment(2) == Admin::$prefix){
            if(!is_null(Auth::guard('admin')->user())){
                
                if(!in_array(custom_route_name(), Admin::$unprivileges_page)){
                    # count notification

                    # check privileges
                    if(!has_previleges( custom_route_name() )){
                        return new Response(view('admin/internal/no-privileges'));
                    }
                }
            }
        }

        # user side
        if($request->segment(1) || $request->segment(2) == User::$prefix){
            if(!is_null(Auth::guard('web')->user())){
                # count notification
            }
        }

        # public side
        if($request->segment(1) != Admin::$prefix  || $request->segment(2) != Admin::$prefix  && $request->segment(1) != User::$prefix || $request->segment(2) != User::$prefix){

        }

        return $next($request);
    }
}
