import Config from '@/js/config/app.js';
import axios from 'axios';
import Router from '@/js/routes';

export default {
    namespaced: true,
    state: {
        memberInfo: { 
            id: null,
            uid: null,
            name: null,
            email: null,
        },
        updateInfoSignal: {
            updateProfileSignal: '',
            updatePasswordSignal: '',
        },
        field_error: {},
        profile_password_field_error: {},
    },
    mutations: {
        saveMemberInfo(state, memberData){
            localStorage.setItem('memberInfo', JSON.stringify(memberData.memberInfo));
            state.memberInfo.id = memberData.memberInfo.id;
            state.memberInfo.uid = memberData.memberInfo.uid;
            state.memberInfo.name = memberData.memberInfo.name;
            state.memberInfo.email = memberData.memberInfo.email;
        },
        field_error_info(state, fieldInfo){
            state.field_error = fieldInfo;
        },
        updateInfoSignal(state, signalInfo){
            state.updateInfoSignal.updateProfileSignal = signalInfo.updateProfileSignal;
            state.updateInfoSignal.updatePasswordSignal = signalInfo.updatePasswordSignal;
        },
        profile_password_field_error_info(state, fieldInfo){
            state.profile_password_field_error = fieldInfo;
        },
    },
    getters: {        
    },
    actions: {   
        updateProfile({state, commit, rootState}, aInfo){     
            axios.post(Config.base_url() + 'member/update-profile', aInfo)
                .then(res => {
            	if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    const aInfo = res['data']['data']['profile'];
                    commit('saveMemberInfo', {memberInfo: aInfo, saveLocal: true});                    
                    commit('updateInfoSignal', {updateProfileSignal: Config.generalStatus['success']});
            	} else {
                    commit('updateInfoSignal', {updateProfileSignal: Config.generalStatus['error']});
                    commit('field_error_info', res['data']['field_error']);
                }
            })
            .catch(error => console.log(error));
        },
        updatePassword({state, commit, rootState}, pInfo){
            axios.post(Config.base_url() + 'member/update-password', pInfo)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    const aInfo = res['data']['data']['profile'];                    
                    commit('saveMemberInfo', {memberInfo: aInfo, saveLocal: true});
                    commit('updateInfoSignal', {updatePasswordSignal: Config.generalStatus['success']});
            	} else {
                    commit('updateInfoSignal', {updatePasswordSignal: Config.generalStatus['error']});
                    commit('profile_password_field_error_info', res['data']['field_error']);
                }
            })
            .catch(error => console.log(error));
        },
    },
}