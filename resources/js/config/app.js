const prefix = {
	'admin_prefix': 'admin',
	'member_prefix': 'member',
};

const locale = {
	'english': 'en',
	'chinese': 'zh-CN',
};

const locale_name = {
	'en': 'english',
	'zh-CN': 'chinese',
};

const date = () => {
	let today = new Date();
	return today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
}

const time = () => {
	let today = new Date();
	let seconds = Number(today.getSeconds()) + 1;
    if ( seconds < 10 ) {
        seconds = "0" + seconds;
    } else {
        seconds = seconds;
    }
    return today.getHours() + ":" + today.getMinutes() + ":" + seconds;
};

const base_url = () => {
	return window.location.origin + '/api/latest/';
};

const getWSID = () => {
	let d = new Date();
	return Date.parse(d);
};

const generalStatus = {
	"success" : 1,
	"error" : 2,
};

const infoSearchType = {
	'quick': "1",
	'advance': "2",
};

const generatePagination = (current_page = '', prev_page_url = '', first_page_url = '', first_page = 1, last_page = '', last_page_url = '', next_page_url = '', path = '') => {
	return {
		'current_page': current_page,
		'prev_page_url': prev_page_url,
		'first_page_url': first_page_url,
		'first_page': first_page,
		'last_page': last_page,
		'last_page_url': last_page_url,
		'next_page_url': next_page_url,
		'path': path,
	};
};

export default { prefix, locale, date, time, base_url, getWSID, locale_name, generalStatus, infoSearchType, generatePagination };