<?php 

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use App\LogActivity;
use Auth;

class LoadActivity  
{
    

    public function __construct(Application $app, Redirector $redirector, Request $request) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $la = new LogActivity;
            $la->ip = $this->request->ip();
            $la->post_data = json_encode($_POST);
            $la->get_data = json_encode($_GET);            
            $la->url = $this->request->fullUrl();

            if(!empty(Auth()->guard('web')->user())){
                $uid = Auth()->guard('web')->user()->id;
            }elseif(!empty(Auth()->guard('admin')->user())){
                $uid = Auth()->guard('admin')->user()->id;
            }else{
                $uid = 0;
            }

            $la->uid = $uid;
            $la->save();

            return $next($request);
        }catch(Exception $e){
            dd($e);
        }
    }
}