<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MerchantResetPasswordNotification;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use Illuminate\Database\QueryException;
use Validator;
use App;
use Carbon\Carbon;
use DB;
use Auth;
use App\Rules\MerchantExtraValidation;

class Merchant extends Authenticatable
{
    use Notifiable;

    protected $guard = 'merchant';

    protected $table = "merchants";

    public static $prefix = "merchant";

    public static $merchant_table = "merchants";

    public static $status_to_code = array(
        'active' => 10,
        'suspend' => 90,
        'terminited' => 100,
    );
	public static $code_to_text = array(
        10 => "trans.person/status/active",
        90 => "trans.person/status/suspend",
        100 => "trans.person/status/terminated"
    );

    protected $class_code = "3000";

    public $err_query_info = ["code" => "", "message" => "", "params" => array()];
    
    public $custom_err_info = ["code" => "", "message" => "", "params" => array()];

    public $cust_err_field_params = array();

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'mobile', 'ic', 'bank_name', 'bank_account_num', 'remember_token', 'api_token', 'api_token_expire_date', 'forgot_password_vcode', 'register_vcode', 'status', ' 	register_vcode_cdate ', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MerchantResetPasswordNotification($token));
    }

    protected $insert_rules = [
        'name' => 'required',
        'password' => 'required|min:6',
        'mobile' => 'required|min:10|numeric',
        'ic' => 'required|digits_between:12,12|numeric',
        'bank_name' => "required",
        'bank_account_num' => "required|numeric"
    ];

    protected $login_rules = [
        'email' => 'required|email', 
        'password' => 'required|min:6', 
    ];

    protected $reset_password_rules = [
        'email' => 'required|email',
        'password' => 'required|min:6',
        'code' => 'required',
        'confirm_password' => 'required|min:6|same:password',
    ];

    public function validate_add_merchant($data){
        $method_code = "001";
        $method_name = "validate-add-merchant";

        $messages = [];
        
        $this->insert_rules['email'] = ['required', 'email', 'unique:merchants', new AuthExtraValidation];
        $validator = Validator::make($data, $this->insert_rules, $messages); 

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;      
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }        
    }

    public function generate_unique_id()
    {
        $random_id = 0;

        do{
            $random_id = mt_rand(00000000, 99999999);
            $info = DB::table(static::$merchant_table)->select('uid')->where('uid', $random_id)->get();
        }while(!$info->isEmpty());

        return $random_id;
    }

    public function add_merchant($data){
        $method_code = "002";
        $method_name = "add-merchant";
    
        $data['uid'] = $this->generate_unique_id();
        $data['created_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();   
        $data['updated_at'] = Carbon::now(config('app.system_timezone'))->toDateTimeString();
        $data['status'] = static::$status_to_code['active'];
        
        $this->validate_add_merchant($data);

        $data['password'] = bcrypt($data['password']);
        $data['remember_token'] = "";

        try{
            $aid = DB::table($this::$merchant_table)->insertGetId($data);
        }catch(QueryException $e){
            dd($e->getMessage());
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }  

        return $aid;
    }

    public function get_merchant_detail($uid = '',$id = '')
    {
        $method_code = "003";
        $method_name = "get-merchant-module-detail";

        try{
            $result = Merchant::where(['uid'=> $uid])->orWhere(['id' => $id])->first();
            $result = json_decode($result, true);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomQueryException($method_name, $this->class_code.$method_code.'001');
        }

        return $result;
    }

    public function validate_login($data){
        $method_code = "004";
        $method_name = "validate-login";

        $messages = [];
        $validator = Validator::make($data, $this->login_rules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;      
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }  
    }

    public function login($data){
        $method_code = "005";
        $method_name = "login";
        
        $this->validate_login($data);

        $result = [];

        if(Auth::guard('merchant')->attempt(['email' => $data['email'], 'password' => $data['password']])){
            $ws = new WebService();
            $ws_user = new WebServiceUser();

            $merchant = Auth::guard('merchant')->user();
            $rdata = array();
            
            // check previous time has login token or not            
            $currentTokenInfo = $ws->retrieve_user_token(static::$merchant_table, $merchant->uid);

            if(!empty($currentTokenInfo['api_token']) && isset($currentTokenInfo['api_token'])){
                try{
                    $tokenInfo = $ws->check_login_token(static::$merchant_table, $merchant->uid);
                }catch(CustomGeneralException $e){
                    $this->err_query_info['code'] = $e->getCode();
                    $this->err_query_info['message'] = $e->getMessage();
                    $this->err_query_info['params'] = $e->getBindings();
                    throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
                }

                $rdata = array(
                    "profile" => $merchant->get_merchant_detail($merchant->uid),
                    "auth" => $tokenInfo,
                );                
                $result = $ws->api_result($ws::$api_code_to_status['success'], $rdata);
                return $result;
            }
            
            try{
                $loginTokenInfo = $ws->generate_new_token(static::$merchant_table, $merchant->uid);
            }catch(CustomGeneralException $e){
                $this->err_query_info['code'] = $e->getCode();
                $this->err_query_info['message'] = $e->getMessage();
                $this->err_query_info['params'] = $e->getBindings();
                throw new CustomGeneralException($method_name, $this->class_code.$method_code.'002');
            }

            $adata = array(
                "profile" => $this->get_merchant_detail($merchant->uid),
                "auth" => $loginTokenInfo,
            );
            
            $result = $ws->api_result($ws::$api_code_to_status['success'], $adata);            
        }
        
        return $result;
        
    }

    public function validate_forgot_password($data){
        $method_code = "006";
        $method_name = "validate-forgot-password";

        $messages = [];
        $validateField['email'] = ['required', 'email', new MerchantExtraValidation];
        $validator = Validator::make($data, $validateField, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;     
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        }   
    }

    public function forgot_password($data){
        $method_code = "007";
        $method_name = "forgot-password";

        $this->validate_forgot_password($data);
        
        $code = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 16);
        $system = new System();
        $third_party_mail = new ThirdPartyMail($system->pickEmailProvider());
        $ws = new WebService();
        $emailParam = array();
        $emailParam['email'] = $data['email'];

        try{
            Merchant::where('email', $data["email"])->update(['forgot_password_vcode' => $code]);
            $info = Merchant::where('email', $data['email'])->first();
            $emailParam['forgot_password_link'] = url('reset-password?email='. $info['email'] . '&vcode=' . $info['forgot_password_vcode']);                      
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }

        try{            
            $third_party_mail->sendForgotPassword($emailParam);   
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'002');
        }

        $adata = array(
            "url" => $emailParam['forgot_password_link'],
        );

        $result = $ws->api_result($ws::$api_code_to_status['success'], $adata);    

        return $result;
    }

    public function validate_reset_password($data){
        $method_code = '008';
        $method_name = "validate-reset-password";

        $messages = [];
        $validator = Validator::make($data, $this->reset_password_rules, $messages);

        foreach(config('app.validateLanguage') as $vlanguage){
            if(config('app.locale') != $vlanguage){
                $validator->getTranslator()->setLocale($vlanguage);
            }
            if($validator->fails()){
                $this->cust_err_field_params[$vlanguage] = $validator->errors();
            }
        }
       
        if(!empty($this->cust_err_field_params)){
            $this->err_field_params = $this->cust_err_field_params;      
            throw new CustomValidationException($method_name, $this->class_code.$method_code.'200');
        } 
    }

    public function reset_password($data){
        $method_code = '009';
        $method_name = "reset-password";

        $this->validate_reset_password($data);

        $info = Merchant::where('email', $data['email'])->where('forgot_password_vcode', $data['code'])->first();

        if(empty($info)){
            $this->err_query_info['message'] = 'Invalid Code';
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'001');
        }

        try{
            Merchant::where('email', $data['email'])->where('forgot_password_vcode', $data['code'])->update(['password' => bcrypt($data['password'])]);
        }catch(QueryException $e){
            $this->err_query_info['code'] = $e->getCode();
            $this->err_query_info['message'] = $e->getMessage();
            $this->err_query_info['params'] = $e->getBindings();
            throw new CustomGeneralException($method_name, $this->class_code.$method_code.'002');
        }
    }
}
