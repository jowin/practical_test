<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\WebService;
use App\Admin;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use App\System;
use App\Role;
use Lang;

class AdminRoleController extends Controller
{
    public function index(Request $request)
    {
        $ws = new WebService();
        $role = new Role();

        $quick_search_fields = array(
            'name' => array('search_type' => 'like'),
        );
        $adv_search_fields = array('name');
        $sort_type = '';
        $sort_fields = array();
        $sort_field = '';

        foreach(array('admin_uid', 'qsearch', 'searchType', 'sortField', 'sortType') as $field){
            $data[$field] = $request->$field;
        }

        if($data['searchType'] == System::$searchType['quick']){
            $searchType = System::$searchType['quick'];
        }elseif($data['searchType'] == System::$searchType['advance']){
            $searchType = System::$searchType['advance'];
        }else{
            $searchType = '';
        }

        if(!empty($data['sortField'])){
            $sort_field = $data['sortField'];
            $sort_type = $data['sortType'];
        }

        $filterData = System::process_search_input($request, $searchType, $quick_search_fields, $adv_search_fields, $sort_fields, $sort_field, $sort_type); 
        $data = array_merge($data, $filterData);
       
        try{
            $role->validate_get_role_info($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            
            switch ($err_code) {   
                case '6500009001':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $role->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }
       
        try{
            $result = $role->get_role_info($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }
        return response()->json($result);
    }

    public function create(Request $request){
        $ws = new WebService();        
        $role = new Role();

        foreach(array("name", "permission_id") as $field){
            $data[$field] = $request->$field;
        }
        
        try{
            $role->validate_add_role($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '6500001200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $role->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        try{
            $pid = $role->add_role($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        try{
            $info = $role->get_role_detail($pid);
        }catch(CustomValidationException $e){   
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $info)); 

        return $result;
    }

    public function detail(Request $request){
        
        $ws = new WebService();
        $role = new Role();

        $result = '';

        foreach(array("id") as $field){
            $data[$field] = $request->$field;
        }

        try{
            $role->validate_get_role($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '6500004001':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $role->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        try{
            $info = $role->get_role_detail($data['id']);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                   
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $info)); 

        return $result;
    }

    public function update(Request $request){
        
        $ws = new WebService();
        $role = new Role();

        $result = '';

        foreach(array("name", "permission_id", "id") as $field){
            $data[$field] = $request->$field;
        }

        try{
            $info = $role->get_role_detail($data['id']);
        }catch(CustomValidationException $e){   
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        try{
            $role->validate_update_role($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '6500006200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $role->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }
       
        try{
            $role->update_role($data);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                   
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $info));

        return $result;
    }

    public function delete(Request $request)
    {
        $ws = new WebService();
        $role = new Role();

        $result = '';

        foreach(array("id") as $field){
            $data[$field] = $request->$field;
        } 

        try{
            $info = $role->validate_get_role($data);
        }catch(CustomValidationException $e){   
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        try{
            $role->delete_role($data);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                   
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }
        
        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $data)); 

        return $result;
    }

    public function get_permission_list(Request $request)
    {
        $role = new Role();
        $ws = new WebService();

        try{
            $info = $role->get_permission_list();
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                   
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $role->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $info)); 

        return $result;
    }
}

?>