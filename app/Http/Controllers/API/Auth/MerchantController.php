<?php 

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use Illuminate\Support\Facades\Auth;
use App\WebService;
use App\Merchant;
use App\WebServiceUser;
use Lang;
use Lcobucci\JWT\Parser;

class MerchantController extends Controller
{
    public function register(Request $request) { 
		$ws = new WebService();
		$ws_user = new WebServiceUser();
        $merchant = new Merchant();
		
		$form_fields = array("name", "password", "email", "mobile", "ic", "bank_name", "bank_account_num");

        foreach($form_fields as $field){
            $data[$field] = $request->$field;
        }

        try{
            $merchant->validate_add_merchant($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '3000001200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data, $field_error = $merchant->err_field_params);
                    return response()->json($result);  
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array("dev_msg" => "Error at validate add merchant."), $field_error = array());
                	return response()->json($result);  
                    break;
            }   
        }

        try{
            $mid = $merchant->add_merchant($data);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $merchant->err_field_params['message']))), $field_error = array());
                    break;
            }    
            return response()->json($result);  
        }
        
        try{
            $merchantInfo = $merchant->get_merchant_detail($uid='',$id = $mid);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $merchant->err_field_params['message']))), $field_error = array());
                    break;
            }    
            return response()->json($result);  
        }

        try{
        	$wsdata = array("uid" => $merchantInfo['uid']);
            $ws_user->add_ws_user($wsdata);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $merchant->err_field_params['message']))), $field_error = array());
                    break;
            }    
            return response()->json($result);  
        }

		$adata = array();
		
		$adata['success'] = array(
			"profile" => $merchantInfo,
		);
		
		$result = $ws->api_result($ws::$api_code_to_status['success'], $adata);
		
		return response()->json($result);        
    }
    
    public function login(Request $request){
		$merchant = new Merchant();
		$ws = new WebService();

		$result = [];

		$form_fields = array("email", "password");

        foreach($form_fields as $field){
            $data[$field] = $request->$field;
		}
		
		try{
			$merchant->validate_login($data);
		}catch(CustomValidationException $e){
			$err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '3000004200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $merchant->err_field_params);
                    return response()->json($result);  
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array("dev_msg" => "Error at merchant login."), $field_error = array());
                	return response()->json($result);  
                    break;
            }   
		}

		$result = $merchant->login($data);

		if(empty($result)){
			$status = $ws::$api_code_to_status['internal-server-error'];
			$data = array(
				"dev_status" => $ws::$api_error_detail_to_code['internal-server-error']['user-not-found'],
				"general_msg" => Lang::get('trans.api/login/error'),
				"success" => "",
				"dev_msg" => "User Not Found.",
			); 
			$result = $ws->api_result($status, $data);
            return response()->json($result); 
		}

		return response()->json($result);
    }
    
    public function logout(Request $request){
		$ws = new WebService();
        Auth::guard('merchant')->logout();  
		$data = array();		
		$data['success'] = Lang::get('trans.api/logout/success/message');
		$result = $ws->api_result($status = $ws::$api_code_to_status['success'], $data);
        
		return response()->json($result);
    }

    /*
    public function forgot_password(Request $request){
        $merchant = new Merchant();
        $ws = new WebService();
        $form_fields = array("email");

        foreach($form_fields as $field){
            $data[$field] = $request->$field;
		}

        try{
            $merchant->validate_forgot_password($data);
        }catch(CustomValidationException $e){
			$err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '3000006200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data, $field_error = $merchant->err_field_params);
                    return response()->json($result);  
                    break;               
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array("dev_msg" => "Error at merchant forgot password."), $field_error = array());
                	return response()->json($result);  
                    break;
            }   
        }
        
        try{
            $result = $merchant->forgot_password($data);
            return response()->json($result);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $merchant->err_field_params['message']))), $field_error = array());
                    break;
            }    
            return response()->json($result); 
        }
    }
    */
}