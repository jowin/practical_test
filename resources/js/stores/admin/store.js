import Vue from 'vue'
import Vuex from 'vuex'
import Config from '@/js/config/app.js';
import Router from '@/js/routes';
import langEnglish from '@/js/lang/en.js';
import langChinese from '@/js/lang/zh-CN.js';
import validateLangEnglish from '@/js/lang/validation-en.js';
import validateLangChinese from '@/js/lang/validation-zh-CN.js';
import AdminModules from '@/js/stores/modules/admin/admin-modules.js';
import LoginModules from '@/js/stores/modules/admin/login.js';
import ProfileModules from '@/js/stores/modules/admin/profile.js';
import LogoutModule from '@/js/stores/modules/admin/logout.js';
import PermissionModule from '@/js/stores/modules/admin/admin-permissions.js';
import RoleModule from '@/js/stores/modules/admin/admin-role.js';
import UserModule from '@/js/stores/modules/admin/user-modules.js';

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        AdminModules: AdminModules,
        LoginModules: LoginModules,
        ProfileModules: ProfileModules,
        LogoutModule: LogoutModule,
        PermissionModule: PermissionModule,
        RoleModule: RoleModule,
        UserModule: UserModule,
    }, 
    state: {
        result: {
           status: null,
           message: null,
        },
        authInfo: {
            token: null,
            expireDate: null,
            isAuthenticated: null,
            status: null,
            message: null,
        },
        adminInfo: { 
            id: null,
            uid: null,
            name: null,
            mobile: null,
            master: null,
            dev: null,
            privileges: null,
            email: null,
            status: null,
        },
        /*
        field_error: {},
        profile_password_field_error: {},
        updateInfoSignal: {
            updateProfileSignal: '',
            updatePasswordSignal: '',
        },
        */
		adminCurrentPrivileges: "",
        noPrivileges: true,
        privilegesList: '',
        permission: '',
    },
    mutations: {
        saveAuthInfo(state, adminData){
            localStorage.setItem('loginToken', adminData.loginToken); 
            localStorage.setItem('loginTokenExpireDate', adminData.loginTokenExpireDate); 
            //localStorage.setItem('adminInfo', JSON.stringify(adminData.adminInfo));

            state.authInfo.token = adminData.loginToken;
            state.authInfo.expireDate = adminData.loginTokenExpireDate;
            state.authInfo.status = Config.generalSuccessStatus;
            state.authInfo.message = 'success';
            state.authInfo.isAuthenticated = true;
            /*
            state.adminInfo.id = adminData.adminInfo.id;
            state.adminInfo.uid = adminData.adminInfo.uid;
            state.adminInfo.name = adminData.adminInfo.name;
            state.adminInfo.mobile = adminData.adminInfo.mobile;
            state.adminInfo.master = adminData.adminInfo.master;
            state.adminInfo.dev = adminData.adminInfo.dev;
            state.adminInfo.privileges = adminData.adminInfo.privileges;
            state.adminInfo.status = adminData.adminInfo.status;   
            state.adminInfo.email = adminData.adminInfo.email;
            */   
        },
        /*saveAdminInfo(state, adminData){
            if(adminData.saveLocal){   
                localStorage.setItem('adminInfo', JSON.stringify(adminData.adminInfo));
            }

            state.adminInfo.id = (adminData.adminInfo.id != undefined)? adminData.adminInfo.id : state.adminInfo.id;
            state.adminInfo.uid = (adminData.adminInfo.uid != undefined)? adminData.adminInfo.uid : state.adminInfo.uid;
            state.adminInfo.name = (adminData.adminInfo.name != undefined)? adminData.adminInfo.name : state.adminInfo.name;
            state.adminInfo.mobile = (adminData.adminInfo.mobile != undefined)? adminData.adminInfo.mobile : state.adminInfo.mobile;
            state.adminInfo.master = (adminData.adminInfo.master != undefined)? adminData.adminInfo.master : state.adminInfo.master;
            state.adminInfo.dev = (adminData.adminInfo.dev != undefined)? adminData.adminInfo.dev : state.adminInfo.dev;
            state.adminInfo.privileges = (adminData.adminInfo.privileges != undefined)? adminData.adminInfo.privileges : state.adminInfo.privileges;
            state.adminInfo.status = (adminData.adminInfo.status != undefined)? adminData.adminInfo.status : state.adminInfo.status;   
            state.adminInfo.email = (adminData.adminInfo.email != undefined)? adminData.adminInfo.email : state.adminInfo.email;
        },*/
        saveAdminInfo(state, adminData){
            localStorage.setItem('adminInfo', JSON.stringify(adminData.adminInfo));
            state.adminInfo.id = adminData.adminInfo.id;
            state.adminInfo.uid = adminData.adminInfo.uid;
            state.adminInfo.name = adminData.adminInfo.name;
            state.adminInfo.mobile = adminData.adminInfo.mobile;
            state.adminInfo.master = adminData.adminInfo.master;
            state.adminInfo.dev = adminData.adminInfo.dev;
            state.adminInfo.privileges = adminData.adminInfo.privileges;
            state.adminInfo.status = adminData.adminInfo.status;   
            state.adminInfo.email = adminData.adminInfo.email;
        },
        /*
        errorAuthInfo(state, validateLanguage){
            state.authInfo.token = null;
            state.authInfo.expireDate = null;
            state.authInfo.status = Config.generalErrorStatus;
            state.authInfo.message = validateLanguage['auth/failed']; 
            state.authInfo.isAuthenticated = false;
        },
        clearAuthInfo(state){
            state.authInfo.token = null;
            state.authInfo.expireDate = null; 
            state.authInfo.isAuthenticated = false;
            state.authInfo.status = null;
            state.authInfo.message = null;
            state.authInfo.aid = null;
        },
        field_error_info(state, fieldInfo){
            state.field_error = fieldInfo;
        },
        profile_password_field_error_info(state, fieldInfo){
            state.profile_password_field_error = fieldInfo;
        },
        updateInfoSignal(state, signalInfo){
            state.updateInfoSignal.updateProfileSignal = signalInfo.updateProfileSignal;
            state.updateInfoSignal.updatePasswordSignal = signalInfo.updatePasswordSignal;
        },*/
		saveCurrentPrivileges(state, info){
			state.adminCurrentPrivileges = info;
		},
        updatePrivileges(state, value){
            state.noPrivileges = value;
        },
        savePrivilegesList(state, value){
            state.privilegesList = value;
        },
        saveAdminPermission(state, value){
            state.permission = value;
        },
    }, 
    actions: {
        tryAutoLogin ({commit}) {
            const token = localStorage.getItem('loginToken');
            if (!token) {
                return;
            }
            const expirationDate = localStorage.getItem('loginTokenExpireDate');
            const now = new Date()
            if (now >= expirationDate) {
                return;
            }
            let aInfo = localStorage.getItem('adminInfo');
            aInfo = JSON.parse(aInfo);
            
            commit('saveAuthInfo', {
                loginToken: token,
                loginTokenExpireDate: expirationDate,
            })
           
            commit('saveAdminInfo', {adminInfo: aInfo, saveLocale: false});
           
            if (Router.currentRoute.name == 'admin.login' || Router.currentRoute.name == 'admin' || !Router.currentRoute.name){
                Router.replace({name: 'admin-index'});
            } 
        },
        swtichLanguage({commit, dispatch}, ldata){
            if(localStorage.getItem('language') != ldata.choosed){
                localStorage.setItem('language', Config.locale[ldata.choosed]);
                ldata.app.$root.langValue = Config.locale[ldata.choosed]
                if(ldata.choosed == Config.locale_name['en']){
                    ldata.app.$root.language = langEnglish;
                    ldata.app.$root.validateLanguage = validateLangEnglish;
                }else{
                    ldata.app.$root.language = langChinese;
                    ldata.app.$root.validateLanguage = validateLangChinese;
                }
            }
        },
        adminPrivileges({commit, dispatch}){
            const data = {
                admin_uid: this.getters.adminInfo.uid,
				wsid: Config.getWSID(),
            };
			
			axios.post(Config.base_url() + 'admin/get-admin-privileges', data)
                .then(res => {
                if( (res['status'] == 200)){
                    const privilegesInfo = res['data'];
					commit("saveCurrentPrivileges", privilegesInfo);
            	}
            })
            .catch(error => console.log(error));
        },
        getAdminPermission({commit, dispatch}){
            const data = {
                admin_uid: this.getters.adminInfo.uid,
				wsid: Config.getWSID(),
            };
			axios.post(Config.base_url() + 'admin/get-admin-permission', data)
                .then(res => {
                if( (res['status'] == 200)){
                    const permission = res['data'];
					commit("saveAdminPermission", permission);
            	}
            })
            .catch(error => console.log(error));
        },
        getPrivliegesList({commit, dispatch}){
            const data = {
				wsid: Config.getWSID(),
            };
			axios.post(Config.base_url() + 'admin/get-privileges-list', data)
                .then(res => {
                if( (res['status'] == 200)){
                    const list = res['data'];
					commit("savePrivilegesList", list);
            	}
            })
            .catch(error => console.log(error));
        },
        checkPermission({state, commit, dispatch}, routeName){
            // let admin_info = localStorage.getItem('adminInfo');
            // admin_info = JSON.parse(admin_info);
            // let admin_uid = admin_info.uid;

            // const data = {
            //     admin_uid: admin_uid,
            //     route: routeName,
            // };

            // axios.post(Config.base_url() + 'admin/check-permission', data)
            //     .then(res => {
            //     if( (res['status'] == 200)){
            //         const result = res['data'];
			// 		console.log(result);
            // 	}
            // })
            // .catch(error => console.log(error));
            //console.log( this.$store.getters.getAdminCurrentPrivileges );
        },
        /*
        logout({commit}){
            commit('clearAuthInfo');
            localStorage.removeItem('loginTokenExpireDate');
            localStorage.removeItem('loginToken');
            localStorage.removeItem('adminInfo');
            Router.replace({name: 'admin.login'});
        },
        login({state, commit, rootState}, authData){      
            axios.post(Config.base_url() + 'admin/login', authData)
                .then(res => {
            	if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    const loginToken = res['data']['data']['auth']['api_token'];
                    const loginTokenExpireDate = res['data']['data']['auth']['api_token_expire_date'];
                    const aInfo = res['data']['data']['profile'];            
                    commit('saveAuthInfo', {loginToken: loginToken, loginTokenExpireDate: loginTokenExpireDate});
                    commit('saveAdminInfo', {adminInfo: aInfo, saveLocal: true});
                    Router.replace({name: "admin-index"});
                    Router.go({name: "admin-index"});
            	} else {
                    commit('errorAuthInfo', authData.vlanguage);
                }
                
            })
            .catch(error => console.log(error));
        },
        updateProfile({commit, dispatch}, aInfo){
            axios.post(Config.base_url() + 'admin/update-profile', aInfo)
                .then(res => {
            	if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    const aInfo = res['data']['data']['profile'];
                    commit('saveAdminInfo', {adminInfo: aInfo, saveLocal: true});                    
                    commit('updateInfoSignal', {updateProfileSignal: Config.generalStatus['success']});
            	} else {
                    commit('updateInfoSignal', {updateProfileSignal: Config.generalStatus['error']});
                    commit('field_error_info', res['data']['field_error']);
                }
            })
            .catch(error => console.log(error));
        },
        updatePassword({commit, dispatch}, pInfo){
            axios.post(Config.base_url() + 'admin/update-password', pInfo)
                .then(res => {
                    console.log(res);
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    const aInfo = res['data']['data']['profile'];                    
                    commit('saveAdminInfo', {adminInfo: aInfo, saveLocal: true});
                    commit('updateInfoSignal', {updatePasswordSignal: Config.generalStatus['success']});
            	} else {
                    commit('updateInfoSignal', {updatePasswordSignal: Config.generalStatus['error']});
                    commit('profile_password_field_error_info', res['data']['field_error']);
                }
            })
            .catch(error => console.log(error));
        },
        checkPrivileges({commit, dispatch}){
            const privileges = this.$store.getters.getAdminCurrentPrivileges;
            console.log("privileges", privileges);
            if(privileges != ""){
                if(!this.$route.meta.excludePrivileges){
                    let currentRouteName = this.$route.name;
                    if(Object.values(privileges.detail).indexOf(currentRouteName)){
                        commit('updatePrivileges', false);
                    }
                }
            }
        },
        */
    },
    getters: {
        authResult(state){
            return state.authInfo;
        },
        isAuthenticated(state){
            return state.authInfo.isAuthenticated;
        },
        adminInfo(state){
            return state.adminInfo;
        },
        privilegesList(state){
            return state.privilegesList;
        },
        permission(state){
            return state.permission;
        },
        /*getFieldErrorInfo(state){
            return state.field_error;
        },
        getUpdateSign(state){
            return state.updateInfoSignal;
        },
        getProfilePasswordFieldErrorInfo(state){
            return state.profile_password_field_error;
        },
		getAdminCurrentPrivileges(state){
			return state.adminCurrentPrivileges;
		},
        noPrivileges(state){
            return "abc";
        },
        */
    },
});

