<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use App\WebService;
use Lang;
use Config;

class ApiUrl
{
	public function __construct(Application $app, Redirector $redirector, Request $request) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
    }
	
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		// $language = $request->segment(2)
		$api_version = $request->segment(2);
		$user_path = $request->segment(3);
		
		$ws = new WebService();	
		
		if(in_array($api_version, $this->app->config->get('app.locales'))){
			$locale = $request->segment(2);
			$api_version = $request->segment(3);
			$user_path = $request->segment(4);
			$this->app->setLocale($locale);
		}
		
		if((!in_array($api_version, $this->app->config->get('app.api_versions'))) || ($api_version != $this->app->config->get('app.api_latest_version')) ){
			
			$data = array(
				"dev_status" => $ws::$api_error_detail_to_code['internal-server-error']['invalid-api-version'],
				"dev_msg" => "Invalid API Version",
				"general_msg" => Lang::get(WebService::$api_error_message['internal-server-error']['invalid-api-version']),
				"success" => "",
			);
			
			$status = $ws::$api_code_to_status['internal-server-error'];
			
			$result = $ws->api_result($status, $data);
			
			return response()->json($result);
		}else{
			$request->attributes->add(['api_version' => $api_version]);
		}
		
		if($user_path == Config::get('app.api_user_path')){
			# check whether has user login
		}
		
        return $next($request);
    }
}
