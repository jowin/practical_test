<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use App\Admin;

class AdminLoginController extends Controller
{
	public function __construct()
	{
		$this->middleware('guest:admin', ['except' => 'logout']);
	}

    public function showLoginForm()
    {
    	return view('admin.login');
    }

    public function login(Request $request)
    {
    	// Validate the form data
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        $not_match = false;

    	// Attemp to log the user in
    	if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
			$admin_currenct_status = Auth::guard('admin')->user()->status;
            $admin_status_list = Admin::$status_to_code;
			unset($admin_status_list['active']);
            if(in_array(Auth::guard('admin')->user()->status, $admin_status_list)){
                $this->logout();
                return redirect()->back()->with(["login_err_msg" => trans('trans.admin/login/error/account/message', ['status' => strtolower(trans(Admin::$code_to_text[$admin_currenct_status]))])]);
            }
			
			// if successfull, then redirect to their intended location
    		return redirect()->intended(route('admin.dashboard'));
    	}else{
            return $this->sendFailedLoginResponse($request);
        }
        
    	// if unsuccessfuill, then redierect to the login with form data
        return redirect()->back()->withErrors($validator)->withInput();
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
    	Auth::guard('admin')->logout();                

        return redirect('/');
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }
}
