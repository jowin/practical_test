<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Mail\ExceptionOccured;
use App\Mail\ForgotPassword;
use Mail;
use Config;
use App\ErrorLog;
use Illuminate\Http\Request;

class InternalMail extends Model
{
    public function send($data = array())
    {
    	if(!empty($data)){
    		try{
    			$developer_email = Config::get('app.developer_email');
    			foreach($developer_email as $email){
                    Mail::to($email)->send(new ExceptionOccured($data));
                } 
    		}catch(Exception $e){
    			$el = new ErrorLog;
    			$data['message'] = $e->getMessage();
    			$data['trace'] = $e->getTrace();
    			try{
    				$el->add_err_log($data);
    			}catch(Exception $ex){
    				dd($ex);
    			}
    		}	
    	}
	}
	
	public function sendForgotPassword($data){
		if(!empty($data) && isset($data['email'])){
    		try{
    			Mail::to($data["email"])->send(new ForgotPassword($data)); 
    		}catch(Exception $e){
    			$el = new ErrorLog;
    			$data['message'] = $e->getMessage();
    			$data['trace'] = $e->getTrace();
    			try{
    				$el->add_err_log($data);
    			}catch(Exception $ex){
    				dd($ex);
    			}
    		}	
    	}
	}
}
