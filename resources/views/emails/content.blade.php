@extends('emails/component/header')

@section('email_body')
<tr>
	<td>
		{!! $content['message'] !!}
	</td>
</tr>
@endsection