<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Merchant;
use App\WebService;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use App\System;
use App\User;
use Lang;

class GeneralAuth extends Controller
{
    public function forgot_password(Request $request){
        $merchant = new Merchant();
        $ws = new WebService();
        $form_fields = array("email");

        foreach($form_fields as $field){
            $data[$field] = $request->$field;
		}

        $sys = new System();
        $ws = new WebService();
        $userRole = $sys->findRoleUser($data['email']);

        if(empty($userRole)){
            $field_error = array();
            $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => "User Not Found"), $field_error = array());    
            return response()->json($result); 
        }
        
        $user = ($userRole == "merchant")? new Merchant() : new User();

        try{
            $user->validate_forgot_password($data);
        }catch(CustomValidationException $e){
			$err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '3000006200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data, $field_error = $user->err_field_params);
                    return response()->json($result);  
                    break;   
                case '4000006200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data, $field_error = $user->err_field_params);
                    return response()->json($result);  
                    break;               
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array("dev_msg" => "Error at forgot password."), $field_error = array());
                	return response()->json($result);  
                    break;
            }   
        }
        
        try{
            $result = $user->forgot_password($data);
            return response()->json($result);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_query_info['message']))), $field_error = array());
                    break;
            }    
            return response()->json($result); 
        }
    }

    public function reset_password(Request $request){
        $form_fields = ["email", "code", "password", 'confirm_password'];

        foreach($form_fields as $field){
            $data[$field] = $request->$field;
        }

        $sys = new System();
        $ws = new WebService();
        $userRole = $sys->findRoleUser($data['email']);
        
        if(empty($userRole)){
            $field_error = array();
            $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => "User Not Found"), $field_error = array());    
            return response()->json($result); 
        }
        
        $user = ($userRole == "merchant")? new Merchant() : new User();
        
        try{
            $user->validate_reset_password($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                case '3000008200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data, $field_error = $user->err_field_params);
                    return response()->json($result);  
                    break;  
                case '4000008200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data, $field_error = $user->err_field_params);
                    return response()->json($result);  
                    break;               
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array("dev_msg" => "Error at validate reset password."), $field_error = array());
                	return response()->json($result);  
                    break;
            }
        }

        try{
            $user->reset_password($data);
            $result = $ws->api_result($status = $ws::$api_code_to_status['success']);        
        return response()->json($result);
            return response()->json($result);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_query_info['message']))), $field_error = array());
                    break;
            }    
            return response()->json($result); 
        }
    }
}
