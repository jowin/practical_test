<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute是被接受的',
    'active_url'           => ':attribute必须是一个合法的 URL',
    'after'                => ':attribute必须是 :date 之后的一个日期',
    'after_or_equal'       => ':attribute 必须过后或相同 :date.',
    'alpha'                => ':attribute必须全部由字母字符构成。',
    'alpha_dash'           => ':attribute必须全部由字母、数字、中划线或下划线字符构成',
    'alpha_num'            => ':attribute必须全部由字母和数字构成',
    'array'                => ':attribute必须是个数组',
    'before'               => ':attribute 必须是 :date 之前的一个日期',
    'before_or_equal'      => ':attribute 必须之前或相同 :date.',
    
    'between'              => [
        'numeric'   => ':attribute必须在 :min 到 :max之间',
        'file'      =>  ':attribute必须在 :min 到 :max KB之间',
        'string'    => ':attribute必须在 :min 到 :max 个字符之间',
        'array'     => ':attribute必须在 :min 到 :max 项之间',
    ],
 
    'boolean'              => ':attribute字符必须是 true 或 false',
    'confirmed'            => ':attribute二次确认不匹配',
    'date'                 => ':attribute必须是一个合法的日期',
    'date_format'          => ':attribute与给定的格式 :format 不符合',
    'different'            => ':attribute必须不同于:other',
    'digits'               => ':attribute必须是 :digits 位',
    'digits_between'       => ':attribute必须在 :min and :max 位之间',
    'dimensions'           => ':attribute图像尺寸不合法',
    'distinct'             => ':attribute字段值不能重复.',
    'email'                => ':attribute必须是一个合法的电子邮件地址。',
    'exists'               => '选定的:attribute 是无效的.',
    'file'                 => ':attribute必须是文件',
    'filled'               => ':attribute的字段是必填的',

    'gt'                   => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file'    => 'The :attribute must be greater than :value kilobytes.',
        'string'  => 'The :attribute must be greater than :value characters.',
        'array'   => 'The :attribute must have more than :value items.',
    ],
    'gte'                  => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file'    => 'The :attribute must be greater than or equal :value kilobytes.',
        'string'  => 'The :attribute must be greater than or equal :value characters.',
        'array'   => 'The :attribute must have :value items or more.',
    ],

    'image'                => ':attribute必须是一个图片 (jpeg, png, bmp 或者 gif)',
    'in'                   => '选定的:attribute是无效的',
    'in_array'             => ':attribute不在:other中',
    'integer'              => ':attribute必须是个整数',
    'ip'                   => ':attribute必须是一个合法的IP地址。',
    'ipv4'                 => ':attribute必须是一个合法的IPv4地址。',
    'ipv6'                 => ':attribute必须是一个合法的IPv6地址。',
    'json'                 => ':attribute必须是一个合法的JSON字符串。',
    'lt'                   => [
        'numeric' => 'The :attribute must be less than :value.',
        'file'    => 'The :attribute must be less than :value kilobytes.',
        'string'  => 'The :attribute must be less than :value characters.',
        'array'   => 'The :attribute must have less than :value items.',
    ],
    'lte'                  => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file'    => 'The :attribute must be less than or equal :value kilobytes.',
        'string'  => 'The :attribute must be less than or equal :value characters.',
        'array'   => 'The :attribute must not have more than :value items.',
    ],

    'max'                  => [
        'numeric' => ':attribute的最大长度为:max位',
        'file'    => ':attribute的最大为:max',
        'string'  => ':attribute的最大长度为:max字符',
        'array'   => ':attribute的最大个数为:max个',
    ],

    'mimes'                => ':attribute的文件类型必须是:values',
    'mimetypes'            => ':attribute的文件类型必须是:values',

    'min'                  => [
        'numeric' => ':attribute的最小长度为:min位',
        'file'    => ':attribute大小至少为:min KB',
        'string'  => ':attribute的最小长度为:min字符',
        'array'   => ':attribute 至少有:min项',
    ],
    'not_in'               => '选定的:attribute 是无效的',
    'not_regex'            => 'The :attribute format is invalid.',
    'numeric'              => ':attribute必须是数字',
    'present'              => ':attribute字段必须存在',
    'regex'                => ':attribute格式是无效的',
    'required'             => ':attribute字段是必须的',
    'required_if'          => ':attribute字段是必须的当:other是:value',
    'required_unless'      => ':attribute字段是必须的除非:other在:values中',
    'required_with'        => ':attribute字段是必须的当:values是存在的',
   
    'required_with_all'    => ':attribute 字段是必须的当 :values 是存在的',
    'required_without'     => ':attribute 字段是必须的当 :values 是不存在的.',
    'required_without_all' => ':attribute 字段是必须的当 没有一个 :values 是存在的',
    'same'                 => ':attribute和:other必须匹配',
    
    'size'                 => [
        'numeric' => ':attribute必须是:size位',
        'file'    => 'attribute必须是:sizeKB',
        'string'  => 'attribute必须是:size个字符',
        'array'   => 'attribute必须包括:size项',
    ],
    'string'               => ':attribute必须是字符串',
    'timezone'             => ':attribute必须是个有效的时区',
    'unique'               => ':attribute已存在',
    'uploaded'             => ':attribute上传失败',
    'url'                  => ':attribute无效的格式',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],

        // 'detail' => [
        //     'required' => '详述字段是必须的',
        //     'array' => '详述必须是个数组',
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'username' => '用户名',
        // 'account'  => '账号',
        // 'captcha'  => '验证码',
        'mobile'   => '电话',
        // 'content'  => '内容',
        // 'identity' => '手机号/用户名',
        'password' => "密码",
        'email' => "邮件",
        'first_name' => "名字",
        'last_name' => "姓",
        'ic' => "身份证",
        'address' => "地址",
        'bank_name' => "银行名称",
        'bank_acc_no' => "银行户口号码",
        'country' => "国家", 
        'name' => "名字", 
        'detail' => "详述",
        'privileges' => "权限",
        'gender' => "性别",
        'bank' => "银行",
        'message' => "信息",
        'game' => "产品",
        'confirm_password' => "重复密码",
    ],    

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Service Provider
    |--------------------------------------------------------------------------
    |
    | The following language is based on Service Provider File
    |
    */
    'valid_privilege_options' => "请选择正当的:attribute",
    'valid_privileges' => "请选择正当的:attribute",
    'valid_country' => "请选择正当的:attribute",
    'valid_gender' => "请选择正当的:attribute",
    'valid_bank' => "请选择正当的:attribute",
    'valid_data' => "请选择正当的:attribute",
    'valid_admin' => "请选择正当的:attribute",
    'valid_email' => ":attribute不在系统里",
    'info_used' => ':attribute已存在',
];