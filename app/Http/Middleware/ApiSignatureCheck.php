<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Config;
use App\WebService;

class ApiSignatureCheck
{
	protected $check_signature;
	
	public function __construct(Application $app, Redirector $redirector, Request $request) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
		$this->check_signature = Config::get('app.check_signature');
    }
	
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$ws = new WebService();
		$input_signature = $this->request->signature;
		$link = $this->request->fullUrl();
		if($this->check_signature){
			switch($link){
				case url('/api/'.$this->request->get('api_version').'/test-form-error'):
					$data = $this->request->fname.$this->request->lname.$this->request->phone.$this->request->wsid;
					$original_signature = $ws->encrypt('', '', $data);
					$seperate_signature = explode(":", $original_signature);
					if($input_signature != $seperate_signature[0]){
						$result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['invalid-signature']);
						return response()->json($result);
					}
					break;
				default:
					$result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['invalid-signature']);
					return response()->json($result);
					break;
			}			
		}
		
        return $next($request);
    }
}
