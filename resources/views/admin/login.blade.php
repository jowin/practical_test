<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title id="title"></title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/admin/bootstrap.css') }}"></link>
        <link rel="stylesheet" href="{{ asset('css/admin/metisMenu.css') }}"></link>
        <link rel="stylesheet" href="{{ asset('css/admin/index.css') }}"></link>
        <link rel="stylesheet" href="{{ asset('css/admin/jquery.mCustomScrollbar.css') }}"></link>
        <link rel="stylesheet" href="{{ asset('css/admin/line-awesome.min.css') }}"></link>
        <link rel="stylesheet" href="{{ asset('css/admin/dripicons.min.css') }}"></link>
        <link rel="stylesheet" href="{{ asset('css/admin/material-design-iconic-font.min.css') }}"></link>
        <link rel="stylesheet" href="{{ asset('css/admin/main.bundle.css') }}"></link>
        <link rel="stylesheet" href="{{ asset('css/admin/main.css') }}"></link>
        <link rel="stylesheet" href="{{ asset('css/admin/default.css') }}"></link>
        <link rel="stylesheet" href="{{ asset('css/admin/theme-j.css') }}"></link>
        <link rel="icon" href="http://www.authenticgoods.co/themes/quantum-pro/demos/assets/img/qt-logo@2x.png" type="image/gif" sizes="16x16">
        <!-- <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet"> -->
        <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-datepicker.min.css') }}"></link>
        <link rel="stylesheet" href="{{ asset('css/admin/daterangepicker.css') }}"></link>
    </head>

    <body>
        <div id="adminApp"></div>
        <script src="{{ (config('app.env') == 'dev')? mix('js/app.js') : asset('js/app.js') }}"></script>
        <script src="{{ asset('js/admin/modernizr.custom.js') }}"></script>
        <script src="{{ asset('js/admin/jquery.min.js') }}"></script>
        <script src="{{ asset('js/admin/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('js/admin/js.storage.js') }}"></script>
        <script src="{{ asset('js/admin/js.cookie.js') }}"></script>
        <script src="{{ asset('js/admin/pace.js') }}"></script>
        <script src="{{ asset('js/admin/metisMenu.js') }}"></script>
        <script src="{{ asset('js/admin/index.js') }}"></script>
        <script src="{{ asset('js/admin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
        <script src="{{ asset('js/admin/app.js') }}"></script>
        <script src="{{ asset('js/admin/moment.min.js') }}"></script>
        <script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>    
        <script src="{{ asset('js/admin/daterangepicker.js') }}"></script>    
        <script src="{{ asset('js/admin/bootstrap-datepicker-init.js') }}"></script>    
        <script src="{{ asset('js/admin/bootstrap-date-range-picker-init.js') }}"></script>  
    </body>
</html>