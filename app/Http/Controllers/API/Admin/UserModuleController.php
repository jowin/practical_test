<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\WebService;
use App\Admin;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomQueryException;
use App\Exceptions\CustomGeneralException;
use App\System;
use App\User;
use Lang;

class UserModuleController extends Controller
{
    public function index(Request $request)
    {
        $ws = new WebService();
        $user = new User();

        $quick_search_fields = array(
            'name' => array('search_type' => 'like'),
        );
        $adv_search_fields = array('name', 'email');
        $sort_type = '';
        $sort_fields = array();
        $sort_field = '';

        foreach(array('admin_uid', 'qsearch', 'searchType', 'sortField', 'sortType') as $field){
            $data[$field] = $request->$field;
        }

        if($data['searchType'] == System::$searchType['quick']){
            $searchType = System::$searchType['quick'];
        }elseif($data['searchType'] == System::$searchType['advance']){
            $searchType = System::$searchType['advance'];
        }else{
            $searchType = '';
        }

        if(!empty($data['sortField'])){
            $sort_field = $data['sortField'];
            $sort_type = $data['sortType'];
        }

        $filterData = System::process_search_input($request, $searchType, $quick_search_fields, $adv_search_fields, $sort_fields, $sort_field, $sort_type); 
        $data = array_merge($data, $filterData);
       
        try{
            $user->validate_get_user_info($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            
            switch ($err_code) {   
                case '4000010001':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $user->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }
       
        try{
            $result = $user->get_user_info($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }
        return response()->json($result);
    }

    public function create(Request $request){
        $ws = new WebService();        
        $user = new User();

        foreach(array("name", "email", "password") as $field){
            $data[$field] = $request->$field;
        }
        
        try{
            $user->validate_add_member($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '4000001200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $user->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        try{
            $uid = $user->add_member($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        try{
            $info = $user->get_member_detail('', $uid);
        }catch(CustomValidationException $e){   
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $info)); 

        return $result;
    }

    public function detail(Request $request){
        
        $ws = new WebService();
        $user = new User();

        $result = '';

        foreach(array("id") as $field){
            $data[$field] = $request->$field;
        }

        try{
            $user->validate_get_user($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '4000012001':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $user->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        try{
            $info = $user->get_user_detail($data['id']);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                   
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $info)); 

        return $result;
    }

    public function update(Request $request){
        
        $ws = new WebService();
        $user = new User();

        $result = '';

        foreach(array("name", "email", "password", "id") as $field){
            $data[$field] = $request->$field;
        }

        try{
            $info = $user->get_user_detail($data['id']);
        }catch(CustomValidationException $e){   
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        try{
            $user->validate_update_user($data);
        }catch(CustomValidationException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {   
                case '4000015200':
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['field-error']['input-field-error'], $data = array(), $field_error = $user->err_field_params);
                    break;                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }
       
        try{
            $user->update_user($data);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                   
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }

        try{
            $userInfo = $user->get_user_detail($data['id']);
        }catch(CustomValidationException $e){   
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $userInfo));

        return $result;
    }

    public function delete(Request $request)
    {
        $ws = new WebService();
        $user = new User();

        $result = '';

        foreach(array("id") as $field){
            $data[$field] = $request->$field;
        } 

        try{
            $info = $user->validate_get_user($data);
        }catch(CustomValidationException $e){   
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    return response()->json($result);  
                    break;
            }   
            return response()->json($result);
        }

        try{
            $user->delete_user($data);
        }catch(CustomGeneralException $e){
            $err_code = $e->getCode();
            $field_error = array();
            switch ($err_code) {                   
                default:
                    $result = $ws->api_result($status = $ws::$api_error_detail_to_code['internal-server-error']['system-error'], $data = array('dev_msg' => Lang::get('trans.api/general/dev/message', array('error_code' => $err_code, 'method_name' => $e->getMessage(), 'error_message' => $user->err_field_params['message']))), $field_error = array());
                    break;  
            }   
            return response()->json($result);
        }
        
        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $data)); 

        return $result;
    }

    public function get_status_list(Request $request)
    {
        $ws = new WebService();
        $user = new User();

        $list = $user::$code_to_status;

        $result = $ws->api_result($ws::$api_code_to_status['success'], array("info" => $list)); 

        return $result;
    }
}

?>