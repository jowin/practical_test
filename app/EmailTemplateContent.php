<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EmailTemplate;

class EmailTemplateContent extends Model
{
    protected $table = 'email_template_content';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'eid', 'language', 'subject', 'message',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function email_template()
    {
        return $this->belongsTo('App\EmailTemplate', 'code');
    }
}
