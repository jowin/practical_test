import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/js/components/Home';
import About from '@/js/components/About';

/* Admin */
import AdminLogin from '@/js/components/admin/Login';
import AdminDashboard from '@/js/components/admin/Dashboard';
import AdminProfile from '@/js/components/admin/Profile';
import Config from '@/js/config/app';
import LanguageEnglish from '@/js/lang/en';
import LanguageChinese from '@/js/lang/zh-CN';
import AdminModules from '@/js/components/admin/admin-module/index';
import AdminModuleCreate from '@/js/components/admin/admin-module/create';
import AdminModuleDetail from '@/js/components/admin/admin-module/detail';
import AdminModuleEdit from '@/js/components/admin/admin-module/edit';

import AdminPermission from '@/js/components/admin/admin-permission/index';
import AdminPermissionDetail from '@/js/components/admin/admin-permission/detail';
import AdminPermissionCreate from '@/js/components/admin/admin-permission/create';
import AdminPermissionEdit from '@/js/components/admin/admin-permission/edit';

import AdminRole from '@/js/components/admin/admin-role/index';
import AdminRoleDetail from '@/js/components/admin/admin-role/detail';
import AdminRoleCreate from '@/js/components/admin/admin-role/create';
import AdminRoleEdit from '@/js/components/admin/admin-role/edit';

import UserModule from '@/js/components/admin/user-module/index';
import UserModuleDetail from '@/js/components/admin/user-module/detail';
import UserModuleCreate from '@/js/components/admin/user-module/create';
import UserModuleEdit from '@/js/components/admin/user-module/edit';

/* Member */
import MemberLogin from '@/js/components/member/Login';
import MemberDashboard from '@/js/components/member/Dashboard';
import MemberProfile from '@/js/components/member/Profile';

Vue.use(VueRouter);

let locale = localStorage.getItem('language');
let language = Config.locale_name[locale];
let lfiles = '';
if(language == Config.locale_name['en']){
	lfiles = LanguageEnglish;
}else {
	lfiles = LanguageChinese;
}

const router = new VueRouter({
	mode: 'history',
	routes: [
		/* Admin Route */
		{
			path: '/admin/login',
			name: 'admin.login',
			component: AdminLogin,
			meta: {
				title: lfiles['admin/login/pagetitle'],
				langKey: 'admin/login/pagetitle',
				excludePrivileges: true,
			},
		},
		{
			path: '/admin',
			name: 'admin',
			component: AdminDashboard,
			meta: {
				title: lfiles['admin/dashboard/title'],
				langKey: 'admin/dashboard/title',
				excludePrivileges: true,
			},
		},
		{
			path: '/admin/index',
			name: 'admin-index',
			component: AdminDashboard,
			meta: {
				title: lfiles['admin/dashboard/title'],
				langKey: 'admin/dashboard/title',
				excludePrivileges: true,
			},
			props: true,
		},
		{
			path: '/admin/profile',
			name: 'admin.profile',
			component: AdminProfile,
			meta: {
				title: lfiles['admin/dashboard/profile'],
				langKey: 'admin/dashboard/profile',
				excludePrivileges: true,
			},
		},
		{
			path: '/admin/admin-modules',
			name: 'admin-modules',
			component: AdminModules,
			meta: {
				title: lfiles['admin/admin-modules/pagetitle'],
				langKey: 'admin/admin-modules/pagetitle',
				excludePrivileges: false,
			},
		},
		{
			path: '/admin/admin-modules-create',
			name: 'admin-modules-insert',
			component: AdminModuleCreate,
			meta: {
				title: lfiles['admin/admin-modules-create/pagetitle'],
				langKey: 'admin/admin-modules-create/pagetitle',
				excludePrivileges: false,
			},
			
		},
		{
			path: '/admin/admin-modules-detail-:aid',
			name: 'admin-modules-view',
			component: AdminModuleDetail,
			meta: {
				title: lfiles['admin/admin-modules-detail/pagetitle'],
				langKey: 'admin/admin-modules-detail/pagetitle',
				excludePrivileges: false,
			},
			
		},
		{
			path: '/admin/admin-modules-edit-:aid',
			name: 'admin-modules-edit',
			component: AdminModuleEdit,
			meta: {
				title: lfiles['admin/admin-modules-edit/pagetitle'],
				langKey: 'admin/admin-modules-edit/pagetitle',
				excludePrivileges: false,
			},
			
		},

		/* Permission */
		{
			path: '/admin/permission',
			name: 'admin-permission',
			component: AdminPermission,
			meta: {
				title: lfiles['admin/permission/pagetitle'],
				langKey: 'admin/permission/pagetitle',
				excludePrivileges: false,
			},
		},
		{
			path: '/admin/permission-create',
			name: 'admin-permission-insert',
			component: AdminPermissionCreate,
			meta: {
				title: lfiles['admin/permission-create/pagetitle'],
				langKey: 'admin/permission-create/pagetitle',
				excludePrivileges: false,
			},
		},
		{
			path: '/admin/permission-detail-:pid',
			name: 'admin-permission-view',
			component: AdminPermissionDetail,
			meta: {
				title: lfiles['admin/permission-detail/pagetitle'],
				langKey: 'admin/permission-detail/pagetitle',
				excludePrivileges: false,
			},
			
		},
		{
			path: '/admin/permission-edit-:pid',
			name: 'admin-permission-edit',
			component: AdminPermissionEdit,
			meta: {
				title: lfiles['admin/permission-edit/pagetitle'],
				langKey: 'admin/permission-edit/pagetitle',
				excludePrivileges: false,
			},
			
		},

		/* Role */
		{
			path: '/admin/role',
			name: 'admin-role',
			component: AdminRole,
			meta: {
				title: lfiles['admin/role/pagetitle'],
				langKey: 'admin/role/pagetitle',
				excludePrivileges: false,
			},
		},
		{
			path: '/admin/role-create',
			name: 'admin-role-insert',
			component: AdminRoleCreate,
			meta: {
				title: lfiles['admin/role-create/pagetitle'],
				langKey: 'admin/role-create/pagetitle',
				excludePrivileges: false,
			},
		},
		{
			path: '/admin/role-detail-:rid',
			name: 'admin-role-view',
			component: AdminRoleDetail,
			meta: {
				title: lfiles['admin/role-detail/pagetitle'],
				langKey: 'admin/role-detail/pagetitle',
				excludePrivileges: false,
			},
			
		},
		{
			path: '/admin/role-edit-:rid',
			name: 'admin-role-edit',
			component: AdminRoleEdit,
			meta: {
				title: lfiles['admin/role-edit/pagetitle'],
				langKey: 'admin/role-edit/pagetitle',
				excludePrivileges: false,
			},
			
		},

		/* User Module */
		{
			path: '/admin/user-module',
			name: 'user-modules',
			component: UserModule,
			meta: {
				title: lfiles['admin/user-modules/pagetitle'],
				langKey: 'admin/user-modules/pagetitle',
				excludePrivileges: false,
			},
		},
		{
			path: '/admin/user-module-create',
			name: 'user-modules-insert',
			component: UserModuleCreate,
			meta: {
				title: lfiles['admin/user-modules-create/pagetitle'],
				langKey: 'admin/user-modules-create/pagetitle',
				excludePrivileges: false,
			},
		},
		{
			path: '/admin/user-module-detail-:uid',
			name: 'user-modules-view',
			component: UserModuleDetail,
			meta: {
				title: lfiles['admin/user-modules-detail/pagetitle'],
				langKey: 'admin/user-modules-detail/pagetitle',
				excludePrivileges: false,
			},
			
		},
		{
			path: '/admin/user-module-edit-:uid',
			name: 'user-modules-edit',
			component: UserModuleEdit,
			meta: {
				title: lfiles['admin/user-modules-edit/pagetitle'],
				langKey: 'admin/user-modules-edit/pagetitle',
				excludePrivileges: false,
			},
			
		},

		/* Merchant Route */
		/* Login Page */
		{
			path: '/member/login',
			name: 'member.login',
			component: MemberLogin,
			meta: {
				title: lfiles['member/login/pagetitle'],
				langKey: 'member/login/pagetitle',
				excludePrivileges: true,
			},
		},
		{
			path: '/member',
			name: 'member',
			component: MemberDashboard,
			meta: {
				title: lfiles['member/dashboard/title'],
				langKey: 'member/dashboard/title',
				excludePrivileges: true,
			},
		},
		{
			path: '/member/dashboard',
			name: 'member-dashboard',
			component: MemberDashboard,
			meta: {
				title: lfiles['member/dashboard/title'],
				langKey: 'member/dashboard/title',
				excludePrivileges: true,
			},
		},
		{
			path: '/member/member-index',
			name: 'member-index',
			component: MemberDashboard,
			meta: {
				title: lfiles['member/dashboard/title'],
				langKey: 'member/dashboard/title',
				excludePrivileges: true,
			},
		},
		{
			path: '/member/profile',
			name: 'member-profile',
			component: MemberProfile,
			meta: {
				title: lfiles['member/dashboard/profile'],
				langKey: 'member/dashboard/profile',
				excludePrivileges: true,
			},
		},
		/*
		{
			path: '/member/home',
			name: 'home',
			component: Home
		},

		{
			path: '/member/about',
			name: 'about',
			component: About
		},
		*/
		
	]
});

export default router;