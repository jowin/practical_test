import Config from '@/js/config/app.js';
import axios from 'axios';
import Router from '@/js/routes';

export default {
    namespaced: true,
    state: {
        adminInfo: { 
            id: null,
            uid: null,
            name: null,
            mobile: null,
            master: null,
            dev: null,
            privileges: null,
            email: null,
            status: null,
        },
        updateInfoSignal: {
            updateProfileSignal: '',
            updatePasswordSignal: '',
        },
        field_error: {},
        profile_password_field_error: {},
    },
    mutations: {
        saveAdminInfo(state, adminData){
            localStorage.setItem('adminInfo', JSON.stringify(adminData.adminInfo));
            state.adminInfo.id = adminData.adminInfo.id;
            state.adminInfo.uid = adminData.adminInfo.uid;
            state.adminInfo.name = adminData.adminInfo.name;
            state.adminInfo.mobile = adminData.adminInfo.mobile;
            state.adminInfo.master = adminData.adminInfo.master;
            state.adminInfo.dev = adminData.adminInfo.dev;
            state.adminInfo.privileges = adminData.adminInfo.privileges;
            state.adminInfo.status = adminData.adminInfo.status;   
            state.adminInfo.email = adminData.adminInfo.email;
        },
        field_error_info(state, fieldInfo){
            state.field_error = fieldInfo;
        },
        updateInfoSignal(state, signalInfo){
            state.updateInfoSignal.updateProfileSignal = signalInfo.updateProfileSignal;
            state.updateInfoSignal.updatePasswordSignal = signalInfo.updatePasswordSignal;
        },
        profile_password_field_error_info(state, fieldInfo){
            state.profile_password_field_error = fieldInfo;
        },
    },
    getters: {        
    },
    actions: {   
        updateProfile({state, commit, rootState}, aInfo){     
            axios.post(Config.base_url() + 'admin/update-profile', aInfo)
                .then(res => {
            	if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    const aInfo = res['data']['data']['profile'];
                    commit('saveAdminInfo', {adminInfo: aInfo, saveLocal: true});                    
                    commit('updateInfoSignal', {updateProfileSignal: Config.generalStatus['success']});
            	} else {
                    commit('updateInfoSignal', {updateProfileSignal: Config.generalStatus['error']});
                    commit('field_error_info', res['data']['field_error']);
                }
            })
            .catch(error => console.log(error));
        },
        updatePassword({state, commit, rootState}, pInfo){
            axios.post(Config.base_url() + 'admin/update-password', pInfo)
                .then(res => {
                    console.log(res);
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    const aInfo = res['data']['data']['profile'];                    
                    commit('saveAdminInfo', {adminInfo: aInfo, saveLocal: true});
                    commit('updateInfoSignal', {updatePasswordSignal: Config.generalStatus['success']});
            	} else {
                    commit('updateInfoSignal', {updatePasswordSignal: Config.generalStatus['error']});
                    commit('profile_password_field_error_info', res['data']['field_error']);
                }
            })
            .catch(error => console.log(error));
        },
    },
}