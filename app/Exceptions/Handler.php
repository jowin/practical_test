<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Mail;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use App\Mail\ExceptionOccured;
use Config;
use Illuminate\Http\Request;
use App\ErrorLog;
use Auth;
use App\InternalMail;
use Illuminate\Auth\AuthenticationException;
use App\Admin;
use App\User;
use App\Merchant;
use App\WebService;
use Lang;
use App\System;
use DB;

class Handler extends ExceptionHandler
{
    public function __construct(Request $request){
        $this->request = $request;
    }

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {   
        dd($exception);
        if ($this->shouldReport($exception)) {
            $this->insert_error_record($exception);
            $this->sendEmail($exception); // sends an email
        }
        
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }
    
    public function sendEmail(Exception $exception)
    {
        $msg_data = array("message" => $exception->getMessage(), "file" => $exception->getFile(), "line" => $exception->getLine(), "url" => $this->request->fullUrl(), "postData" => $_POST, "getData" => $_GET);
            
        // $e = FlattenException::create($exception);
        //$handler = new SymfonyExceptionHandler();
        //$html = $handler->getHtml($e);
        //$developer_email = Config::get('app.developer_email');
        
        if(config('app.send_error_email')){
            $internal_mail = new InternalMail();
            $internal_mail->send($msg_data);
        }
    }

    public function insert_error_record(Exception $exception)
    {
        $el = new ErrorLog;
        $data = array(
            "message" => $exception->getMessage(),
            "trace" => json_encode($exception->getTrace()),
        );
       
        try{
            $el->add_err_log($data);
        }catch(Exception $ex){ 
            dd($ex);
        }        
    }

    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        $guard = array_get($exception->guards(), 0);
        switch ($guard) {
            case 'api':
                $mobile_authentication = true;
                break;
            case 'admin-api':
                $mobile_authentication = true;
                break;
            case 'merchant-api':
                $mobile_authentication = true;
                break;
            default:
                $mobile_authentication = true;
                break;
        }
        
        if($request->segment(1) ==  Config::get('app.chinese_locale') ){
            return redirect()->guest(route($request->segment(1).'.'.$login));
        }
        
        if(!$mobile_authentication){
            return redirect()->guest(route($login));
        }
        else{
            $ws = new WebService();
            $status = $ws::$api_code_to_status['internal-server-error'];
            $data = array(
                "dev_status" => $ws::$api_error_detail_to_code['internal-server-error']['user-not-found'],
                "general_msg" => Lang::get('trans.api/authenticated/error'),
                "success" => "",
                "dev_msg" => "Unauthenticated.",
            ); 
            $result = $ws->api_result($status, $data);
            return response()->json($result); 
        }
    }
}
