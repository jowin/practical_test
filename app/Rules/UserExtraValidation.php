<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\User;

class UserExtraValidation implements Rule
{
    protected $field = '';

    public function __construct()
    {
    }

    public function passes($attribute, $value)
    {
        $confirm_valid = true;

        switch ($attribute) {
            case 'email':
                $this->field = "email";
                $confirm_valid = false;
                $result = User::where('email', $value)->first();
                $confirm_valid = (!empty($result))? true : false;
                break;
            default:
                break;
        }

        return $confirm_valid;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $msg = trans('validation.valid_data');

        if($this->field == "email"){
            $msg = trans('validation.used');
        }

        return $msg;
    }
}