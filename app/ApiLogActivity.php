<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiLogActivity extends Model
{
    protected $table = 'ws_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'wsid', 'last_response', 'url', 'post_data', 'get_data', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
