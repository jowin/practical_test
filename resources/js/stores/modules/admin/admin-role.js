import Config from '@/js/config/app.js';
import Router from '@/js/routes';
import axios from 'axios';

export default {
    namespaced: true,
    state: {
        searchResult: '',
        roleList: '',
        infoSignal: {
            addSignal: '',   
            updateSignal: '',
            deleteSignal: '',
            quickSearchSignal: '',
            advanceSearchSignal: '',
        }, 
        field_error: {},
        isShowingRoleInfo: '',
        permissionList: {},
    },
    mutation: {
    },
    getters: {
    },
    actions: {
        retriveRoleList({state, commit, rootState}, incomingData){
            axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('loginToken');
            axios.post(Config.base_url() + 'admin/admin-role/index', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['role_list_info'];
                }
            })
            .catch(error => console.log(error));             
        },
       
        executeQuickSearch({state, commit, rootState}, incomingData){         
			axios.post(Config.base_url() + 'admin/admin-role/index', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['role_list_info'];
                    state.infoSignal.quickSearchSignal = true;
                }
            })
            .catch(error => console.log(error));            
        },

        executeAdvanceSearch({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-role/index', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['role_list_info'];
                    state.infoSignal.advanceSearchSignal = true;
                }
            })
            .catch(error => console.log(error));    
        },
        switchPage({state, commit, rootState}, incomingData){  
            axios.post(incomingData.url, incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.searchResult = '';
                    state.searchResult = res['data']['data']['role_list_info'];
                }
            })
            .catch(error => console.log(error));             
        },
        
        /* Create */       
        insertRole({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-role/add-role', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.infoSignal.addSignal = Config.generalStatus['success'];
                    Router.push({ name: "admin-role"});
                }else {
                    state.field_error = res['data']['field_error'];
                    state.infoSignal.addSignal = Config.generalStatus['error'];
                }
            })
            .catch(error => console.log(error)); 
        },
        /* End Create */

        retrivePermissionList({state, coimmit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-role/get-permission-list', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.permissionList = res['data']['data']['info'];
                }  
            })
            .catch(error => console.log(error));
        },

        /* Retrieve Role Info */
        retreiveRoleInfo({state, coimmit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-role/get-role-info', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.isShowingRoleInfo = res['data']['data']['info'];
                }  
            })
            .catch(error => console.log(error));
        },
        /* End Role Info */ 
        
        /* Update Role Info */
        updateRole({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-role/update-role', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.infoSignal.updateSignal = Config.generalStatus['success'];
                    Router.push({ name: "admin-role"});
                }else {
                    state.field_error = res['data']['field_error'];
                    state.infoSignal.updateSignal = Config.generalStatus['error'];
                }
            })
            .catch(error => console.log(error)); 
        },

        /* Delete Role */
        deleteRole({state, commit, rootState}, incomingData){
            axios.post(Config.base_url() + 'admin/admin-role/delete-role', incomingData)
                .then(res => {
                if( (res['status'] == 200) && (res['data']['status'] == 1000) && (res['data']['message'] == "success") ){
                    state.infoSignal.deleteSignal = Config.generalStatus['success'];
                    Router.push({ name: "admin-role"});
                }else {
                    state.field_error = res['data']['field_error'];
                    state.infoSignal.deleteSignal = Config.generalStatus['error'];
                }
            })
            .catch(error => console.log(error)); 
        },
    }
}