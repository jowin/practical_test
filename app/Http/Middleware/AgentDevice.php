<?php

namespace App\Http\Middleware;

use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Closure;

class AgentDevice
{
    public $attribute;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function handle($request, Closure $next)
    {
        $agent = new Agent();

        if($agent->isDesktop()){
            $request->attributes->add(['is_desktop' => true, 'is_mobile' => false]);
        }else{ 
            $request->attributes->add(['is_desktop' => false, 'is_mobile' => true]);
        }

        return $next($request);
    }
}
