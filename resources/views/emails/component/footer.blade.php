    <tr>
        <td colspan="2" style="color: #777; background: #000; padding: 16px; font-size: 14px; text-align: center; border-top: 1px solid #b1861c;">Copyright © @php echo \Carbon\Carbon::now(config('app.server_datetime'))->format('Y'); @endphp Uniqhue. All rights reserved.</td>
    </tr>
</table>
</body>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #ececec;
        color: #000;
        text-align: left;
    }

    tr:first-child {
        background: #fff; 
        padding: 20px 12px; 
        text-align: center;
        margin: 0 auto;
    }

    tr:last-child {
        color: #fff; 
        background: #e0d50b; 
        padding: 20px 12px; 
        text-align: center;
    }
</style>


