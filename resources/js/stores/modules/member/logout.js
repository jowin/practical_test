import Router from '@/js/routes';

export default {
    namespaced: true,
    state: {
        
    },
    mutations: {
        
    },
    getters: {        
    },
    actions: {   
        logout({commit}){
            commit('clearAuthInfo');
            localStorage.removeItem('loginTokenExpireDate');
            localStorage.removeItem('loginToken');
            localStorage.removeItem('memberInfo');
            Router.replace({name: 'member.login'});
            Router.go({name: "member.login"});
        },
    },

}